const elixir = require('laravel-elixir');

require('laravel-elixir-vue');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
  mix
    .sass('web/web.scss')
    .copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts')
    .webpack('web/main.js', null, null, {
      output: {
        publicPath: 'js/', //异步加载路径
        filename: 'web.js'
      },
      resolve: {
        //使用别名,压缩体积
        alias: {
          'vue': 'vue/dist/vue.min',
          'vue-resource': 'vue-resource/dist/vue-resource.min',
          'jquery': 'jquery/dist/jquery.min'
        }
      },
    });
});
