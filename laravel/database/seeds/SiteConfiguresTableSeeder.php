<?php

use Illuminate\Database\Seeder;

use App\Models\SiteConfigure;

class SiteConfiguresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=SiteConfiguresTableSeeder
     * @return void
     */
    public function run()
    {
        $configures = SiteConfigure::get();
        $configures_id = $configures->pluck('id')->toArray();

        $shipping_costs = [
            [
                'id' => 'express_price',
                'local_use' => 'order',
                'value' => 0,
                'label' => '快递价格',
                'meta' => null
            ], [
                'id' => 'canvas_parameter',
                'local_use' => 'template',
                'value' => '',
                'label' => '模板画布参数',
                'meta' => [
                    'width' => 300,
                    'height' => 500,
                    'unit' => 'px'
                ]
            ], [
                'id' => 'start_image',
                'local_use' => 'start',
                'value' => 'http://7xw8t9.com1.z0.glb.clouddn.com/o_1ars4u1f793m1csa1d1711b8bs47.png',
                'label' => '首页背景图',
                'meta' => null
            ]
        ];
        foreach ($shipping_costs as $v) {
            if (in_array($v['id'], $configures_id)) {
                $configures->find($v['id'])->update($v);
                continue;
            }
            SiteConfigure::create($v);
        }

    }
}
