<?php

use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=TestTableSeeder
     * php artisan migrate:refresh --seed
     * @return void
     */
    public function run()
    {
//        User::whereRaw('1=1')->forceDelete();

        factory(\App\Models\User::class, 50)->create()->each(function($u){
            factory(\App\Models\UserEducation::class, 1)->create([
                'user_id' => $u->id
            ]);
            factory(\App\Models\UserExperience::class, 1)->create([
                'user_id' => $u->id
            ]);
        });
    }
}
