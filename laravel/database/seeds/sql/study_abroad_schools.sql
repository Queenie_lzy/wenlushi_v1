-- MySQL dump 10.13  Distrib 5.7.13, for osx10.11 (x86_64)
--
-- Host: localhost    Database: wenlushi
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `study_abroad_schools`
--

DROP TABLE IF EXISTS `study_abroad_schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_abroad_schools` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '学校中文名',
  `en_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '学校英文名',
  `type` enum('at_home','abroad') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'abroad' COMMENT '国内国外',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `study_abroad_schools_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_abroad_schools`
--

LOCK TABLES `study_abroad_schools` WRITE;
/*!40000 ALTER TABLE `study_abroad_schools` DISABLE KEYS */;
INSERT INTO `study_abroad_schools` VALUES (1,'德克萨斯大学-奥斯汀分校','University of Texas - Austin','abroad',NULL,NULL),(2,'斯坦福大学','Stanford University','abroad',NULL,NULL),(3,'德州农工大学-卡城分校','Texas A&M University - College Station','abroad',NULL,NULL),(4,'塔尔萨大学','University of Tulsa','abroad',NULL,NULL),(5,'科罗拉多矿业大学','Colorado School of Mines','abroad',NULL,NULL),(6,'俄克拉荷马大学','University of Oklahoma','abroad',NULL,NULL),(7,'南加利福尼亚大学','University of Southern California','abroad',NULL,NULL),(8,'德州理工大学','Texas Tech University','abroad',NULL,NULL),(9,'怀俄明大学','University of Wyoming','abroad',NULL,NULL),(10,'宾夕法尼亚州立大学-伯克分校','Pennsylvania State University - University Park','abroad',NULL,NULL),(11,'密歇根大学-安娜堡分校','University of Michigan - Ann Arbor','abroad',NULL,NULL),(12,'亚利桑那大学','University of Arizona','abroad',NULL,NULL),(13,'加利福尼亚理工学院','California Institute of Technology','abroad',NULL,NULL),(14,'麻省理工学院','Massachusetts Institute of Technology','abroad',NULL,NULL),(15,'加利福尼亚大学-伯克利分校','University of California - Berkeley','abroad',NULL,NULL),(16,'加利福尼亚大学-圣巴巴拉分校','University of California - Santa Barbara','abroad',NULL,NULL),(17,'科罗拉多大学-波德分校','University of Colorado - Boulder','abroad',NULL,NULL),(18,'威斯康星大学-麦迪逊分校','University of Wisconsin - Madison','abroad',NULL,NULL),(19,'德州农工大学','Texas A&M University','abroad',NULL,NULL),(20,'莱斯大学','Rice University','abroad',NULL,NULL),(21,'休斯顿大学','University of Houston','abroad',NULL,NULL),(22,'俄克拉荷马州立大学','Oklahoma State University','abroad',NULL,NULL),(23,'拉马尔大学','Lamar University','abroad',NULL,NULL),(24,'路易斯安那大学','University of Louisiana','abroad',NULL,NULL),(25,'路易斯安那州立大学','Louisiana State University','abroad',NULL,NULL),(26,'杜兰大学','Tulane University','abroad',NULL,NULL),(27,'堪萨斯大学','University of Utah','abroad',NULL,NULL),(28,'犹他大学','University of Texas - Austin','abroad',NULL,NULL),(29,'密苏里科技大学','Missouri University of Science & Technology','abroad',NULL,NULL),(30,'马里兰大学','University of Maryland','abroad',NULL,NULL);
/*!40000 ALTER TABLE `study_abroad_schools` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-04 11:10:53
