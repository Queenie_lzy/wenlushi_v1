-- MySQL dump 10.13  Distrib 5.7.13, for osx10.11 (x86_64)
--
-- Host: localhost    Database: wenlushi
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `school_specialty_infos`
--

DROP TABLE IF EXISTS `school_specialty_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_specialty_infos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '院系名称',
  `grade` json DEFAULT NULL COMMENT '成绩要求',
  `deadline_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '截止时间',
  `inform_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '结果通知',
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '链接',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `school_id` int(10) unsigned NOT NULL,
  `specialty_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `school_specialty_infos_school_id_specialty_id_unique` (`school_id`,`specialty_id`),
  KEY `school_specialty_infos_specialty_id_foreign` (`specialty_id`),
  CONSTRAINT `school_specialty_infos_school_id_foreign` FOREIGN KEY (`school_id`) REFERENCES `study_abroad_schools` (`id`) ON DELETE CASCADE,
  CONSTRAINT `school_specialty_infos_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `study_abroad_specialties` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_specialty_infos`
--

LOCK TABLES `school_specialty_infos` WRITE;
/*!40000 ALTER TABLE `school_specialty_infos` DISABLE KEYS */;
INSERT INTO `school_specialty_infos` VALUES (1,'Department of Petroleum and Geosystems Engineering ','{\"gpa\": \"3\", \"ielts\": \"6.5\", \"tofel\": \"79\"}','15/12/2016',NULL,NULL,'2016-11-04 02:12:06','2016-11-04 02:12:06',1,1),(2,'Department of Energy Resources Engineering','{\"tofel\": \"100(PhD) 89(Master)\"}','13/12/2016',NULL,NULL,'2016-11-04 02:13:02','2016-11-04 02:13:02',2,1),(3,'Harold Vance Department of Petroleum Engineering','{\"tofel\": \"80\"}','1/2/2017',NULL,NULL,'2016-11-04 02:13:33','2016-11-04 02:13:33',3,1),(4,'McDougall School of Petroleum Engineering','{\"gpa\": \"B\", \"ielts\": \"6\", \"tofel\": \"80\"}','01/15/2017 (Fellowship/Scholarship); 02/01/2017',NULL,NULL,'2016-11-04 02:14:37','2016-11-04 02:14:37',4,1),(5,'Petroleum Engineering Department ','{\"gpa\": \"3\", \"tofel\": \"79\"}','01/05/2017 (financial support); 03/01/2017',NULL,NULL,'2016-11-04 02:15:13','2016-11-04 02:15:13',5,1),(6,'Department of Engergy and Mineral Engineering','{\"gre\": \"159-146-3.5\", \"ielts\": \"6.5\", \"tofel\": \"80 (Speaking>=19)\"}','1/12/2016',NULL,NULL,'2016-11-04 02:15:52','2016-11-04 02:15:52',10,1),(7,'Mewbourne School of Petroleum and Geological Engineering','{\"gpa\": \"3.2\", \"gre\": \"151-159-3\", \"ielts\": \"6.5\", \"tofel\": \"79\"}','1/3/2017',NULL,NULL,'2016-11-04 02:16:51','2016-11-04 02:16:51',6,1),(8,'Mork Family Department of Chemical Engineering and Materials Science','{\"ielts\": \"7.0(PhD) 6.5(Master)\", \"tofel\": \"100 (each >=20) 90 (Master)\"}','15/12/2016',NULL,NULL,'2016-11-04 02:17:30','2016-11-04 02:17:30',7,1),(9,'Bob L. Herd Department of Petroleum Engineering','{\"gpa\": \"3\", \"ielts\": \"6.5\", \"tofel\": \"79\"}','15/1/2017',NULL,NULL,'2016-11-04 02:18:04','2016-11-04 02:18:04',8,1),(10,'Department of Petroleum Engineering','{\"gpa\": \"3\", \"gre\": \"305 (V+Q)\", \"ielts\": \"6.5\", \"tofel\": \"80\"}','1/2/2017',NULL,NULL,'2016-11-04 02:18:59','2016-11-04 02:18:59',9,1),(11,'Craft & Hawkins Department Of Petroleum Engineering','{\"gpa\": \"3\", \"gre\": \"150-150\", \"ielts\": \"6\", \"tofel\": \"79\"}','15/2/2017',NULL,NULL,'2016-11-04 02:19:42','2016-11-04 02:19:42',25,1),(12,'Department of Geosciences','{\"gpa\": \"3\", \"ielts\": \"6.5\", \"tofel\": \"80 (Speaking>=19)\"}','11/18/2016 (Fellowship/Scholarship); 12/15/2016',NULL,NULL,'2016-11-04 02:20:43','2016-11-04 02:20:43',10,2),(13,'Department of Earth and Environmental Sciences','{\"tofel\": \"100 (Dept); 84 (Univ)\"}','5/1/2017',NULL,NULL,'2016-11-04 02:21:38','2016-11-04 02:21:38',11,2),(14,'Department of Geological Sciences','{\"tofel\": \"100\"}','29/11/2016',NULL,NULL,'2016-11-04 02:22:28','2016-11-04 02:22:28',2,2),(15,'Department of Geosciences','{\"gpa\": \"3\", \"ielts\": \"7\", \"tofel\": \"79\"}','6/1/2017',NULL,NULL,'2016-11-04 02:23:56','2016-11-04 02:23:56',12,2),(16,'Division of Geological and Planetary Sciences','{\"gpa\": \"3.5\"}','1/1/2017',NULL,NULL,'2016-11-04 02:24:30','2016-11-04 02:24:30',13,2),(17,'Department of Geological Sciences','{\"ielts\": \"6.5\", \"tofel\": \"79\"}','12/01/2016 (Fellowship); 1/1/2017',NULL,NULL,'2016-11-04 02:24:57','2016-11-04 02:24:57',1,2),(18,'Department of Earth, Atmospheric, and Planetary Sciences','{\"ielts\": \"7\", \"tofel\": \"100\"}','5/1/2017',NULL,NULL,'2016-11-04 02:25:24','2016-11-04 02:25:24',14,2),(19,'Department of Earth and Planetary Science','{\"gpa\": \"3\", \"ielts\": \"7\", \"tofel\": \"90\"}','21/12/2016',NULL,NULL,'2016-11-04 02:25:56','2016-11-04 02:25:56',15,2),(20,'Department of Earth Science','{\"gpa\": \"B\", \"ielts\": \"7\", \"tofel\": \"80\"}','8/1/2017',NULL,NULL,'2016-11-04 02:26:33','2016-11-04 02:26:33',16,2),(21,'Department of Geological Sciences','{\"gpa\": \"3\", \"ielts\": \"Not accepted\", \"tofel\": \"75\"}','1/12/2016',NULL,NULL,'2016-11-04 02:27:11','2016-11-04 02:27:11',17,2),(22,'Department of Geoscience','{\"gpa\": \"3\", \"ielts\": \"7\", \"tofel\": \"92\"}','4/1/2017',NULL,NULL,'2016-11-04 02:27:37','2016-11-04 02:27:37',18,2),(23,'Artie McFerrin Department of Chemical Engineering','{\"gpa\": \"min 3.3/4; ave 3.65/4\", \"gre\": \"No min; ave 165Q 156V\", \"tofel\": \"550 pBT/213 cBT/ 79-80 iBT/GRE 146V\"}','12.01 Fall; 08.01 Spring',NULL,'http://engineering.tamu.edu/chemical/future-students','2016-11-04 02:29:25','2016-11-04 02:29:25',19,3),(24,'McKetta Department  of Chemical Engineering','{\"gpa\": \"min 3.0/4.0\", \"ielts\": \"6.5\", \"tofel\": \"79 iBT\"}','12.15 Fall',NULL,'http://che.utexas.edu/prospective-students/graduate-students/admissions/','2016-11-04 02:31:24','2016-11-04 02:31:24',1,3),(25,'Chemical and Biomolecular Engineering','{\"ielts\": \"7\", \"tofel\": \"90 iBT/600 pBT/250 cBT\"}','12.31 Fall',NULL,'https://chbe.rice.edu/Content.aspx?id=92','2016-11-04 02:32:23','2016-11-04 02:32:23',20,3),(26,'Chemical and Biomolecular Engineering','{\"gpa\": \"min 3.0/4/0\", \"gre\": \"Ave Q+V 315, Q160\", \"ielts\": \"6.5(550 pBT 58 writing)\", \"tofel\": \"78-80 iBT 20 writing\"}','02.15 Fall; all other docs 3.1',NULL,'http://www.chee.uh.edu/graduate/admissions','2016-11-04 02:34:15','2016-11-04 02:34:15',21,3),(27,'Chemical Engineering','{\"gpa\": \"5% of class\", \"gre\": \"Outstanding\", \"ielts\": \"Outstanding\", \"tofel\": \"Outstanding\"}',NULL,NULL,'http://www.depts.ttu.edu/che/grad/admissions.php','2016-11-04 02:35:13','2016-11-04 02:35:13',8,3),(28,'Chemical, Biological & Materials Engineering','{\"gpa\": \"3.0/4.0\", \"gre\": \"not required; 70% considered good\", \"ielts\": \"7.0(Dept)/6.5(School)\", \"tofel\": \"100iBT/250cBT/550pBT(Dept)  79iBT/213cBT/550pBT(School)\"}','4.1 (rec. 2.1-3.1) Fall; 9.1 Spring',NULL,'http://www.ou.edu/content/coe/cbme/academics/graduate/frequentlyaskedquestions.html','2016-11-04 02:38:03','2016-11-04 02:38:03',6,3),(29,'Chemical Engineering','{\"gpa\": \"3.0/4.0 Under; 3.5/4.0 Grad\", \"gre\": \"310 Q+V\", \"ielts\": \"6.5\", \"tofel\": \"79iBT/213cBT/550pBT\"}','2.1 Fall; 8.1 Spring',NULL,'https://che.okstate.edu/content/graduate_admissions','2016-11-04 02:39:12','2016-11-04 02:39:12',22,3),(30,'Chemical Engineering','{\"gpa\": \"3.5/4.0\", \"gre\": \"150V+155Q\", \"ielts\": \"6.5\", \"tofel\": \"85iBT/563pBT\"}','2.1; 10.1',NULL,'http://utulsa.catalog.acalog.com/preview_program.php?catoid=14&poid=1881&returnto=767','2016-11-04 02:40:40','2016-11-04 02:40:40',4,3),(31,'Dan F. Smith Department of Chemical Engineering','{\"gpa\": \"3.0/4.0 Under; 3.5/4.0 Grad\", \"gre\": \"308 Q+V\", \"tofel\": \"80iBT\"}',NULL,NULL,'http://engineering.lamar.edu/chemical/graduate/phd-program.html','2016-11-04 02:41:32','2016-11-04 02:41:32',23,3),(32,'Cain Department of Chemical Engineering','{\"gpa\": \"3.0/4.0\", \"gre\": \"300 Q+V\", \"ielts\": \"6.5IELTS/PTE59\", \"tofel\": \"79iBT/213cBT/550pBT\"}','4.15 (financial) Fall; 10.15 Spring',NULL,'http://www.che.lsu.edu/academics/graduate/application','2016-11-04 02:42:37','2016-11-04 02:42:37',24,3),(33,'Chemical Engineering (no Ph.D., M.S. only)','{\"gpa\": \"3.0/4.0\", \"gre\": \"145V, 287 Q+V\"}','3.1 (financial) fall; 11.1 (financial) Spring',NULL,'http://chemical.louisiana.edu/programs/masters/requirements','2016-11-04 02:43:24','2016-11-04 02:43:24',25,3),(34,'Chemical and Biomolecular Engineering','{\"gre\": \"Required\", \"ielts\": \"Required\", \"tofel\": \"Required\"}','12.31 Fall; 10.15 Spring',NULL,'http://www2.tulane.edu/sse/cbe/academics/graduate/admissions-and-financial-aid/index.cfm','2016-11-04 02:43:56','2016-11-04 02:43:56',26,3),(35,'Chemical and Biological Engineering','{\"tofel\": \"No graduate req; Under req: 79iBT(20R-21L-17W-21S)/550pBT(54R-55L-55W)\"}','12.15 Fall',NULL,'http://www.mines.edu/graduate_admissions','2016-11-04 02:45:06','2016-11-04 02:45:06',5,3),(36,'Chemical and Petroleum Engineering','{\"gpa\": \"3.0/4.0\", \"gre\": \"Q-158, V-149, A-3.5\", \"ielts\": \"6.5(no individual <5.5)\", \"tofel\": \"90iBT\"}','12.15 priority funding, 3.1 final Fall',NULL,'https://cpe.ku.edu/graduate-admissions','2016-11-04 02:46:11','2016-11-04 02:46:11',27,3),(37,'Chemical Engineering','{\"gpa\": \"3.0/4.0\", \"gre\": \"155Q\", \"ielts\": \"6.5\", \"tofel\": \"80iBT/550pBT\"}','12.15 priority, 1.15 final',NULL,'https://www.che.utah.edu/graduate/admissions','2016-11-04 02:47:00','2016-11-04 02:47:00',28,3),(38,'Chemical and Biochemical Engineering','{\"gpa\": \"3.0/4.0\", \"gre\": \"300Q+V, 155Q\", \"ielts\": \"6.5\", \"tofel\": \"79iBT/230cBT/570pBT\"}','2.1 Fall',NULL,'http://chemeng.mst.edu/graduatedegreeprograms/gradchemicalengineering/','2016-11-04 02:47:43','2016-11-04 02:47:43',29,3),(39,'Chemical and Biomolecular Engineering','{\"gre\": \"Required\", \"tofel\": \"100iBT\"}','2.1 Fall (financial); 10.1 Spring',NULL,'http://www.ench.umd.edu/graduate/admissions','2016-11-04 02:48:22','2016-11-04 02:48:22',30,3);
/*!40000 ALTER TABLE `school_specialty_infos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-04 11:06:28
