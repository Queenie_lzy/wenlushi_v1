<?php

use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=SchoolTableSeeder
     * @return void
     */
    public function run()
    {
        $abroad_schools = [
            ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/德克萨斯大学-奥斯汀分校.png?imageView2/1/w/128/h/128', 'name' => '德克萨斯大学-奥斯汀分校', 'en_name' => 'University of Texas - Austin'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '斯坦福大学', 'en_name' => 'Stanford University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/德州农工大学.png?imageView2/1/w/128/h/128', 'name' => '德州农工大学-卡城分校', 'en_name' => 'Texas A&M University - College Station'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/塔尔萨大学.png?imageView2/1/w/128/h/128', 'name' => '塔尔萨大学', 'en_name' => 'University of Tulsa'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/科罗拉多矿业大学.png?imageView2/1/w/128/h/128', 'name' => '科罗拉多矿业大学', 'en_name' => 'Colorado School of Mines'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/俄克拉荷马大学.png?imageView2/1/w/128/h/128', 'name' => '俄克拉荷马大学', 'en_name' => 'University of Oklahoma'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '南加利福尼亚大学', 'en_name' => 'University of Southern California'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/德州理工大学.png?imageView2/1/w/128/h/128', 'name' => '德州理工大学', 'en_name' => 'Texas Tech University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '怀俄明大学', 'en_name' => 'University of Wyoming'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '宾夕法尼亚州立大学-伯克分校', 'en_name' => 'Pennsylvania State University - University Park'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '密歇根大学-安娜堡分校', 'en_name' => 'University of Michigan - Ann Arbor'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '亚利桑那大学', 'en_name' => 'University of Arizona'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '加利福尼亚理工学院', 'en_name' => 'California Institute of Technology'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '麻省理工学院', 'en_name' => 'Massachusetts Institute of Technology'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '加利福尼亚大学-伯克利分校', 'en_name' => 'University of California - Berkeley'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '加利福尼亚大学-圣巴巴拉分校', 'en_name' => 'University of California - Santa Barbara'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '科罗拉多大学-波德分校', 'en_name' => 'University of Colorado - Boulder'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '威斯康星大学-麦迪逊分校', 'en_name' => 'University of Wisconsin - Madison'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/德州农工大学.png?imageView2/1/w/128/h/128', 'name' => '德州农工大学', 'en_name' => 'Texas A&M University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/莱斯大学.png?imageView2/1/w/128/h/128', 'name' => '莱斯大学', 'en_name' => 'Rice University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/休斯顿大学.png?imageView2/1/w/128/h/128', 'name' => '休斯顿大学', 'en_name' => 'University of Houston'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/俄克拉荷马州立大学.png?imageView2/1/w/128/h/128', 'name' => '俄克拉荷马州立大学', 'en_name' => 'Oklahoma State University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/拉马尔大学.png?imageView2/1/w/128/h/128', 'name' => '拉马尔大学', 'en_name' => 'Lamar University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128', 'name' => '路易斯安那大学', 'en_name' => 'University of Louisiana'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/路易斯安那州立大学.png?imageView2/1/w/128/h/128', 'name' => '路易斯安那州立大学', 'en_name' => 'Louisiana State University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/杜兰大学.png?imageView2/1/w/128/h/128', 'name' => '杜兰大学', 'en_name' => 'Tulane University'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/堪萨斯大学.png?imageView2/1/w/128/h/128', 'name' => '堪萨斯大学', 'en_name' => 'University of Utah'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/犹他大学.png?imageView2/1/w/128/h/128', 'name' => '犹他大学', 'en_name' => 'University of Texas - Austin'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/密苏里科技大学.png?imageView2/1/w/128/h/128', 'name' => '密苏里科技大学', 'en_name' => 'Missouri University of Science & Technology'], ['logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/马里兰大学.png?imageView2/1/w/128/h/128', 'name' => '马里兰大学', 'en_name' => 'University of Maryland'],
        ];
        foreach ($abroad_schools as $k => $v) {
            $v['type'] = 'abroad';
            \App\Models\StudyAbroadSchool::create($v);
        }

        $host = env('DB_HOST');
        $port = env('DB_PORT');
        $db = env('DB_DATABASE');
        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');

        $sqls[] = __DIR__ . '/sql/school_specialty_infos.sql';
        $sqls[] = __DIR__ . '/sql/study_abroad_specialties.sql';

        foreach ($sqls as $sql) {
            exec("mysql -u " . $user . " -p" . $pass . ' -h' . $host . ' -P' . $port . ' ' . $db . " < " . $sql);
        }

         //获取国内院校信息以及建立关联
        $specialties = \App\Models\StudyAbroadSpecialty::All();
        foreach ($specialties as $key => $value) {
            $path = __DIR__ . '/csv/'.$value->name.'.csv';
            if(!file_exists($path)) break;
            $file = fopen($path,'r');

            while ($data = fgetcsv($file)) {
                if(!$data[0]) break;
                $school = \App\Models\StudyAbroadSchool::whereName($data[0])->first();
                if(!$school) {
                  $school['id'] = \App\Models\StudyAbroadSchool::create([
                        'name' => $data[0],
                        'en_name' => $data[1],
                        'type' => 'at_home'
                    ])->id;
                }

                $schoolSpecialtyInfo = \App\Models\SchoolSpecialtyInfo::whereSchoolId($school['id'])
                    ->whereSpecialtyId($value->id)
                    ->first();
                if(!$schoolSpecialtyInfo) {
                    \App\Models\SchoolSpecialtyInfo::create([
                            'school_id' => $school['id'],
                            'specialty_id' => $value->id
                        ]);
                }
            }
            fclose($file);
        }
    }
}
