<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');

    return [
        'name' => $faker->name.rand(1,9),
        'avatar' => 'http://7xpyuo.com1.z0.glb.clouddn.com/' . rand(1, 900) . '.jpeg?imageView2/1/w/280/h/280/interlace/1/q/100',
        'email' => $faker->safeEmail.rand(1,9),
        'password' => '666666',
        'role' => $faker->randomElement(array_keys(trans('params.user.role'))),
        'cellphone_code' => 86,
        'cellphone' => $faker->phoneNumber,
        'contact_tool' => $faker->randomElement(['微信', 'QQ']),
        'contact_tool_no' => $faker->numberBetween(0, 1000000000),
    ];
});

$factory->define(App\Models\UserEducation::class, function () {
    $faker = Faker\Factory::create('zh_CN');

    return [
        'degree' => $faker->randomElement(['学士', '硕士', '博士']),
        'school' => $faker->randomElement(['华南', '中山', '理工', '师范']).'大学',
        'specialty' => $faker->randomElement(['信息管理与信息系统', '管理科学', '工程管理', '工程造价']),
        'date' => $faker->randomElement(['2013-9 ~ 2016-7', '2010-9 ~ 2013-7', '2011-9 ~ 2014-7', '2012-9 ~ 2015-7', '2016-8 ~ 2016-9'])
    ];
});

$factory->define(App\Models\UserExperience::class, function () {
    $faker = Faker\Factory::create('zh_CN');

    return [
        'company' => $faker->company,
        'description' => $faker->randomElement(['信息部', '技术部', '人事部', '财务部']),
        'position' => $faker->randomElement(['专员', '助理', '实习', '组长']),
        'date' => $faker->randomElement(['2013-9 ~ 2014-7', '2010-9 ~ 2012-7', '2011-9 ~ 2013-7', '2012-9 ~ 2016-7', '2016-8 ~ 2016-9'])
    ];
});
