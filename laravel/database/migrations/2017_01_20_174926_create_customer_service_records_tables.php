<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerServiceRecordsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_service_records', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('email', 100);
            $table->string('cellphone_code')->comment('国家电话区号');
            $table->string('cellphone')->comment('手机号码');
            $table->string('contact_tool')->comment('聊天工具');
            $table->string('contact_tool_no')->nullable()->comment('聊天号码');
            $table->text('summary');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        //记录和服务关联表
        Schema::create('record_service_tag', function(Blueprint $table){
            $table->unsignedInteger('record_id');
            $table->foreign('record_id')->references('id')->on('customer_service_records')->onDelete('cascade');

            $table->unsignedInteger('service_tag_id');
            $table->foreign('service_tag_id')->references('id')->on('service_tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('record_service_tag');
        Schema::drop('customer_service_records');
    }
}
