<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyAbroadAppliesTable extends Migration
{
    /**
     * Run the migrations.
     * 服务-留学申请
     * @return void
     */
    public function up()
    {
        Schema::create('study_abroad_applies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->comment('留学类型');
            $table->string('country')->comment('目标国家');
            $table->json('season')->nullable()->comment('申请季');
            $table->string('major')->comment('首选专业');
            $table->json('school')->nullable()->comment('首选专业院校');
            $table->string('secondary')->comment('次选专业');
            $table->json('secondary_school')->nullable()->comment('次选专业院校');

            $table->string('other_requirement')->comment('其他要求');
            $table->enum('status', [
                    'sales-in',
                    'service-in',
                    'complete',
                    'stop'
                ])->nullable()->index()->comment('状态');
            $table->string('status_record')->default('0')->comment('申请所处状态记录');
            $table->timestamp('submit_at')->nullable()->comment('提交时间');
            $table->timestamp('service_at')->nullable()->comment('启动服务时间');
            $table->timestamp('complete_at')->nullable()->comment('完成服务时间');
            $table->timestamp('stop_at')->nullable()->comment('终止服务时间');
            $table->string('remarks')->nullable()->comment('备注');
            $table->timestamps();

            $table->unsignedInteger('tutor_id')->nullable()->comment('分配导师');
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('study_abroad_applies');
    }
}
