<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     * 活动
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('start')->comment('开始时间');
            $table->timestamp('end')->comment('结束时间');
            $table->timestamp('deadline_at')->comment('截止时间');
            $table->unsignedInteger('maximum')->nullable()->comment('名额限制');
            $table->json('contact')->nullable()->comment('联系方式');
            $table->timestamps();

            $table->unsignedInteger('article_id')->comment('活动类别文章ID');
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->unsignedInteger('user_id')->comment('嘉宾ID');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('activity_registers', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->comment('用户ID');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('activity_id')->comment('活动ID');
            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activity_registers');
        Schema::drop('activities');
    }
}
