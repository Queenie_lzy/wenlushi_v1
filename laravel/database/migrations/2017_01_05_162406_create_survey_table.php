<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     * 调查统计表
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('wechat')->default(0)->comment('微信公众号');
            $table->unsignedInteger('network')->default(0)->comment('互联网');
            $table->unsignedInteger('brochure')->default(0)->comment('宣传手册');
            $table->unsignedInteger('friend')->default(0)->comment('朋友');
            $table->unsignedInteger('activity')->default(0)->comment('近期活动');
            $table->unsignedInteger('other')->default(0)->comment('其他');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey');
    }
}
