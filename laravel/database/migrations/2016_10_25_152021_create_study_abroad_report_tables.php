<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyAbroadReportTables extends Migration
{
    /**
     * Run the migrations.
     * 留学服务报告表
     * @return void
     */
    public function up()
    {
        //启动服务表
        Schema::create('study_abroad_start', function (Blueprint $table) {
            $table->increments('id');
            $table->string('evaluation')->nullable()->comment('背景评估');
            $table->string('seller_conversation')->nullable()->comment('销售约谈');
            $table->string('expert_conversation')->nullable()->comment('专家约谈');
            $table->string('service_scope')->nullable()->comment('服务范围');
            $table->json('document_type')->nullable()->comment('文书类别');
            // $table->enum('service-scope',[])->default('')->comment('服务范围');
            $table->json('contract')->nullable()->comment('服务合同');
            $table->json('lock')->nullable()->comment('各字段是否可锁定不可编辑');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //申请学校表
        Schema::create('study_abroad_apply_schools', function (Blueprint $table) {
            $table->increments('id');
            $table->json('grade')->nullable()->comment('成绩要求');
            $table->string('deadline_at')->nullable()->comment('截止时间');
            $table->string('inform_at')->nullable()->comment('通知时间');
            $table->string('link')->nullable()->comment('链接');
            $table->string('professor')->nullable()->comment('教授');
            $table->string('department')->nullable()->comment('专业部门');
            $table->string('result')->default('not_submit')->comment('申请结果');
            // $table->enum('result', [])->default('')->comment('申请结果');
            $table->timestamps();

            $table->unsignedInteger('school_id');
            $table->foreign('school_id')->references('id')->on('study_abroad_schools')->onDelete('cascade');
            $table->unsignedInteger('specialty_id');
            $table->foreign('specialty_id')->references('id')->on('study_abroad_specialties')->onDelete('cascade');
            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');
        });

        //网上申请表
        Schema::create('study_abroad_online_applies', function(Blueprint $table){
            $table->increments('id');
            $table->json('lock')->nullable()->comment('各字段是否可锁定不可编辑');
            $table->timestamps();

            $table->unsignedInteger('apply_school_id')->nullable()->comment('最终选择的学校，申请学校ID');
            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //制定策略表
        Schema::create('study_abroad_strategy', function (Blueprint $table) {
            $table->increments('id');
            $table->text('strategy')->nullable()->comment('整体策略');
            $table->json('lock')->nullable()->comment('各字段是否可锁定不可编辑');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //提升背景表
        Schema::create('study_abroad_promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->json('content')->nullable()->comment('活动内容');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //时间规划表
        Schema::create('study_abroad_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->json('promotion')->comment('提升背景');
            $table->json('document')->comment('文书创作');
            $table->json('online_apply')->comment('网络申请');
            $table->json('visa')->comment('准备签证');
            $table->json('feedback')->comment('调查反馈/结束服务');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //文书创作表
        Schema::create('study_abroad_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->json('file')->comment('上传的文件');
            $table->string('type')->comment('文书类别');
            // $table->enum('type',[])->default('')->comment('文书类别');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique(['type', 'apply_id']);
        });

        //申请签证表
        Schema::create('study_abroad_visa', function (Blueprint $table) {
            $table->increments('id');
            $table->json('lock')->nullable()->comment('各字段是否可锁定不可编辑');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });

        //调查反馈表
        Schema::create('study_abroad_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('whole_evaluation')->default(0)->comment('整体评价');
            $table->smallInteger('prospect_evaluation')->default(0)->comment('前景评估');
            $table->smallInteger('strategize')->default(0)->comment('策略制定');
            $table->smallInteger('promotion')->default(0)->comment('背景提升');
            $table->smallInteger('document')->default(0)->comment('文书创作');
            $table->smallInteger('online_apply')->default(0)->comment('网络申请');
            $table->smallInteger('visa_training')->default(0)->comment('签证培训');
            $table->smallInteger('service_communication')->default(0)->comment('服务沟通');
            $table->smallInteger('service_price')->default(0)->comment('服务价格');
            $table->boolean('use_service')->nullable()->comment('是否使用过其他类似服务');
            $table->boolean('recommend_service')->nullable()->comment('是否会向朋友推荐服务');
            $table->boolean('service_again')->nullable()->comment('是否再次使用我们的服务');
            $table->text('advice')->nullable()->comment('意见');
            $table->smallInteger('tutor_score')->default(0)->comment('导师对学生的评分');
            $table->text('tutor_evaluation')->nullable()->comment('导师评价内容');
            $table->text('experience_sharing')->nullable()->comment('经验分享');
            $table->json('lock')->nullable()->comment('各字段是否可锁定不可编辑');
            $table->timestamps();

            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');

            $table->unique('apply_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('study_abroad_feedback');
        Schema::drop('study_abroad_visa');
        Schema::drop('study_abroad_documents');
        Schema::drop('study_abroad_schedules');
        Schema::drop('study_abroad_promotions');
        Schema::drop('study_abroad_strategy');
        Schema::drop('study_abroad_online_applies');
        Schema::drop('study_abroad_apply_schools');
        Schema::drop('study_abroad_start');

    }
}
