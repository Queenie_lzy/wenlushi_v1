<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     * http://laravelacademy.org/post/6191.html#ipt_kb_toc_6191_8
     * @return void
     */
    public function up()
    {
        //标签表
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('tag_gables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tag_gable_id');
            $table->string('tag_gable_type');
            $table->timestamps();

            $table->unsignedInteger('tag_id');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tag_gables');
        Schema::drop('tags');
    }
}
