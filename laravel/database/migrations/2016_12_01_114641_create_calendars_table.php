<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * 导师日历记录表
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function(Blueprint $table){
            $table->increments('id');
            $table->string('time_zone')->default('')->comment('时区');
            $table->date('date')->comment('日期');
            $table->enum('period', ['AM','PM'])->default('AM')->comment('上午/下午');
            $table->string('hour')->comment('小时');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendars');
    }
}
