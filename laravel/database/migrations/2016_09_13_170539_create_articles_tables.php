<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTables extends Migration
{
    /**
     * about infos.
     * 资讯信息模块
     * @return void
     */
    public function up()
    {
        //资讯表
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('article')->comment('类型：（article）普通文章/（activity）活动');
            $table->boolean('login_access')->default(false)->comment('是否需要登录查看');
            $table->string('author')->default('personal')->comment('文章发布者属性：（system）系统发文/（personal）个人发文');
            $table->string('status')->default('check')->comment('状态: draft（草稿）/check（审批）/publish（发布）/reject（退稿）/register（报名中）/underway（进行中）/finish （完成）');
            $table->string('title')->nullable()->comment('标题');
            $table->text('content')->nullable()->comment('正文');;
            $table->string('cover')->nullable()->comment('封面');
            $table->string('remark')->nullable()->comment('记录');
            $table->unsignedInteger('read_count')->default(0)->comment('浏览人数');
            $table->timestamps();

            $table->unsignedInteger('user_id')->nullable()->comment('发文人');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
