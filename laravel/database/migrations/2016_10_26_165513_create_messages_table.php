<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //留言信息表
        Schema::create('messages', function(Blueprint $table){
            $table->increments('id');
            // $table->enum('period',[
            //         'start', //启动服务
            //         'strategy', //制定策略
            //         'promotion', //提升背景
            //         'document', //创作文书
            //         'online_apply', //网络申请
            //         'visa', //准备签证
            //         'finish' //结束服务
            //     ])->default('start')->comment('不同阶段的留言内容');
            $table->string('period')->default('start')->comment('不同阶段的留言内容');
            $table->text('content')->comment('留言内容');
            $table->softDeletes();
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');
        });

        //查看消息记录表
        Schema::create('read_messages', function(Blueprint $table){
            $table->increments('id');
            // $table->enum('period',[
            //         'start', //启动服务
            //         'strategy', //制定策略
            //         'promotion', //提升背景
            //         'document', //创作文书
            //         'online_apply', //网络申请
            //         'visa', //准备签证
            //         'finish' //结束服务
            //     ])->default('start')->comment('不同阶段的留言');
            $table->string('period')->default('start')->comment('不同阶段的留言内容');
            $table->unsignedInteger('read_id')->nullable()->comment('记录上次浏览的最新ID');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('apply_id');
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
        Schema::drop('read_messages');
    }
}
