<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillsTables extends Migration
{
    /**
     * Run the migrations.
     * 用户技能表（用户表多对多关系）
     * @return void
     */
    public function up()
    {
        //技能表
        Schema::create('skills', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('技能名称');
            $table->timestamps();

            $table->unique('name');
        });

        //关联表
        Schema::create('user_skill', function(Blueprint $table){
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('skill_id');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skills');
        Schema::drop('user_skill');
    }
}
