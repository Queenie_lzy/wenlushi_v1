<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTagsTables extends Migration
{
    /**
     * Run the migrations.
     * 用户服务类别
     * @return void
     */
    public function up()
    {
        //服务项目表
        Schema::create('service_tags', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('服务名称');
            $table->timestamps();

            $table->unique('name');
        });

        //用户和服务项目的关联表
        Schema::create('user_service_tag', function(Blueprint $table){
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('service_tag_id');
            $table->foreign('service_tag_id')->references('id')->on('service_tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_service_tag');
        Schema::drop('service_tags');
    }
}
