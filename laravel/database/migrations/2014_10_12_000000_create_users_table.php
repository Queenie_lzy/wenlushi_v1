<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * 前台账号
     * @return void
     */
    public function up()
    {
        //用户表
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar')->nullable();
            $table->string('name');
            $table->string('nickname')->nullable()->comment('昵称');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('occupation')->nullable()->comment('职业');
            $table->smallInteger('duration')->default(0)->comment('圆桌会议时长（单位分）');
            $table->string('city')->default('')->comment('城市');
            $table->string('brief')->default('')->comment('个人简介');
            $table->string('cellphone_code')->comment('国家电话区号');
            $table->string('cellphone')->comment('手机号码');
            $table->string('contact_tool')->comment('聊天工具');
            $table->string('contact_tool_no')->comment('聊天号码');
            $table->enum('role', array_keys(trans('params.user.role')))->default('student');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        //教育经历表
        Schema::create('user_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('degree');
            $table->string('school', 100)->default('')->comment('毕业院校');
            $table->string('specialty', 100)->default('')->comment('专业');
            $table->string('date')->nullable();
            // $table->date('entrance_date')->comment('入学时间');
            // $table->date('graduation_date')->comment('毕业时间');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        //工作经历表
        Schema::create('user_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company', 100)->default('')->comment('公司');
            // $table->string('department', 100)->default('')->comment('部门');
            $table->string('description', 100)->default('')->comment('描述');
            $table->string('position', 100)->default('')->comment('职务');
            $table->string('date')->nullable();
            // $table->date('start_date');
            // $table->date('end_date');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        //各类考试成绩表
        Schema::create('user_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->json('school_work')->nullable()->comment('平均学业成绩');
            $table->json('english')->nullable()->comment('英语水平考试');
            $table->json('postgraduate')->nullable()->comment('研究生入学考试');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

       //专业技能表
       Schema::create('user_skills', function(Blueprint $table){
            $table->increments('id');
            $table->json('items')->nullable()->comment('用户技能');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
       });

        //用户其他补充材料表
        Schema::create('user_supplements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable()->comment('类型');
            $table->json('content')->nullable()->comment('内容');
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_supplements');
        Schema::drop('user_skills');
        Schema::drop('user_exams');
        Schema::drop('user_experiences');
        Schema::drop('user_educations');
        Schema::drop('users');
    }
}
