<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     * 圆桌会议/线上问答表
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function(Blueprint $table){
            $table->increments('id');
            $table->string('mode')->comment('形式（圆桌会议/线上留言）');
            $table->string('theme')->nullable()->comment('主题');
            $table->string('description')->nullable()->comment('描述');
            $table->string('answer')->nullable()->comment('解答/归纳总结');
            $table->json('session')->nullable()->comment('预约时间');
            $table->smallInteger('duration')->nullable()->comment('时长（单位分）');
            $table->string('status')->default('untake')->comment('状态');
            $table->string('remark')->nullable()->comment('记录（拒接原因）');
            $table->timestamps();

            $table->unsignedInteger('calendar_id')->nullable()->comment('开展时间');
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('tutor_id');
            $table->foreign('tutor_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('apply_id')->nullable();
            $table->foreign('apply_id')->references('id')->on('study_abroad_applies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meetings');
    }
}
