<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteConfiguresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_configures', function (Blueprint $table) {
            $table->string('id', 100);
            $table->string('local_use')->index()->comment('使用地方');
            $table->string('value')->nullable();
            $table->string('label')->nullable()->comment('input label');
            $table->json('meta')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_configures');
    }
}
