<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyAbroadSchoolSpecialtyTables extends Migration
{
    /**
     * Run the migrations.
     * 留学报告 （学校、专业）申请学校用表
     * @return void
     */
    public function up()
    {
        //学校表
        Schema::create('study_abroad_schools', function(Blueprint $table){
            $table->increments('id');
            $table->string('logo')->nullable()->comment('学校logo');
            $table->string('name')->comment('学校中文名');
            $table->string('en_name')->nullable()->comment('学校英文名');
            $table->enum('type', ['at_home', 'abroad'])->default('abroad')->comment('国内国外');
            $table->timestamps();

            $table->unique('name');
        });

        //专业表
        Schema::create('study_abroad_specialties', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('专业名');
            $table->enum('type', ['at_home', 'abroad'])->default('abroad')->comment('国内国外');
            $table->timestamps();

            $table->unique('name');
        });

        //院校专业信息表
        Schema::create('school_specialty_infos', function(Blueprint $table){
            $table->increments('id');
            $table->string('department')->nullable()->comment('院系名称');
            $table->json('grade')->nullable()->comment('成绩要求');
            $table->string('deadline_at')->nullable()->comment('截止时间');
            $table->string('inform_at')->nullable()->comment('结果通知');
            $table->string('link')->nullable()->comment('链接');
            $table->timestamps();

            $table->unsignedInteger('school_id');
            $table->foreign('school_id')->references('id')->on('study_abroad_schools')->onDelete('cascade');
            $table->unsignedInteger('specialty_id');
            $table->foreign('specialty_id')->references('id')->on('study_abroad_specialties')->onDelete('cascade');
            $table->unique(['school_id', 'specialty_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('study_abroad_schools');
        Schema::drop('study_abroad_specialties');
    }
}
