<div class="mail-body">
    <div class="mail-user">
        <img src="{{ $user->avatar }}" class="left" style="width: 60px;border-radius: 50%">
        <div class="right">
            <b>{{ $user->name }}</b>
        </div>
    </div>
    <div class="mail-content">
        <div>
            <h3>{{ implode(' - ', $path) }}</h3>
            <hr>
            @if (isset($messages))
                {{ $messages->content }}
            @endif
            @if (isset($process))
                <ul class="process">
                @foreach ($process as $key => $value)
                    @if ( array_key_exists($key,trans('params.process')) )
                        @foreach ((array)$value as $k => $v)
                        <li><span class="sub-process">{{trans('params.process.'.$key.'.'.$k)}}</span><span class="{{$v == 1 ? 'finish' : 'unfinish'}}">{{$v == 1 ? '已完成' : '未完成'}}</span></li>
                        @endforeach
                    @else
                        @foreach ((array)$value as $k => $v)
                        <li><span class="sub-process">{{$k}}</span><span class="{{$v == 1 ? 'finish' : 'unfinish'}}">{{$v == 1 ? '已完成' : '未完成'}}</span></li>
                        @endforeach
                    @endif
                @endforeach
                </ul>
            @endif
        </div>
        <div style="text-align: right">
            <h4 style="color: #999;">问路石</h4>
        </div>
    </div>
</div>

<style>
    .mail-body .mail-user {
        height: 70px;
        line-height: 70px;
    }

    .mail-body .mail-user .left {
        float:left;
        width: 70px;
    }

    .mail-body .mail-user .right {
        float:left;
        margin-left: 10px;
    }

    .mail-body .mail-content h3{
        color:#0056b7;
    }

    .mail-body .mail-content .process li{
        list-style: none
    }

    .mail-body .mail-content .process .sub-process{
        margin-right: 25px;
    }

    .mail-body .mail-content .process .finish{
        color:green;
    }
    .mail-body .mail-content .process .unfinish{
        color:orange;
    }
</style>