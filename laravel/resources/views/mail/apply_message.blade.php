@if ($isEdit)
{{ $user->name }}已修改了留学意愿与个人背景。请通过点击下面链接完成留学前景评估。
@else
{{ $user->name }}已提交了留学意愿与个人背景。请通过点击下面链接完成留学前景评估。
@endif
<br/>
<a href="{{ action('Web\DefaultController@getIndex') }}/#/auth/login">登录</a>
<br/>
<div style="text-align: right">
    <h4 style="color: #999;">问路石团队</h4>
</div>