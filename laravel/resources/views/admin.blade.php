<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="x5-page-mode" content="app"/>
    <meta name="browsermode" content="application">

    <title>问路石</title>
</head>
<body>
<div id="app"></div>
<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
<script src="js/admin.js"></script>
</body>
</html>
