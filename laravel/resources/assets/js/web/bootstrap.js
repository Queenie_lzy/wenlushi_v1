/* ============
 * Bootstrap File
 * ============
 *
 * Will configure and bootstrap the application
 */

/* ============
 * Lodash
 * ============
 *
 * A modern JavaScript utility library delivering modularity, performance, & extras
 *
 * https://lodash.com/docs/4.16.1
 * http://www.css88.com/doc/lodash/
 */
// import _ from 'lodash';
// window._ = _;


/* ============
 * jQuery
 * ============
 *
 * Require jQuery
 *
 * http://jquery.com/
 */
import jQuery from 'jquery';
window.$ = window.jQuery = jQuery;

/* ============
 * js-cookie
 * ============
 *
 * Require js-cookie
 *
 * https://github.com/js-cookie/js-cookie
 */
window.Cookies = require('js-cookie');

/* ============
 * Vue
 * ============
 *
 * Vue.js is a library for building interactive web interfaces.
 * It provides data-reactive components with a simple and flexible API.
 *
 * http://rc.vuejs.org/guide/
 */
import Vue from 'vue';

Vue.config.debug = true;

/* ============
 * Vue Resource
 * ============
 *
 * Vue Resource provides services for making web requests and handle
 * responses using a XMLHttpRequest or JSONP.
 *
 * https://github.com/vuejs/vue-resource/tree/master/docs
 */
import VueResource from 'vue-resource'

Vue.use(VueResource);
Vue.http.headers.common.Accept = 'application/json';
Vue.http.interceptors.push((request, next) => {
  request.headers['X-XSRF-TOKEN'] = Cookies.get('XSRF-TOKEN');
  next();
});

/* ============
 * vue lazyload
 * ============
 *
 * Vue module for lazyloading images in your applications
 *
 * https://github.com/hilongjw/vue-lazyload
 */
// import VueLazyload from 'vue-lazyload'
// Vue.use(VueLazyload, {
//   preLoad: 1.3,
//   error: 'dist/error.png',
//   loading: 'dist/loading.gif',
//   try: 1
// });

import './pages'

export default {
  // router,
};
