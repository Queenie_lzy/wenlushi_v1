import $ from 'jquery'

export default class {
  constructor() {
    this.$page = $('#news');
    if (this.$page.length == 1) {
      this.initTabs();
    }
  }

  initTabs() {
    let $page = this.$page;
    $page.find('.news-tag .more').on('click', function () {
      $page.find('.news-tag .tag-partial').hide();
      $page.find('.news-tag .tag-all').show();
      $(this).hide();
    });
  }
}
