import $ from 'jquery'

export default class {
  constructor() {
    this.$page = $('#user');
    if (this.$page.length == 1) {
      this.initTabs();
    }
  }

  initTabs() {
    let $page = this.$page;
    $page.find('.nav li').on('click', function () {
      $page.find('.nav-tab')
        .eq($(this).index())
        .addClass('active')
        .siblings().removeClass('active');
      $(this).addClass('active').siblings().removeClass('active');
    });
  }
}
