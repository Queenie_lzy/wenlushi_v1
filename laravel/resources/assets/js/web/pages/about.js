import $ from 'jquery'

export default class {
  constructor() {
    this.$page = $('#about');
    if (this.$page.length == 1) {
      this.initTabs();
    }
  }

  initTabs() {
    let $page = this.$page;
    $page.find('.about-nav li').on('click', function () {
      $page.find('.about-content .about-plane')
        .eq($(this).index())
        .addClass('active')
        .siblings().removeClass('active');
      $(this).addClass('active').siblings().removeClass('active');
    });
  }
}
