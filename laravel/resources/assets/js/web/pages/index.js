import about from './about'
import news from './news'
import user from './user'
import serviceApply from './service_apply'

export default {
  about: new about(),
  news: new news(),
  user: new user(),
  serviceApply: new serviceApply()
}