import $ from 'jquery'

export default class {
  constructor() {
    this.$page = $('#service-apply');
    if (this.$page.length == 1) {
      this.initTabs();
    }
  }

  initTabs() {
    let $page = this.$page;
    $page.find('.content .add-work').on('click', function () {
      var init = $page.find('.content .work-form').eq(0);
      var copy = init.clone();
      init.after(copy);
    });
    $page.find('.content .add-education').on('click', function () {
      var init = $page.find('.content .education-form').eq(0);
      var copy = init.clone();
      init.after(copy);
    });
  }
}
