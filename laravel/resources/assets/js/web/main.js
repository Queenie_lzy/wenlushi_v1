import Vue from 'vue'
import './bootstrap'

const lazyLoading = (path) => {
  return (resolve) => {
    require([`${path}`], resolve)
  }
};

const app = new Vue({
  el: 'body',
  components: {
    OwlCarouselBanner: lazyLoading('./components/OwlCarouselBanner.vue'),
    OwlCarouselSlideGallery: lazyLoading('./components/OwlCarouselSlideGallery.vue'),
    CircleProgress: lazyLoading('./components/CircleProgress.vue')
  }
});

