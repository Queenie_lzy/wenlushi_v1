<?php
/**
 * 静态参数配置文件
 */
return [
    'process' => [
        '综合留学申请' => ['start','strategy','promotion','document','online_apply','visa','feedback','finish','school_counseling','meeting'],
        '基础留学申请' => ['start','strategy','document','online_apply','visa','feedback','finish','school_counseling'],
        '文书点石成金' => ['start','strategy','document','feedback','finish','school_counseling','meeting'],
        '文书基础写作' => ['start','strategy','document','feedback','finish','school_counseling'],
        '文书加急写作' => ['start','strategy','document','feedback','finish'],
        '规划与指导'   => ['start','strategy','visa','feedback','finish','school_counseling','meeting']
    ],

    'translate' => [
        'start'         => '启动服务',
        'strategy'      => '制定策略',
        'promotion'     => '提升背景',
        'document'      => '创作文书',
        'online_apply'  => '网络申请',
        'visa'          => '准备签证',
        'feedback'      => '调查反馈',
        'finish'        => '结束服务',
        'meeting'       => '需要圆桌会议',
        'school_counseling' => '选校辅导',
    ],

    'role' => [
        'student' => '用户',
        'service' => '客服',
        'tutor' => '导师',
        'admin' => '质控'
    ]
];