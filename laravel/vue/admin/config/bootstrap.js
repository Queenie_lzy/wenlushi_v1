/* ============
 * Bootstrap File
 * ============
 *
 * Will configure and bootstrap the application
 */

/* ============
 * js-cookie
 * ============
 *
 * Require js-cookie
 *
 * https://github.com/js-cookie/js-cookie
 */
window.Cookies = require('js-cookie');

/* ============
 * Vue
 * ============
 *
 * Vue.js is a library for building interactive web interfaces.
 * It provides data-reactive components with a simple and flexible API.
 *
 * http://rc.vuejs.org/guide/
 */
import Vue from 'vue'

Vue.config.silent = true;
Vue.config.devtools = true;

/* ============
 * Vue Laravel Validator
 * ============
 *
 * https://github.com/MetinSeylan/Vue-Laravel-Validator
 */
import VueLaravelValidator from '../plugin/vue-laravel-validator'

Vue.use(VueLaravelValidator);

/* ============
 * Vue Resource
 * ============
 *
 * Vue Resource provides services for making web requests and handle
 * responses using a XMLHttpRequest or JSONP.
 *
 * https://github.com/vuejs/vue-resource/tree/master/docs
 */
import VueResource from 'vue-resource'

Vue.use(VueResource);

Vue.http.options.root = 'admin';
// Vue.http.headers.common['X-XSRF-TOKEN'] = Cookies.get('XSRF-TOKEN');

Vue.http.interceptors.push((request, next) => {
  request.headers.map['X-XSRF-TOKEN'] = [Cookies.get('XSRF-TOKEN')];
  next((response) => {
    // When the token is invalid, log the user out
    if (response.status === 401 && !router.currentRoute.meta.guest) {
      store.dispatch('clearUser');
      router.push({
        name: 'Login',
      });
    }
  });
});

/* ============
 * Vue Router
 * ============
 *
 * The official Router for Vue.js. It deeply integrates with Vue.js core
 * to make building Single Page Applications with Vue.js a breeze.
 *
 * http://router.vuejs.org/en/index.html
 */
import VueRouter from 'vue-router';

Vue.use(VueRouter);
const router = new VueRouter(require('./router').default);

/* ============
 * Vuex Store
 * ============
 *
 * The store of the application
 *
 * http://vuex.vuejs.org/en/index.html
 */
import Vuex from 'vuex'

Vue.use(Vuex);
const store = new Vuex.Store(require('../store').default);

/* ============
 * Vuex Router Sync
 * ============
 *
 * Effortlessly keep vue-Router and vuex store in sync.
 *
 * https://github.com/vuejs/vuex-router-sync/blob/master/README.md
 */
import VuexRouterSync from 'vuex-router-sync'

VuexRouterSync.sync(store, router);

/* ============
 * bootstrap
 * ============
 *
 * http://v3.bootcss.com/
 */
import 'bootstrap/dist/js/bootstrap.min'

/* ============
 * toastr
 * ============
 *
 * https://github.com/CodeSeven/toastr
 */
let toastr = require('toastr/build/toastr.min.js');
toastr.options = {
  // "closeButton": false,
  // "debug": false,
  // "newestOnTop": false,
  // "progressBar": false,
  "positionClass": "toast-top-center",
  // "preventDuplicates": false,
  // "onclick": null,
  // "showDuration": "300",
  // "hideDuration": "1000",
  // "timeOut": "5000",
  // "extendedTimeOut": "1000",
  // "showEasing": "swing",
  // "hideEasing": "linear",
  // "showMethod": "fadeIn",
  // "hideMethod": "fadeOut"
};
window.toastr = toastr;

import '../styles/material_admin/js/App'
// import '../styles/material_admin/js/AppForm'

export default {store, router}
