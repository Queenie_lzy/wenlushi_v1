const lazyLoading = (name, index = false) => () => System.import(`../pages/${name}${index ? '/index' : ''}.vue`);
// const lazyLoading = (name, index = false) => require(`../pages/${name}${index ? '/index' : ''}.vue`);

export default {
    root: '/admin',
    base: __dirname,
    // mode: 'history',
    //linkActiveClass: 'is-active',
    routes: [
      {
          name: 'Home',
          path: '/',
          component: lazyLoading('Home')
      },
      {
          name: 'My',
          path: '/my',
          component: lazyLoading('My')
      },
      {
          name: 'Login',
          path: '/login',
          component: lazyLoading('Login'),
          meta: {
              guest: true
          }
      },
      {
        name: 'users',
        path: '/users',
        component: lazyLoading('users', true),
        redirect: {
          name: 'users.list'
        },
        children: [
          {
            name: 'users.list',
            path: 'list',
            component: lazyLoading('users/list'),
            meta: {

            },
          },
          {
            name: 'users.add',
            path: 'add',
            component: lazyLoading('users/form'),
            meta: {

            },
          },
          {
            name: 'users.edit',
            path: 'edit/:id',
            component: lazyLoading('users/form'),
            meta: {

            },
          },
        ]
      },
      {
        name: 'Schools',
        path: '/schools',
        component: lazyLoading('schools', true),
        redirect: {
          name: 'schools.list'
        },
        children: [
          {
            name: 'schools.list',
            path: 'list',
            component: lazyLoading('schools/list'),
            meta: {

            },
          },
          {
            name: 'schools.add',
            path: 'add',
            component: lazyLoading('schools/form'),
            meta: {

            },
          },
          {
            name: 'schools.edit',
            path: 'edit/:id',
            component: lazyLoading('schools/form'),
            meta: {

            },
          },
          {
            name: 'schools.schoolLists',
            path: 'school-lists',
            component: lazyLoading('schools/schoolLists'),
            meta: {

            },
          },
          {
            name: 'schools.addSchool',
            path: 'add-school',
            component: lazyLoading('schools/addSchool'),
            meta: {

            },
          },
          {
            name: 'schools.editSchool',
            path: 'edit-school/:id',
            component: lazyLoading('schools/addSchool'),
            meta: {

            },
          },
          {
            name: 'schools.specialtyLists',
            path: 'specialty-lists',
            component: lazyLoading('schools/specialtyLists'),
            meta: {

            },
          },
          {
            name: 'schools.addSpecialty',
            path: 'add-specialty',
            component: lazyLoading('schools/addSpecialty'),
            meta: {

            },
          },
          {
            name: 'schools.editSpecialty',
            path: 'edit-specialty/:id',
            component: lazyLoading('schools/addSpecialty'),
            meta: {

            },
          },
        ]
      },
      {
        path: '*',
        redirect: '/'
      }
    ]
}

