export default {
  roles: {
    'student': '用户',
    'service': '客服',
    'tutor': '导师',
    'admin': '质控'
  },
}
