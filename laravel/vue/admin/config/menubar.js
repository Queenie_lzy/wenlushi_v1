export default [
  {
    to: '/dashboard',
    name: '信息面板',
    icon: 'glyphicon glyphicon-home',
  },
  {
    name: '用户管理',
    icon: 'glyphicon glyphicon-user',
    child: [
      {
        to: '/users/list',
        name: '用户列表',
      },
      {
        to: '/users/add',
        name: '新增用户',
      }
    ]
  },
  {
    name: '学校专业管理',
    icon: 'glyphicon glyphicon-education',
    child: [
      {
        to: '/schools/list',
        name: '院校专业关联列表',
      },
      {
        to: '/schools/add',
        name: '添加院校专业关联',
      },
      {
        to: '/schools/school-lists',
        name: '学校列表',
      },
      {
        to: '/schools/add-school',
        name: '添加学校',
      },
      {
        to: '/schools/specialty-lists',
        name: '专业列表',
      },
      {
        to: '/schools/add-specialty',
        name: '添加专业',
      },
    ]
  }
]
