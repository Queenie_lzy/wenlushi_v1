import Vue from 'vue'
const autosize = require('autosize/dist/autosize.min');

Vue.directive('autosize', {
  bind: function (el, binding, vnode) {
    autosize(el)
  },

  unbind: function(el, binding, vnode, oldVnode) {
    autosize.destroy(el)
  }
});
