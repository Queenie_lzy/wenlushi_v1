export default {
    updateUser(state, payload){
        if (!payload) {
            payload = null;
        }
        state.user = payload;

        const key = 'admin_user';
        if (payload) {
            sessionStorage.setItem(key, JSON.stringify(payload))
        } else {
            sessionStorage.removeItem(key);
        }
    }
}
