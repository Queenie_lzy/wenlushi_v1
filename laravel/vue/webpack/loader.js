module.exports = {
    vue: {
        loaders: {
            css: 'vue-style-loader?sourceMap!css-loader?sourceMap',
            scss: 'vue-style-loader?sourceMap!css-loader?sourceMap!sass-loader?sourceMap',
        },
        autoprefixer: {
            browsers: [
                'last 2 versions',
                'Chrome >= 30',
                'Firefox >= 30',
                'ie >= 10',
                'Safari >= 8'
            ]
        }
    }
};
