let config = require('./base')

config.devServer = {
    historyApiFallback: true,
    compress: true,
    port: 8002
}

module.exports = config