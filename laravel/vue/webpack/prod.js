const webpack = require('webpack')

let config = require('./base')

config.devtool = '#eval';
// http://vue-loader.vuejs.org/en/workflow/production.html
config.plugins = (config.plugins || [])
    .concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            comments: false
        }),
    ])

module.exports = config
