const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: {
        web: './vue/web/main.js',
        admin: './vue/admin/main.js'
    },
    output: {
        path: path.resolve(__dirname, '../../public'),
        publicPath: '',
        filename: 'js/[name].js',
    },
    resolve: {
        extensions: ['.js', '.vue', '.css', '.json'],
        alias: require('./alias')
    },
    module: {
        //noParse: /node_modules\/(jquey|moment|chart\.js)/,
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(css|scss|sass)$/,
                loader: "vue-style-loader?sourceMap!css-loader?sourceMap!sass-loader?sourceMap"
            },
            {
                test: /\.js$/,
                loader: 'babel-loader?cacheDirectory',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                query: {
                    name: 'images/[name].[ext]'
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: 'fonts/[name].[ext]'
                }
            }
        ]
    },
    performance: {
        hints: false, //关闭警告
    },
    plugins: [
        new webpack.LoaderOptionsPlugin(require('./loader')),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": 'jquery',
        }),
        new (require('webpack-notifier'))({
            alwaysNotify: true,
        })
    ],
    externals: {
        'jquery': 'jQuery',
    },
    devtool: '#cheap-module-eval-source-map' //cheap-module-eval-source-map:开发 | cheap-module-source-map:生产 | eval:快
};
