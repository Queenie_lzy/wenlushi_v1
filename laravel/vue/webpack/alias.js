module.exports = {
    'vue': 'vue/dist/vue.min',
    'vue-router': 'vue-router/dist/vue-router.min',
    'vue-resource': 'vue-resource/dist/vue-resource.min',
    'vuex': 'vuex/dist/vuex.min',
    // 'jquery': 'jquery/dist/jquery.min',
    // 'lodash': 'lodash/dist/lodash.core.min',
    // 'lodash': 'lodash/dist/lodash.min',
    // 'bootstrap': 'bootstrap/dist/js/bootstrap.min'
};
