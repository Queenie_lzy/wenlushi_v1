export default {
    updateUser(state, payload){
        if (!payload) {
            payload = null;
        }
        state.user = payload;

        const key = 'web_user';
        if (payload) {
            sessionStorage.setItem(key, JSON.stringify(payload))
        } else {
            sessionStorage.removeItem(key);
        }
    },
    setStudyAbroadApplyId(state, payload){
        state.study_abroad_apply_id = payload
    },
    addLoading(state){
        state.isLoading++;
    },
    removeLoading(state, payload){
        if (state.isLoading > 0) {
            state.isLoading--;
            if(payload){
                state.isLoading = 0;
            }
        }
    }
}
