export default {
    user: (state) => state.user,
    current_role: (state) => {
        if(state.user){
            return state.user.role;
        }
        return 'guest'
    },
    current_id: (state) => {
        if(state.user){
            return state.user.id;
        }
        return null
    },
    isLoading: (state) => state.isLoading,
}
