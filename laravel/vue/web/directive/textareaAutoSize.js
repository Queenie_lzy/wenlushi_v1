import Vue from 'vue'
const autosize = require('autosize/dist/autosize.min');

Vue.directive('autosize', {
    bind(el, binding, vnode) {
        autosize(el)
    },

    unbind(el, binding, vnode, oldVnode) {
        autosize.destroy(el)
    }
});
