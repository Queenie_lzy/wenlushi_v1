/**
 * getCalendar and setCalendar's common methods
 */
export default {
    methods:{
        changeDate(date){
            if(date) {
                this.$set(this, 'date', date);
            }
        },
        numFormat(v){
            return String(v).length == 1 ? '0'+v : v ;
        },
        getHour(v){
            return v == 0 ? 12 : (String(v).length == 1 ? '0'+v : v );
        },
        close(){
            this.$emit('close');
        },
        getTitle(hour, period){
            let next_hour = Number(hour)+1,
                date = new Date(this.date.date+' 23:59:59'),
                ch_time = '',
                en_time = '';

            en_time = '（美中）'+this.date.date +' '+ this.getHour(hour)+':00 '+period+' - '+this.getHour(next_hour)+':00 '+period;
            if(period == 'AM') {
                if(hour >= 10) {
                   ch_time =  '（北京）'+date.getFullYear()+'-'+this.numFormat(date.getMonth()+1)+'-'+this.numFormat(date.getDate() + 1);
                   ch_time  += ' '+this.getHour(Number(hour) - 10)+':00 AM - ';
                   ch_time += this.getHour(Number(hour) - 9)+':00 AM';
                } else {
                    ch_time = '（北京）'+ this.date.date +' '+ this.getHour(Number(hour) + 2)+':00 PM - ';
                    ch_time += this.getHour(Number(hour) + 3)+':00 PM';
                }
            }
            if(period == 'PM') {
                ch_time =  '（北京）'+date.getFullYear()+'-'+this.numFormat(date.getMonth()+1)+'-'+this.numFormat(date.getDate() + 1);
                if(hour >= 10) {
                   ch_time += ' '+this.getHour(Number(hour) - 10)+':00 PM - ';
                   ch_time += this.getHour(Number(hour) - 9)+':00 PM';
                } else {
                    ch_time += ' '+ this.getHour(Number(hour) + 2)+':00 AM - ';
                    ch_time += this.getHour(Number(hour) + 3)+':00 AM';
                }
            }
            return en_time+'\n'+ch_time;
        }
    }
}