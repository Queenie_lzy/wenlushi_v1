/**
 * the common methods to meeting module
 */
export default {
    methods:{
        getHour(v){
            return v == 0 ? 12 : (String(v).length == 1 ? '0'+v : v );
        },
        numFormat(v){
            return String(v).length == 1 ? '0'+v : v ;
        },
        getTimeFormat(date, hour, period){
            let _date = new Date(date+' 23:59:59'),
            ch_time = '',
            en_time = '';

            en_time = '（美中）'+date +' '+ this.getHour(hour)+':00 '+period;
            if(period == 'AM') {
                if(hour >= 10) {
                   ch_time =  '（北京）'+_date.getFullYear()+'-'+this.numFormat(_date.getMonth()+1)+'-'+this.numFormat(_date.getDate() + 1);
                   ch_time  += ' '+this.getHour(Number(hour) - 10)+':00 AM';
                } else {
                    ch_time = '（北京）'+ date +' '+ this.getHour(Number(hour) + 2)+':00 PM';
                }
            }
            if(period == 'PM') {
                ch_time =  '（北京）'+_date.getFullYear()+'-'+this.numFormat(_date.getMonth()+1)+'-'+this.numFormat(_date.getDate() + 1);
                if(hour >= 10) {
                   ch_time += ' '+this.getHour(Number(hour) - 10)+':00 PM';
                } else {
                    ch_time += ' '+ this.getHour(Number(hour) + 2)+':00 AM';
                }
            }
            return ch_time+' /'+en_time;
        }
    }
}