/**
 * Created by stevie on 2016/12/8.
 */
export default {
/*    beforeRouteLeave (to, from, next) {
        $.confirm({
            title: '确定离开当前页面？',
            content: '请确认下有无数据需要保存。',
            buttons: {
                '确定': function () {
                    next();
                },
                '取消': function () {
                    next(false);
                },
            }
        });
    },*/
    methods:{
        isLock(i){
            if(typeof i == 'object'){
                return !!i.lock
            }else{
                if(this.lock[i] == undefined)this.$set(this.lock, i, 0);
                return !!this.lock[i]
            }
        },
        setLock(i){
            if(this.readonly)return;
            if(typeof i == 'object'){
                this.$set(i, 'lock', i.lock ? 0 : 1);
            }else{
                this.$set(this.lock, i, this.lock[i] ? 0 : 1);
            }
        },
        isReadonly(i){
            return this.isLock(i) || this.readonly
        },
    }
}