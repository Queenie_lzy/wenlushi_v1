/**
 * the common methods to meeting module
 */
export default {
    methods:{
        doSurvey(pathName, userId){
            let o = this;
            $.confirm({
                title: '<h3>调查</h3>您是通过什么方式了解问路石服务的？',
                content: '<form id="survey"><div class="radio"><label><input type="radio" name="way" value="wechat">微信公众号</label></div><div class="radio"><label><input type="radio" name="way" value="network">互联网</label></div><div class="radio"><label><input type="radio" name="way" value="brochure">宣传手册</label></div><div class="radio"><label><input type="radio" name="way" value="friend">朋友</label></div><div class="radio"><label><input type="radio" name="way" value="activity">近期活动</label></div><div class="radio"><label><input type="radio" name="way" value="other">其他</label></div>',
                buttons: {
                    ok: {
                        text: '确定',
                        action() {
                            let way = this.$content.find('input[name=way]:checked').val();
                            let data = {};
                            data[way] = 1;
                            o.$http.post('my/survey', data).then((r)=>{
                                if(pathName == 'Service'){
                                    o.$router.push({
                                        name: pathName,
                                    });
                                }
                                if(pathName == 'User') {
                                    o.$router.push({
                                        name:'Service.AuditingUser',
                                        params:{id: userId}
                                    });
                                }
                            });
                        }
                    }
                },
            });
        }
    }
}