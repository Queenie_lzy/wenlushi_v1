/**
 * the common methods to meeting module
 */
export default {
    methods:{
        check(user, goback = true){
            if(!user.name && !user.cellphone){
                $.alert({
                    title: '个人资料不完善',
                    content: '问路石需要发文者完整的个人资料，请点击“继续”填写个人资料。',
                    columnClass: 'col-md-5 col-md-offset-4',
                    buttons: {
                        close:{
                            btnClass: 'bg-white',
                            text: '取消',
                        },
                        ok: {
                            text: '继续',
                            action: () => {
                                this.$router.push('/my');
                            }
                        }
                    },
                });
                if(goback) {
                    this.$router.go(-1);
                }
                return false;
            }
            return true;
        },
    }
}