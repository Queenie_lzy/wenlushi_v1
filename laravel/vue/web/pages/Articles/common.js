/**
 * the common methods to article detail
 */
import {mapGetters} from 'vuex'
export default {
    computed:{
            ...mapGetters([
                'user'
            ]),
            isAdmin(){
                if(this.user){
                    return this.user.role == 'admin' ? true : false;
                }
            },
            isEdit(){
                if(this.isAdmin){
                    return this.article.status == 'reject' ? false : true;
                } else {
                    return this.article.status == 'publish' ? false : true;
                }
            },
            created(){
                return this.article.created_at ? this.article.created_at.substr(0, 10) : null;
            },
            author(){
                if(this.article.author == 'system') {
                    return '问路石';
                } else {
                    return this.article.user.nickname ? this.article.user.nickname : '匿名';
                }
            },
            tags(){
                let tags = this.article.tags;
                if(!tags) return false;

                return tags.split(",");
            }
        },
    methods:{
        getData(){
            this.$http.get('my/article/'+this.$route.params.id).then((r)=>{
                this.article = r.body
            });
        },
    }
}