const lazyLoading = (name, index = false) => () => System.import(`../pages/${name}${index ? '/index' : ''}.vue`);
// const lazyLoading = (name, index = false) => require(`../pages/${name}${index ? '/index' : ''}.vue`);
import {store} from './bootstrap'

export default {
    // root: '',
    base: __dirname,
    // mode: 'history',
    // linkActiveClass: 'is-active',
    routes: [
        {
            name: 'Service',
            path: '/service',
            component: lazyLoading('Service', true),
            redirect: {
                name: 'Service.Default'
            },
            children: [
                {
                    name: 'Service.Default',
                    path: '',
                    component: lazyLoading('Service/Default'),
                },
                {
                    name: 'StudyAbroadDetail',
                    path: 'study-abroad-Detail',
                    component: lazyLoading('Service/StudyAbroadDetail'),
                },
                {
                    name: 'AcademicPaperDetail',
                    path: 'academic-paper-Detail',
                    component: lazyLoading('Service/AcademicPaperDetail'),
                },
                {
                    name: 'case',
                    path: 'case',
                    component: lazyLoading('Service/case'),
                },
                {
                    name: 'StudyAbroadForm',
                    path: 'study-abroad-form',
                    component: lazyLoading('Service/StudyAbroadForm'),
                    meta: {
                        auth: true
                    },
                },
                {
                    name: 'Service.AuditingUser',
                    path: 'auditing-user/:id',
                    component: lazyLoading('Service/AuditingUser'),
                    meta: {
                        auth: true
                    },
                },
                {
                    name: 'Service.quizInfo',
                    path: 'quiz/:status/:id',
                    component: lazyLoading('Service/quizInfo'),
                    meta: {
                        auth: true
                    },
                },
                {
                    name: 'Service.Admin',
                    path: 'admin',
                    component: lazyLoading('Service/Admin', true),
                    redirect(){
                        let ret = {
                            name: 'Admin.StudyAbroad',
                            params:{
                                status: 'service-in'
                            }
                        };
                        if(store.state.user){
                            switch (store.state.user.role){
                                case 'admin':
                                    ret.params.status = 'sales-in';
                                    break;
                                case 'tutor':
                                    ret.params.status = 'service-in';
                                    break
                            }
                        }
                        return ret
                    },
                    children: [
                        {
                            name: 'Admin.StudyAbroad',
                            path: 'study-abroad/:status',
                            component: lazyLoading('Service/Admin/StudyAbroad'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'Admin.quiz',
                            path: 'quiz/:status',
                            component: lazyLoading('Service/Admin/quiz'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'Admin.quizInfo',
                            path: 'quiz/:status/:id',
                            component: lazyLoading('Service/Admin/quizInfo'),
                            meta: {
                                auth: true
                            },
                        }
                    ]
                },
                {
                    name: 'Service.Issue',
                    path: 'issue',
                    component: lazyLoading('Service/Issue', true),
                    redirect:{
                        name: 'Issue.all'
                    },
                    children: [
                        {
                            name: 'Issue.all',
                            path: 'all',
                            component: lazyLoading('Service/Issue/all'),
                            meta: {
                                auth: true
                            },
                        }
                    ]
                },
                {
                    name: 'Service.Group',
                    path: 'group',
                    component: lazyLoading('Service/Group', true),
                    redirect:{
                        name: 'Group.default'
                    },
                    children: [
                        {
                            name: 'Group.default',
                            path: '',
                            component: lazyLoading('Service/Group/Default'),
                            meta: {
                                // auth: true
                            }
                        },
                        {
                            name: 'Group.search',
                            path: 'search/:searchword',
                            component: lazyLoading('Service/Group/search'),
                            meta: {
                                // auth: true
                            }
                        },
                        {
                            name: 'Group.quiz',
                            path: 'quiz/:tutorId/:type/:mode',
                            component: lazyLoading('Service/Group/quiz'),
                            meta: {
                                auth: true
                            }
                        },
                        {
                            name: 'Group.edit',
                            path: 'edit/:id',
                            component: lazyLoading('Service/Group/quiz'),
                            meta: {
                                auth: true
                            }
                        }
                    ]
                },
                {
                    name: 'Service.study_service',
                    path: 'study_service/:id',
                    component: lazyLoading('Service/study_service', true),
                    redirect: {
                        name: 'Service.UserStudyAbroadReport',
                    },
                    children: [
                        {
                            name: 'Service.UserStudyAbroadReport',
                            path: 'report',
                            component: lazyLoading('Service/study_service/report'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'StartService',
                            path: 'start-service',
                            component: lazyLoading('Service/study_service/startService'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'strategy',
                            path: 'strategy',
                            component: lazyLoading('Service/study_service/strategy'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'promotion',
                            path: 'promotion',
                            component: lazyLoading('Service/study_service/promotion'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'produce',
                            path: 'produce',
                            component: lazyLoading('Service/study_service/produce'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'network',
                            path: 'online-apply',
                            component: lazyLoading('Service/study_service/onlineApply'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'visa',
                            path: 'visa',
                            component: lazyLoading('Service/study_service/visa'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'finish',
                            path: 'finish',
                            component: lazyLoading('Service/study_service/finish'),
                            meta: {
                                auth: true
                            },
                        },
                        {
                            name: 'survey',
                            path: 'survey',
                            component: lazyLoading('Service/study_service/survey'),
                            meta: {
                                auth: true
                            },
                        },
                    ]
                }
            ]
        },
        {
            name: 'About',
            path: '/about',
            component: lazyLoading('About', true),
            redirect: {
                name: 'About.default'
            },
            children: [
                {
                    name: 'About.default',
                    path: '',
                    component: lazyLoading('About/default'),
                },
                {
                    name: 'About.founder',
                    path: 'founder',
                    component: lazyLoading('About/founder'),
                },
                {
                    name: 'About.tutor',
                    path: 'tutor',
                    component: lazyLoading('About/tutor'),
                },
                {
                    name: 'About.student',
                    path: 'student',
                    component: lazyLoading('About/student'),
                },
            ]
        },
        {
            name: 'CustomerService',
            path: '/customer-service',
            component: lazyLoading('CustomerService', true),
            redirect: {
                name: 'CustomerService.default'
            },
            children: [
                {
                    name: 'CustomerService.default',
                    path: 'index',
                    component: lazyLoading('CustomerService/default'),
                },
                {
                    name: 'CustomerService.create',
                    path: 'create',
                    component: lazyLoading('CustomerService/form'),
                },
                {
                    name: 'CustomerService.edit',
                    path: 'edit/:id',
                    component: lazyLoading('CustomerService/form'),
                },
            ]
        },
        {
            name: 'Statistics',
            path: '/statistics',
            component: lazyLoading('Statistics'),
        },
        {
            name: 'Notice',
            path: '/notice/:type/:email',
            component: lazyLoading('Notice'),
        },
        {
            name: 'Reset',
            path: '/reset',
            component: lazyLoading('Reset'),
        },
        {
            name: 'Articles',
            path: '/articles',
            component: lazyLoading('Articles', true),
            redirect: {
                name: 'Articles.default'
            },
            children: [
                {
                    name: 'Articles.default',
                    path: '',
                    component: lazyLoading('Articles/default'),
                },
                {
                    name: 'Articles.show',
                    path: 'show/:id',
                    component: lazyLoading('Articles/show'),
                },
                {
                    name: 'Articles.review',
                    path: 'review/:id',
                    component: lazyLoading('Articles/review'),
                },
                {
                    name: 'Articles.check',
                    path: 'check/:id',
                    component: lazyLoading('Articles/check'),
                },
                {
                    name: 'Articles.create',
                    path: 'create',
                    component: lazyLoading('Articles/form'),
                },
                {
                    name: 'Articles.edit',
                    path: 'edit/:id',
                    component: lazyLoading('Articles/form'),
                },
                {
                    name: 'Articles.My',
                    path: 'my',
                    component: lazyLoading('Articles/My', true),
                    redirect: {
                        name: 'ArticlesMy.default'
                    },
                    children: [
                        {
                            name: 'ArticlesMy.default',
                            path: 'index',
                            component: lazyLoading('Articles/My/default'),
                            meta: {
                                auth: true
                            },
                        }
                    ]
                },
            ]
        },
        {
            name: 'My',
            path: '/my',
            component: lazyLoading('My', true),
            redirect: {
                name: 'my.resume'
            },
            children: [
                {
                    name: 'my.account',
                    path: 'account',
                    component: lazyLoading('My/account'),
                    meta: {
                        auth: true
                    },
                },
                {
                    name: 'my.resume',
                    path: 'resume',
                    component: lazyLoading('My/resume'),
                    meta: {
                        auth: true
                    },
                },
            ]
        },
        {
            name: 'Auth',
            path: '/auth',
            component: lazyLoading('Auth', true),
            redirect: {
                name: 'Login'
            },
            children: [
                {
                    name: 'Login',
                    path: 'login',
                    component: lazyLoading('Auth/Login'),
                    meta: {
                        isHideHeader: true
                    },
                },
                {
                    name: 'Register',
                    path: 'register',
                    component: lazyLoading('Auth/Register'),
                    meta: {
                        isHideHeader: true
                    },
                }
            ]
        },
        {
            path: '*',
            redirect: '/service'
        }
    ],
}
