/* ============
 * Bootstrap File
 * ============
 *
 * Will configure and bootstrap the application
 */

/* ============
 * js-cookie
 * ============
 *
 * Require js-cookie
 *
 * https://github.com/js-cookie/js-cookie
 */
window.Cookies = require('js-cookie');
// window._  = require('lodash/lodash.min');
// window._  = require('lodash/core.min');

/* ============
 * Vue
 * ============
 *
 * Vue.js is a library for building interactive web interfaces.
 * It provides data-reactive components with a simple and flexible API.
 *
 * http://rc.vuejs.org/guide/
 */
import Vue from 'vue'
Vue.config.silent = true;
Vue.config.devtools = true;

/* ============
 * Vue Resource
 * ============
 *
 * Vue Resource provides services for making web requests and handle
 * responses using a XMLHttpRequest or JSONP.
 *
 * https://github.com/vuejs/vue-resource/tree/master/docs
 */
import VueResource from 'vue-resource'

Vue.use(VueResource);
// Vue.http.options.root = '/api';
Vue.http.interceptors.push((request, next) => {
    if (!request.hideLoading) {
        store.commit('addLoading');
    }
    request.headers.map['X-XSRF-TOKEN'] = [Cookies.get('XSRF-TOKEN')];
    // next();
    next((response) => {
        // When the token is invalid, log the user out
        if (response.status === 401 && router.currentRoute.meta.auth) {
            store.dispatch('clearUser');
            router.push({
                name: 'Login',
            });
        }
        if (!request.hideLoading) {
            store.commit('removeLoading');
        }
        // if(response.status === 404){
        //     router.go(-1);
        //     $.alert('找不到数据')
        // }
    });
});

/* ============
 * Vuex Store
 * ============
 *
 * The store of the application
 *
 * http://vuex.vuejs.org/en/index.html
 */
import Vuex from 'vuex'

Vue.use(Vuex);
export const store = new Vuex.Store(require('../store').default);

/* ============
 * Vue Router
 * ============
 *
 * The official Router for Vue.js. It deeply integrates with Vue.js core
 * to make building Single Page Applications with Vue.js a breeze.
 *
 * http://router.vuejs.org/en/index.html
 */
import VueRouter from 'vue-router';

Vue.use(VueRouter);
export const router = new VueRouter(require('./router').default);

let count = 0;
router.beforeEach((to, from, next) => {
    if (to.matched.some(m => m.meta.auth) && !store.state.user) {
        if (sessionStorage) {
            sessionStorage.setItem('BeforeLoginName', to.name);
        }
        if(count == 0){
            store.dispatch('refreshUser').then(()=>{
                next();
            }).catch(()=>{
                next({
                    name: 'Login',
                });
            });
            count++;
        }else{
            next({
                name: 'Login',
            });
        }
    }else{
        next();
    }
    // store.commit('addLoading');
});

router.afterEach(route => {
    window.scrollTo(0, 0) // scroll to top
    // store.commit('removeLoading', true);
});

/* ============
 * Vuex Router Sync
 * ============
 *
 * Effortlessly keep vue-Router and vuex store in sync.
 *
 * https://github.com/vuejs/vuex-router-sync/blob/master/README.md
 */
import VuexRouterSync from 'vuex-router-sync'

VuexRouterSync.sync(store, router);

/* ============
 * moment js
 * ============
 *
 * Moment was designed to work both in the browser and in Node.js
 *
 * http://momentjs.com/docs/
 */
// import moment from 'moment'

// moment.locale('zh-cn');
// console.log(moment("2016-10-11 11:11:11", "YYYY-MM-DD HH:ii:ss").fromNow());

/* ============
 * jquery-confirm
 * ============
 *
 * A jQuery plugin that provides great set of features like, Auto-close, Ajax-loading, Themes, Animations and more.
 This plugin is actively developed, I would love you have your suggestions.
 *
 * http://craftpip.github.io/jquery-confirm/
 */
// require('jquery-confirm-npm/dist/jquery-confirm.min.js');
// require('jquery-confirm-npm/dist/jquery-confirm.min.css');


export default {store, router}
