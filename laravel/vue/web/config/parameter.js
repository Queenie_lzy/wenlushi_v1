export default {
    //学历
    degree: {
        '本科': 'bk',
        '硕士': 'ss',
        '博士': 'bs',
        '转学': 'zx',
        '访问': 'fw'
    },
    //留学服务范围
    ServiceArea: {
        '综合留学申请': 'zhlxsq',
        '基础留学申请': 'jclxsq',
        '文书点石成金': 'wsdscj',
        '文书基础写作': 'wsjcxz',
        '文书加急写作': 'wsjjxz',
        '规划与指导': 'ghyzd',
    },
    //申请学校状态
    applySchoolStatus: {
        'not_submit'    : '未提交',
        'waiting'       : '等待结果',
        'own_expense'   : '录取（自费）',
        // 'fellowship'    : '录取（助学金）',
        'scholarship'   : '录取（奖学金）',
        'not_admitted'  : '未录取',
    },
    //圆桌会议状态
    meetingStatus: {
        'untake'        : '未受理',
        'service-in'    : '服务中',
        'resolve'       : '已解决',
        'refuse'        : '已拒绝',
    },

    //时区
    timeZone: {
        'America'   : '美国中部时间',
        'China'     : '北京时间'
    },

    //发布资讯状态
    articleStatus: {
        'draft'         : '草稿',
        'check'         : '待审批',
        'publish'       : '已发布',
        'reject'        : '被退回',
        'register'      : '报名中',
        'underway'      : '进行中',
        'finish'        : '已结束',
    },
    //留学
    StudyAbroad: {
        country: [
            '美国',
            '加拿大',
            '澳大利亚',
            '英国'
        ]
    },
    //留学申请阶段对应状态
    statusRecord:{
        '0'  : '背景评估',
        '1'  : '销售约谈',
        '2'  : '专家约谈',
        '3'  : '服务范围',
        '4'  : '合同签订',
        '5'  : '导师分配',
        'start'  : '启动服务',
        'strategy'  : '制定策略',
        'promotion'  : '提升背景',
        'document'  : '创作文书',
        'online_apply' : '网络申请',
        'visa' : '准备签证',
        'feedback' : '调查反馈',
        'finish' : '结束服务',
    },
    CountriesRegions: [
        {
            "en": "China",
            "zh_cn": "中国(大陆)",
            "abbreviation": "CN",
            "phone_code": "86"
        },
        {
            "en": "Hongkong",
            "zh_cn": "中国(香港)",
            "abbreviation": "HK",
            "phone_code": "852"
        },
        {
            "en": "United States of America",
            "zh_cn": "美国",
            "abbreviation": "US",
            "phone_code": "1",
            "time_difference": "-13"
        },
        {
            "en": "Canada",
            "zh_cn": "加拿大",
            "abbreviation": "CA",
            "phone_code": "1",
            "time_difference": "-13"
        },
        {
            "en": "Australia",
            "zh_cn": "澳大利亚",
            "abbreviation": "AU",
            "phone_code": "61",
            "time_difference": "+2"
        },
        {
            "en": "United Kiongdom",
            "zh_cn": "英国",
            "abbreviation": "GB",
            "phone_code": "44",
            "time_difference": "-8"
        },
        {
            "en": "other",
            "zh_cn": "其它",
            "abbreviation": "OT",
            "phone_code": ' ',
            "time_difference": ''
        }
    ],
    main_nav: {
        admin:[
            {
                text: '问路资讯',
                to: {
                    name:'Articles',
                },
                child:[
                    {
                        text: '全部资讯',
                        to: {
                            name:'Articles'
                        },
                    },
                    {
                        text: '发文管理',
                        to: {
                            name:'Articles.My',
                        },
                    }
                ]
            },
            // {
            //     text: '精品活动',
            //     to: {
            //         name:'Activities',
            //     },
            //     child:[
            //         {
            //             text: '活动管理',
            //             to: {
            //                 name:'Activities.Admin',
            //                 params:{

            //                 }
            //             },
            //         }
            //     ]
            // },
            {
                text: '服务站',
                to: {
                    name:'Service.Admin',
                    params:{
                        status: 'sales-in'
                    }
                },
                child:[
                    {
                        text: '全部学员',
                        to: {
                            name:'Service.Admin',
                            params:{
                                status: 'sales-in'
                            }
                        },
                    },
                    {
                        text: '客服记录',
                        to: {
                            name:'CustomerService',
                        },
                    },
                    {
                        text: '我的服务',
                        to: {
                            name:'Service',
                        },
                    },
                    {
                        text: '导师联盟',
                        to: {
                            name:'Service.Group',
                        },
                    }
                ]
            },
            {
                text: '统计报告',
                to: {
                    name:'Statistics',
                }
            },
        ],
        tutor:[
            {
                text: '问路资讯',
                to: {
                    name:'Articles',
                },
                child:[
                    {
                        text: '全部资讯',
                        to: {
                            name:'Articles'
                        },
                    },
                    {
                        text: '我的发文',
                        to: {
                            name:'Articles.My',
                        },
                    }
                ]
            },
            // {
            //     text: '精品活动',
            //     to: {
            //         name:'Activities',
            //     },
            // },
            {
                text: '服务站',
                to: {
                    name:'Service.Admin',
                    params:{
                        status: 'sales-in'
                    }
                },
                child:[
                    {
                        text: '我的学员',
                        to: {
                            name:'Service.Admin',
                            params:{
                                status: 'sales-in'
                            }
                        },
                    },
                    {
                        text: '我的服务',
                        to: {
                            name:'Service',

                        },
                    },
                    {
                        text: '导师联盟',
                        to: {
                            name:'Service.Group',
                        },
                    }
                ]
            },
        ],
        student:[
            {
                text: '问路资讯',
                to: {
                    name:'Articles',
                },
                child:[
                    {
                        text: '全部资讯',
                        to: {
                            name:'Articles'
                        },
                    },
                    {
                        text: '我的发文',
                        to: {
                            name:'Articles.My',
                        },
                    }
                ]
            },
            // {
            //     text: '精品活动',
            //     to: {
            //         name:'Activities',
            //     },

            // },
            {
                text: '服务站',
                to: {
                    name:'Service',
                },
                child:[
                    {
                        text: '我的服务',
                        to: {
                            name:'Service',
                        },
                    },
                    {
                        text: '导师联盟',
                        to: {
                            name:'Service.Group',
                        },
                    }
                ]
            },
        ],
        service:[
            {
                text: '问路资讯',
                to: {
                    name:'Articles',
                }
            },
            // {
            //     text: '精品活动',
            //     to: {
            //         name:'Activities',
            //     },

            // },
            {
                text: '服务站',
                to: {
                    name:'Service',
                },
                child:[
                    {
                        text: '客服记录',
                        to: {
                            name:'CustomerService',
                        },
                    },
                    {
                        text: '我的服务',
                        to: {
                            name:'Service',
                        },
                    },
                    {
                        text: '导师联盟',
                        to: {
                            name:'Service.Group',
                        },
                    }
                ]
            },
        ],
        guest:[
            {
                text: '问路资讯',
                to: {
                    name:'Articles',
                }
            },
            // {
            //     text: '精品活动',
            //     to: {
            //         name:'Activities',
            //     },

            // },
            {
                text: '服务站',
                to: {
                    name:'Service',
                },
                child:[
                    {
                        text: '导师联盟',
                        to: {
                            name:'Service.Group',
                        },
                    }
                ]
            },
        ],
    }
}
