import Vue from 'vue';

export default class {
    /**
     * 构造函数
     */
    constructor(){

    }

    /**
     * 解构函数
     */
    destructuring(){

    }

    static getProfile(){
        return Vue.http.get('my');
    }

    static getResume(){
        return Vue.http.get('my/resume');
    }

}
