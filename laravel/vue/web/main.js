import Vue from 'vue'
import App from './App'
import Bootstrap from './config/bootstrap'

new Vue({
  ...Bootstrap,
  ...App,
}).$mount('#app');
