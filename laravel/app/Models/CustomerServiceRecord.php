<?php

namespace App\Models;

class CustomerServiceRecord extends Model
{
    public function serviceTags()
    {
        return $this->belongsToMany(ServiceTag::class, 'record_service_tag', 'record_id', 'service_tag_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
