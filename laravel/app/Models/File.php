<?php

namespace App\Models;

/**
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $able
 */
class Image extends Model
{
    const UPDATED_AT = null;

    protected $hidden = [
        'able_id',
        'able_type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo|self
     */
    public function able()
    {
        return $this->morphTo();
    }
}
