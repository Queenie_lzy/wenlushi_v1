<?php

namespace App\Models;

class Survey extends Model
{
    public $timestamps = false;

    protected $table = 'survey';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}