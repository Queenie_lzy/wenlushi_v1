<?php

namespace App\Models;

class StudyAbroadSpecialty extends Model
{
    public function applyInfos()
    {
        return $this->hasMany(SchoolSpecialtyInfo::class, 'specialty_id');
    }

    public function applySchools()
    {
        return $this->hasMany(StudyAbroadApplySchool::class, 'specialty_id');
    }
}