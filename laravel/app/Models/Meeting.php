<?php

namespace App\Models;

use DateTime;

class Meeting extends Model
{
    public $casts = [
        'session' => 'object'
        ];
    private static $updateAttributes = ['status', 'remark', 'calendar_id', 'duration', 'answer'];

    public static function getUpdateAttributes()
    {
        return self::$updateAttributes;
    }

    public function getTimeCalculateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    // 对json结构的session内部排序
    public function getSessionAttribute($value)
    {
        $value = json_decode($value, true);
        $arr = [];
        if(is_array($value) && count($value) > 1){
            foreach($value as $v){
                if(count($v) < 1) continue;
                $k = new DateTime($v['date'].' '.$v['hour'].':00:00 '.$v['period']);
                $arr[strtotime($k->format('Y-m-d H:i:s'))] = $v;
            }
        }
        if(count($arr) > 0) {
            ksort($arr);
            return array_values($arr);
        }
        return $value ? $value : [];
    }

    /**
     * 判断预约时间是否已被用
     * @return boolean
     */
    public static function calendarIdExist($calendarId, $meetingId)
    {
        return Meeting::whereCalendarId($calendarId)
            ->where('id', '<>', $meetingId)
            ->exists();
    }
    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function apply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }

     /**
     * 开展时间
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|calendar
     */
    public function calendar()
    {
        return $this->belongsTo(Calendar::class);
    }

    /**
     * 用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 导师
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|user
     */
    public function tutor()
    {
        return $this->belongsTo(User::class);
    }
}