<?php

namespace App\Models;

class SchoolSpecialtyInfo extends Model
{
    public $casts = [
        'grade' => 'object'
    ];

    public function specialty()
    {
        return $this->belongsTo(StudyAbroadSpecialty::class);
    }

    public function school()
    {
        return $this->belongsTo(StudyAbroadSchool::class);
    }

    public static function getInfo($schoolId, $specialtyId)
    {
        return  static::where('school_id', $schoolId)
            ->where('specialty_id', $specialtyId)
            ->first(['grade', 'deadline_at', 'inform_at', 'link'])
            ->toArray();
    }

    public static function getGroupSchoolsInfo($specialtyName, $schoolNames = [])
    {
        $res = [];
        $specialty = StudyAbroadSpecialty::whereName($specialtyName)->first();
        if($specialty) {
            $res[$specialty->id]['specialty'] = $specialty->name;
            if(count($schoolNames) > 0){
               foreach ($schoolNames as $key => $value) {
                    $school = static::getSchool($value, $specialty->id);

                    if($school) {
                        $school_arr = $school->toArray();
                        if(array_key_exists('id', $school_arr)) unset($school_arr['id']);
                        $res[$specialty->id]['school'][] = $school_arr;
                    }
                }
            }
        }
        return $res;
    }

    public static function getSchool($schoolName, $specialtyId)
    {
        return  static::with('school')
            ->whereHas('school', function ($k) use ($schoolName){
                $k->whereName($schoolName);
            })
            ->where('specialty_id', $specialtyId)
            ->first();
    }


}