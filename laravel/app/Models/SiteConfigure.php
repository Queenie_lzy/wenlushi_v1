<?php

namespace App\Models;

class SiteConfigure extends Model
{
    protected $keyType = 'string';

    protected $casts = [
        'meta' => 'object'
    ];
}
