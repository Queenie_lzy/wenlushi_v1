<?php

namespace App\Models;

class UserExam extends Model
{
    public $casts = [
        'english' => 'object',
        'postgraduate' => 'object',
        'school_work' => 'object',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}