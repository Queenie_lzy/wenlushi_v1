<?php

namespace App\Models;

class Message extends Model
{
    protected $hasModels = [
        'user'
    ];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class, 'apply_id');
    }

    /**
     * 留言人
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}