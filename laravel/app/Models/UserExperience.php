<?php

namespace App\Models;

class UserExperience extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}