<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
//use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

abstract class AuthModel extends Model implements
    AuthenticatableContract
    , AuthorizableContract
//    , CanResetPasswordContract
{
    use Authenticatable
        , Authorizable
//        , CanResetPassword
        ;

    /**
     * 设置用户密码加密方式
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setApiTokenAttribute()
    {
        $this->attributes['api_token'] = str_random(64);
    }
}
