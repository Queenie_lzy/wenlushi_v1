<?php

namespace App\Models;

class StudyAbroadApplySchool extends Model
{
    public $casts = [
        'grade' => 'object'
    ];

    private static $attributeLabels = [
        'grade', 'deadline_at', 'inform_at',
        'link', 'professor', 'department',
        'result', 'school_id', 'specialty_id'
    ];

    public static function getAttributeLabels()
    {
        return self::$attributeLabels;
    }

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class, 'apply_id');
    }

    public function onlineApply()
    {
        return $this->hasOne(StudyAbroadOnlineApply::class, 'apply_school_id');
    }

    public function specialty()
    {
        return $this->belongsTo(StudyAbroadSpecialty::class);
    }

    public function school()
    {
        return $this->belongsTo(StudyAbroadSchool::class);
    }
}