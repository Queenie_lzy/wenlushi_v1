<?php

namespace App\Models;

class StudyAbroadDocument extends Model
{
    protected $casts = [
        'file' => 'object'
    ];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }

    /**
     * 获取进程
     * @param int $applyId
     * @return float
     */
    public static function getProgress($applyId)
    {
        $files = static::where('apply_id', $applyId)
            ->pluck('file')
            ->toArray();

        $locks = [];
        foreach($files as $key=>$value){
            $value = json_decode($value, true);
            if(count($value) < 1) continue;
            foreach ($value as $k => $v) {
                if(!array_key_exists('lock', $v)) $v['lock'] = 0;
                $locks[] = $v['lock'];
            }
        }
        if(count($locks) < 1) return 0;

        return array_sum($locks) / count($locks);
    }

    /**
     * 获取进程
     * @param int $applyId
     * @return float
     */
    public static function getProgressDetail($applyId)
    {
        $files = static::where('apply_id', $applyId)
            ->pluck('file')
            ->toArray();

        $progress = [];
        foreach($files as $key=>$value){
            $value = json_decode($value, true);
            foreach ($value as $k => $v) {
                if(!array_key_exists('lock', $v)) $v['lock'] = 0;
                $progress[$v['desc']] = $v['lock'];
            }
        }

        return $progress;
    }
}
