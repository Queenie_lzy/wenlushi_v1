<?php

namespace App\Models;

class StudyAbroadVisa extends Model
{
    protected $table = 'study_abroad_visa';

    public $casts = [
        'lock' => 'object'
    ];

    /**
     * 追加进度字段
     */
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        $sum = 0;
        foreach((array)$this->lock as $v){
            $sum += $v;
        }
        return $sum / 3;
    }

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|StudyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }
}