<?php

namespace App\Models;

class UserSkill extends Model
{
    protected $table = 'skills';

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_skill', 'skill_id', 'user_id');
    }
}