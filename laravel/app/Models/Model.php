<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Models\Traits\HasModelsTrait;
use Eloquent;
use Carbon\Carbon;

/**
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|Model findOrFailByUpdate($id, $attributes)
 * @method static \Illuminate\Database\Query\Builder|Model findOrFail($id, $columns = array())
 * @method static \Illuminate\Database\Query\Builder|Model groupByRaw($sql)
 * @mixin Eloquent
 */
abstract class Model extends Eloquent
{
    use HasModelsTrait;

    protected $guarded = [
        'images',
        'updated_at',
    ];

    protected $hidden = [
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->getRequestExtend();
    }

    private function getRequestExtend()
    {
        if (request()->method() == 'GET') {
            if (is_numeric(request('per_page'))) {
                $this->setPerPage(request('per_page'));
            }
            if (request('fields')) {
                $fields = explode(',', request('fields'));
                $this->addVisible($fields);
            }
        }
    }

    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();
        if (request()->segment(1) != 'api') {
            return $attributes;
        }
        foreach ($this->getDates() as $key) {
            if (!isset($attributes[$key])) {
                continue;
            }
            $Iso8601 = Carbon::parse(Carbon::createFromFormat($this->getDateFormat(), $attributes[$key])->toIso8601String());
            $attributes[$key] = $Iso8601->setTimezone('UTC')->format("Y-m-d\TH:i:s") . '.000Z';
        }
        return $attributes;
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }
        return $this;
    }

    public function mergeAttributesOld($old = null)
    {
        if (!$old) {
            $old = old() ?: [];
        }
        foreach ($old as $k => $v) {
            if (empty($v) && $v != '0') {
                unset($old[$k]);
            }
        }
        return $this->setAttributes($old);
    }

    /**
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|self $query
     * @param null $table
     */
    protected function addSelectStar($query, $table = null)
    {
        if ($table) {
            $table .= '.';
        }
        if (!$query->getQuery()->columns) {
            $query->addSelect($table . '*');
        }
    }

    /**
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|self $query
     * @param $sql
     * @return mixed
     */
    public function scopeGroupByRaw($query, $sql)
    {
        return $query->groupBy(\DB::raw($sql));
    }
}
