<?php

namespace App\Models;

class Article extends Model
{
    public $timestamps = false;

    // protected $fillable = ['updated_at'];
    protected $guarded = [];

    private static $attributeLabels = ['type', 'title', 'author', 'status', 'content', 'cover', 'remark', 'login_access'];

    public static function getAttributeLabels()
    {
        return self::$attributeLabels;
    }

    public function getDescriptionAttribute()
    {
        $desc = $this->content;
        $desc = preg_replace('/<style.*?<\/style>/is', '', $desc);
        $desc = preg_replace('/<script.*?<\/script>/is', '', $desc);
        $desc = preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", " ", strip_tags($desc));

        return mb_substr($desc, 0, 350);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo|User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne|Activity
     */
    public function activity()
    {
        return $this->hasOne(Activity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany|Tag
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'tag_gable');
    }
}
