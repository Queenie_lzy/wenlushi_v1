<?php
/**
 * Created by PhpStorm.
 * User: stevie
 * Date: 7/29/16
 * Time: 11:55
 */

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasModelsTrait
{
    protected $hasModels = [];

    public static function bootHasModelsTrait()
    {
        static::addGlobalScope('HasModels', function (Builder $builder) {
            $model = new static;
            $has_models = $model->hasModels;
            if ($has_models && is_array($has_models)) {
                foreach ($has_models as $i) {
                    if (is_callable([$model, $i])) {
                        $builder->has($i);
                    }
                }
            }
        });
    }
}
