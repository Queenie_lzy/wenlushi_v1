<?php

namespace App\Models;

class StudyAbroadSchool extends Model
{
    protected $attributes = [
        'logo' => 'http://og9i0ercv.bkt.clouddn.com/schools_logo/placeholder.png?imageView2/1/w/128/h/128'
    ];

    public function applyInfos()
    {
        return $this->hasMany(SchoolSpecialtyInfo::class, 'school_id');
    }

    public function applySchools()
    {
        return $this->hasMany(StudyAbroadApplySchool::class, 'school_id');
    }

    public function onlineApplies()
    {
        return $this->hasMany(StudyAbroadOnlineApply::class, 'school_id');
    }
}