<?php

namespace App\Models;

class UserEducation extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}