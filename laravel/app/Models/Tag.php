<?php

namespace App\Models;

class Tag extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo|self
     */
    public function gable()
    {
        return $this->morphTo();
    }

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'tag_gable');
    }

    public function activities()
    {
        return $this->morphedByMany(Activity::class, 'tag_gable');
    }
}
