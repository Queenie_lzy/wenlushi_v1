<?php

namespace App\Models;

use App\Repositories\StudyAbroadApplyStatusRepository;

class StudyAbroadApply extends Model
{
    public $casts = [
        'school' => 'object',
        'secondary_school' => 'object',
        'season' => 'object',
    ];

    protected $dates = [
        'submit_at'
    ];

    protected $hasModels = [
        'user'
    ];

    /**
     * 追加进度字段
     */
//    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        if($this->status == 'sales-in'){
            return $this->start ? $this->start->progress * 100: 0;
        } else {
            $progress = $this->getProgress();

            return floor(array_sum($progress) / count($progress) * 100);
        }
    }

    //获取流程
    public function getProcessAttribute()
    {
        if($this->start && $this->start->service_scope) {
            return config('params.process.'.$this->start->service_scope);
        } else {
            return head(config('params.process'));
        }
    }

    //获取制定策略结束时间  +15 day
    public function getStrategyEndAttribute()
    {
        if($this->submit_at){
            $date = date('Y-m-d H:i:s',strtotime($this->submit_at));
            return date('Y-m-d H:i:s',strtotime("$date +15 day"));
        }
    }

    /**
     * 提交时间友好提示
     * @return mixed
     */
    public function getSubmitAtFriendlyAttribute()
    {
        return $this->submit_at->diffForHumans();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tutor()
    {
        return $this->belongsTo(User::class);
    }

    public function start()
    {
        return $this->hasOne(StudyAbroadStart::class, 'apply_id');
    }

    public function strategy()
    {
        return $this->hasOne(StudyAbroadStrategy::class, 'apply_id');
    }

    public function applySchools()
    {
        return $this->hasMany(StudyAbroadApplySchool::class, 'apply_id');
    }

    public function onlineApply()
    {
        return $this->hasOne(StudyAbroadOnlineApply::class, 'apply_id');
    }

    public function promotion()
    {
        return $this->hasOne(StudyAbroadPromotion::class, 'apply_id');
    }

    public function schedule()
    {
        return $this->hasOne(StudyAbroadSchedule::class, 'apply_id');
    }

    public function documents()
    {
        return $this->hasMany(StudyAbroadDocument::class, 'apply_id');
    }

    public function visa()
    {
        return $this->hasOne(StudyAbroadVisa::class, 'apply_id');
    }

    public function feedback()
    {
        return $this->hasOne(StudyAbroadFeedback::class, 'apply_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'apply_id');
    }

    public function meetings()
    {
        return $this->hasMany(Meeting::class, 'apply_id');
    }

    public function readMessages()
    {
        return $this->hasMany(ReadMessage::class, 'apply_id');
    }

    public function statusTo()
    {
        return new StudyAbroadApplyStatusRepository($this);
    }

    /**
     * 获取留学服务各个阶段进度
     * @return  array
     */
    public function getProgress()
    {
        $process = $this->process;

        $progress['start'] = $this->start ? $this->start->progress : 0;
        if(in_array('strategy', $process))
            $progress['strategy'] = $this->strategy ? $this->strategy->progress : 0;
        if(in_array('promotion', $process) && $this->promotion)
            $progress['promotion'] =$this->promotion ? $this->promotion->progress : 0;
        if(in_array('document', $process))
            $progress['document'] =StudyAbroadDocument::getProgress($this->id);
        if(in_array('online_apply', $process))
            $progress['online_apply'] = $this->onlineApply ? $this->onlineApply->progress : 0;
        if(in_array('visa', $process))
            $progress['visa'] = $this->visa ? $this->visa->progress : 0;
        if(in_array('feedback', $process))
            $progress['feedback'] = $this->feedback ? $this->feedback->progress : 0;

        return $progress;
    }

    /**
     * 获取互相发送邮件的所有人
     * @param  Int  $userId  当前用户Id
     * @return  array
     */
    public function getEmailUsers($userId = null)
    {
        $admins = User::whereRole('admin')->pluck('email')->toArray();
        $emails = array_merge($admins, [
                User::find($this->user_id)->email,
                $this->tutor_id ? User::find($this->tutor_id)->email : ''
            ]);

        if($userId){
            $user = User::findOrFail($userId);
            array_splice($emails, array_search($user->email, $emails), 1);
        }

        foreach($emails as $k=>$email){
            if(preg_match("/^\w+([-+.]\w+)*@example\.\w+([-.]\w+)*$/", $email))
                unset($emails[$k]);
        }

        return array_values(array_filter($emails));
    }

}
