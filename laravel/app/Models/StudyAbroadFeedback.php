<?php

namespace App\Models;

class StudyAbroadFeedback extends Model
{
    protected $table = 'study_abroad_feedback';

    public $casts = [
        'lock' => 'object'
    ];

    /**
     * 追加进度字段
     */
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        $this->lock = (array)$this->lock;

        return array_sum($this->lock) / 4;
    }
    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|StudyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }
}