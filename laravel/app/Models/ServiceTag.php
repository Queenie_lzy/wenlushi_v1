<?php

namespace App\Models;

class ServiceTag extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany|User
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_service_tag', 'service_tag_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany|CustomerServiceRecord
     */
    public function records()
    {
        return $this->belongsToMany(CustomerServiceRecord::class, 'record_service_tag', 'service_tag_id', 'record_id');
    }
}
