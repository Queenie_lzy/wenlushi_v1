<?php

namespace App\Models;

class ReadMessage extends Model
{
    protected $hasModels = [
        'user'
    ];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }

    /**
     * 留言人
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}