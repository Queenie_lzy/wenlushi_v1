<?php

namespace App\Models;

class Calendar extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function meeting()
    {
        return $this->hasOne(Meeting::class);
    }
}
