<?php

namespace App\Models;

class UserSupplement extends Model
{
    public $casts = [
        'content' => 'object'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}