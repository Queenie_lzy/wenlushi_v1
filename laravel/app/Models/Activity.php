<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use DateTimeZone;

class Activity extends Model
{
    public $casts = [
        'contact' => 'object'
    ];

    private static $attributeLabels = ['start', 'end', 'deadline_at', 'maximum', 'contact', 'user_id'];

    public static function getAttributeLabels()
    {
        return self::$attributeLabels;
    }

    /**
     * 追加进度字段
     */
    protected $appends = ['status', 'register_date'];

    /**
     * 获取距离报名截止天/小时/分
     * @return array
     */
    public function getRegisterDateAttribute()
    {

        if($this->deadline_at){
            $deadline_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->deadline_at);
            $least = $deadline_at->diffForHumans(null, true);
            $days = $deadline_at->diffInDays();
            $hours = $deadline_at->subDays($days)->diffInHours();
            $minutes = $deadline_at->subDays($days)->subHours($hours)->diffInMinutes();
        }

       return [
            'least' => $least,
            'days' => $days,
            'hours' => $hours,
            'minutes' => $minutes
        ];
    }

    /**
     * 由活动时间（默认北京时间）获取美中时区时间
     * @return array
     */
    public function getDateAttribute()
    {
        $time_zome = 'America/Chicago';
        $format = 'Y-m-d h:i A';

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $this->end);

        return [
            '(北京) '.$start->format($format).' - '.$start->diffInHours($end).'小时',
            '(美中) '.$start->setTimezone(new DateTimeZone($time_zome))->format($format).' - '.$start->diffInHours($end).'小时'
        ];
    }

    /**
     * 根据各个时间获取活动所处于的阶段
     * @return string
     */
    public function getStatusAttribute()
    {
        $deadline_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->deadline_at);
        $deadline = strtotime($deadline_at->addDay());
        $end = strtotime($this->end);
        $now =  time();
        if($deadline >= $now) {
            return 'register';
        }
        if($deadline < $now && $end > $now) {
            return 'underway';
        }
        if($end <= $now) {
            return 'finish';
        }
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function guest()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function registers()
    {
        return $this->belongsToMany(User::class, 'activity_registers', 'activity_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany|Tag
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'tag_gable');
    }
}
