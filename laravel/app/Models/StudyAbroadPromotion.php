<?php

namespace App\Models;

class StudyAbroadPromotion extends Model
{
    protected $table = 'study_abroad_promotion';

    public $casts = [
        'content' => 'object'
    ];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }

    /**
     * 追加进度字段
     */
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        $contents = (array)$this->content;
        $locks = array_column($contents, 'lock');

        if(!count($contents)) return 0;

        return array_sum($locks) / count($contents);
    }

    //获取详细进度值
    public function getProgressDetail()
    {
        $contents = json_decode(json_encode($this->content),true);
        $progress = [];
        foreach($contents as $k=>$v){
            if(!array_key_exists('lock', $v)) $v['lock'] = 0;
            $progress[$v['activity']] = $v['lock'];
        }
        return $progress;
    }
}