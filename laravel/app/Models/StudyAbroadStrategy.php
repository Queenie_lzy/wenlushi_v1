<?php

namespace App\Models;

class StudyAbroadStrategy extends Model
{
    protected $table = 'study_abroad_strategy';

    public $casts = [
        'lock' => 'object'
    ];

    private static $attributeLabels = ['strategy', 'lock'];

    public static function getAttributeLabels()
    {
        return self::$attributeLabels;
    }

    /**
     * 追加进度字段
     */
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        $locks = (array)$this->lock;

        if(count($locks) < 1) return 0;

        return array_sum($locks) / count($locks);
    }

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|StudyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }
}