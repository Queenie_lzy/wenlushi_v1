<?php

namespace App\Models;

use App\Models\Events\UserEvent;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends AuthModel
{
    use SoftDeletes;

    protected $hidden = [
        'password',
        'api_token',
        'remember_token',
    ];

    protected $attributes = [
        'avatar' => 'http://7xq8k7.com1.z0.glb.clouddn.com/default_avatar.gif',
        'name' => ''
    ];

    protected $dates = ['deleted_at'];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|StudyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->hasOne(StudyAbroadApply::class);
    }

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|StudyAbroadApply
     */
    public function tutors()
    {
        return $this->hasMany(StudyAbroadApply::class, 'tutor_id');
    }

    /**
     * 作为嘉宾参与的活动
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id');
    }

    //获取作为嘉宾的活动记录
    public function latestActivities()
    {
        return $this->hasMany(Activity::class, 'user_id')->orderBy('id', 'DESC');
    }

    /**
     * 报名的活动
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function activityRegisters()
    {
        return $this->belongsToMany(Activity::class, 'activity_registers','user_id', 'activity_id');
    }

    /**
     * 教育经历
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|UserEducation
     */
    public function educations()
    {
        return $this->hasMany(UserEducation::class);
    }

    public function education()
    {
        return $this->hasOne(UserEducation::class)->orderBy('date', 'DESC');
    }
    /**
     * 工作经历
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|UserExperience
     */
    public function experiences()
    {
        return $this->hasMany(UserExperience::class);
    }

    public function experience()
    {
        return $this->hasOne(UserExperience::class)->orderBy('date', 'DESC');
    }

    /**
     * 各种水平考试成绩
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasOne(UserExam::class);
    }

//    /**
//     * 学术成就
//     * @return \Illuminate\Database\Eloquent\Relations\HasMany|UserScholarship
//     */
//    public function scholarships()
//    {
//        return $this->hasMany(UserScholarship::class);
//    }

    /**
     * 补充材料
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|UserSupplement
     */
    public function supplements()
    {
        return $this->hasMany(UserSupplement::class, 'user_id');
    }

    /**
     * 留言
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Message
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * 已读留言
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|ReadMessage
     */
    public function readMessages()
    {
        return $this->hasMany(ReadMessage::class);
    }

    /**
     * 专业技能
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|UserSkill
     */
    public function skills()
    {
        return $this->belongsToMany(UserSkill::class, 'user_skill', 'user_id', 'skill_id');
    }

    /**
     * 圆桌会议/在线留言（一问一答）
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Meeting
     */
    public function meetings()
    {
        return $this->hasMany(Meeting::class, 'user_id');
    }

    /**
     * 圆桌会议/在线留言（一问一答）
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Meeting
     */
    public function meetingTutors()
    {
        return $this->hasMany(Meeting::class, 'tutor_id');
    }

    /**
     * 服务项目多对多关联
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany|ServiceTag
     */
    public function serviceTags()
    {
        return $this->belongsToMany(ServiceTag::class, 'user_service_tag', 'user_id', 'service_tag_id');
    }

    /**
     * 资讯
     * @return \Illuminate\Database\Eloquent\Relations\hasMany|Article
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    //获取资讯（活动）
    public function latestArticles()
    {
        return $this->hasMany(Article::class)->orderBy('id', 'DESC')
            ->where('author', '<>', 'system')
            ->where('status', '<>', 'draft');
    }

    /**
     * 导师日历
     * @return \Illuminate\Database\Eloquent\Relations\hasMany|Calendar
     */
    public function calendars()
    {
        return $this->hasMany(Calendar::class);
    }

    /**
     * 调查
     * @return \Illuminate\Database\Eloquent\Relations\hasOne|Survey
     */
    public function survey()
    {
        return $this->hasOne(Survey::class);
    }

}
