<?php

namespace App\Models;

class StudyAbroadOnlineApply extends Model
{
    public $casts = [
        'lock' => 'object'
    ];

    private static $attributeLabels = ['apply_school_id', 'lock'];

    public static function getAttributeLabels()
    {
        return self::$attributeLabels;
    }

    /**
     * 追加进度字段
     */
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        $this->lock = (array)$this->lock;

        if(! count($this->lock)) return 0;

        return array_sum($this->lock) / count($this->lock);
    }

    //获取详细进度值
    public function getProgress($applyId)
    {
        $schools = StudyAbroadApplySchool::whereApplyId($applyId)
        ->with('school')
        ->get();
        $locks = $this->lock;
        $process = [];
        $process['最终选择'] = $locks[0];
        foreach($schools as $k=>$v){
            if(!array_key_exists($k+1, $locks)) $locks[$k+1] = 0;
            $process[$v->school->name] = $locks[$k+1];
        }
        return $process;
    }

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|StudyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }

    public function applySchool()
    {
        return $this->belongsTo(StudyAbroadApplySchool::class);
    }
}