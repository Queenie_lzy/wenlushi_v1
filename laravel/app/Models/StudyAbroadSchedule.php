<?php

namespace App\Models;

class StudyAbroadSchedule extends Model
{
    public $casts = [
        'promotion' => 'object',
        'document' => 'object',
        'online_apply' => 'object',
        'visa' => 'object',
        'feedback' => 'object'
    ];

    /**
     * 留学申请记录
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|studyAbroadApply
     */
    public function studyAbroadApply()
    {
        return $this->belongsTo(StudyAbroadApply::class);
    }
}