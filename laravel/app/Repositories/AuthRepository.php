<?php
/**
 * Created by PhpStorm.
 * User: stevie
 * Date: 10/29/16
 * Time: 11:19
 */

namespace App\Repositories;

use App\Models\UserSkill;

class AuthRepository
{
    /**
     * @var \App\Models\User
     */
    private $user;

    public function __construct()
    {
        if(auth()->guest()){
            abort(403);
        }
        $this->user = auth()->user();
    }

    public function saveResume($request)
    {
        $user = $this->user;

        //工作
        $data = $request->user_experiences;
        if (is_array($data)) {
            $model = $user->experiences();
            if(empty($data)){
                $model->delete();
            }else{
                $ids = array_column($data, 'id');
                (clone $model)->whereNotIn('id', $ids)->delete();
                $models = (clone $model)->whereIn('id', $ids)->get();
                foreach ($data as $v) {
                    if (!empty($v['id']) && $find_model = $models->find($v['id'])) {
                        $find_model->update($v);
                    } else {
                        $model->create($v);
                    }
                }
            }
        }

        //教育
        $data = $request->user_educations;
        if (is_array($data)) {
            $model = $user->educations();
            if(empty($data)){
                $model->delete();
            }else{
                $ids = array_column($data, 'id');
                (clone $model)->whereNotIn('id', $ids)->delete();
                $models = (clone $model)->whereIn('id', $ids)->get();
                foreach ($data as $v) {
                    if (!empty($v['id']) && $find_model = $models->find($v['id'])) {
                        $find_model->update($v);
                    } else {
                        $model->create($v);
                    }
                }
            }
        }

        //成绩
        if ($data = $request->user_exams) {
            $user->exams()->updateOrCreate([], $data);
        }

        //专业技能
        if($data = $request->user_skills){
            $skills = array_filter($data);
            $ids = array_column($data, 'id');
            $oldIds = $user->skills()->pluck('id')->toArray();
            foreach ($skills as $value) {
                $name = trim($value['name']);
                $skill = UserSkill::whereName($name)->first();
                if($skill) {
                    $user_ids = UserSkill::find($skill->id)->users()->pluck('id')->toArray();
                    if(in_array($user->id, $user_ids)) continue;
                } else {
                    $skill = UserSkill::create(['name' => $name]);
                }
                $user->skills()->attach($skill->id);
            }
            $delIds = array_diff($oldIds, $ids);
            if($delIds) $user->skills()->detach($delIds);
        } else {
            $user->skills()->detach();
        }

        //学术成果
//        $data = $request->user_scholarships;
//        if (is_array($data)) {
//            $model = $user->scholarships();
//            if(empty($data)){
//                $model->delete();
//            }else{
//                $ids = array_column($data, 'id');
//                (clone $model)->whereNotIn('id', $ids)->delete();
//                $models = (clone $model)->whereIn('id', $ids)->get();
//                foreach ($data as $v) {
//                    if (!empty($v['id']) && $find_model = $models->find($v['id'])) {
//                        $find_model->update($v);
//                    } else {
//                        $model->create($v);
//                    }
//                }
//            }
//        }

        //其他资料
        $data = $request->user_supplements;
        if (is_array($data)) {
            $model = $user->supplements();
            if(empty($data)){
                $model->delete();
            }else{
                $ids = array_column($data, 'id');
                (clone $model)->whereNotIn('id', $ids)->delete();
                $models = (clone $model)->whereIn('id', $ids)->get();
                foreach ($data as $v) {
                    if (!empty($v['id']) && $find_model = $models->find($v['id'])) {
                        $find_model->update($v);
                    } else {
                        $model->create($v);
                    }
                }
            }
        }


        return $user;
    }
}
