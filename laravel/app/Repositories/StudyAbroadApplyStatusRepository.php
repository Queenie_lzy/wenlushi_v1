<?php

namespace App\Repositories;


use App\Models\StudyAbroadApply;

class StudyAbroadApplyStatusRepository
{
    /**
     * @var \App\Models\StudyAbroadApply
     */
    public $model;

    public function __construct($model)
    {
        if (is_numeric($model)) {
            $model = StudyAbroadApply::findOrFail($model);
        }
        if ($model instanceof StudyAbroadApply) {
            $this->model = $model;
        } else {
            abort(500, '资源错误');
        }
    }

    public function draft()
    {
        $this->model->update([
            'submit_at' => null,
            'status' => null
        ]);
        return $this;
    }

    public function submit($attr = [])
    {
        $this->model->update(array_merge([
            'submit_at' => date('Y-m-d H:i:s'),
            'status' => 'sales-in',
            'stop_at' => null,
        ], $attr));
        return $this;
    }

    public function start($attr = [])
    {
        $this->model->update(array_merge([
            'service_at' => date('Y-m-d H:i:s'),
            'status' => 'service-in',
            'stop_at' => null,
        ], $attr));
        $this->record();
        return $this;
    }

    public function restore()
    {
        if($this->model->service_at){
            $this->start([
                'remarks' => null,
            ]);
        }else{
            $this->submit([
                'remarks' => null,
            ]);
        }
        return $this;
    }

    public function complete()
    {
        $this->model->update([
            'complete_at' => date('Y-m-d H:i:s'),
            'status' => 'complete',
        ]);
        return $this;
    }

    public function stop($remark)
    {
        $this->model->update([
            'stop_at' => date('Y-m-d H:i:s'),
            'status' => 'stop',
            'remarks' => $remark
        ]);
        return $this;
    }

    public function record($status = null)
    {
        if(isset($status)){
            $this->model->update([
                'status_record' => $status
            ]);
        } else {
            $progress = $this->model->getProgress();
            unset($progress['start']);
            foreach($progress as $k=>$v){
                if($v != 1){
                    $this->model->update([
                        'status_record' => $k
                    ]);
                    break;
                }
            }
        }
    }
}
