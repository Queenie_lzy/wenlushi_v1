<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth.driver:admin'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::controllers([
            'my' => 'MyController',
            'supplier' => 'SupplierController',
        ]);
        Route::resources([
            'users' => 'UsersController',
            'school-specialty-info' => 'SchoolSpecialtyInfoController',
            'schools' => 'SchoolsController',
            'specialties' => 'SpecialtiesController',
        ]);
    });
    Route::controllers([
        'auth' => 'AuthController',
        '/' => 'DefaultController'
    ]);
});

Route::group(['prefix' => '', 'namespace' => 'Web', 'middleware' => 'auth.driver:web'], function () {
    Route::group(['middleware' => 'auth'], function () {

        Route::group(['prefix' => 'my', 'namespace' => 'My'], function(){
            Route::resources([
                'study-abroad' => 'StudyAbroadController',
                'meeting' => 'MeetingController',
                'calendar' => 'CalendarController',
                'article' => 'ArticleController',
                'activity' => 'ActivityController',
                'survey' => 'SurveyController',
            ]);

            Route::controllers([
                '/' => 'DefaultController',
            ]);
        });

        Route::group(['prefix' => 'study-abroad', 'namespace' => 'StudyAbroad'], function () {
            Route::resources([
                'applies' => 'AppliesController',
                'start' => 'StartController',
                'strategy' => 'StrategyController',
                'promotion' => 'PromotionController',
                'document' => 'DocumentController',
                'apply-school' => 'ApplySchoolController',
                'visa' => 'VisaController',
                'feedback' => 'FeedbackController',
            ]);

            Route::controllers([
                '/' => 'DefaultController',
            ]);
        });

        Route::group(['prefix' => 'service', 'namespace' => 'Service'], function(){
            Route::resources([
                'read-message' => 'ReadMessageController',
                '{type}/{id}/{progress}/messages' => 'MessagesController',
            ]);
        });

        Route::resources([
            'customer-service' => 'CustomerServiceRecordController',
            'meetings' => 'MeetingsController'
        ]);
    });
    Route::group(['prefix' => 'meeting', 'namespace' => 'Meeting'], function(){
        Route::resources([
            'tutor' => 'TutorController',
        ]);
        Route::controllers([
            '/' => 'DefaultController',
        ]);
    });

    Route::resources([
        'articles' => 'ArticlesController',
        'users' => 'UsersController',
        'survey' => 'SurveyController',
        'case' => 'CaseController',
    ]);
    Route::controllers([
        'auth' => 'AuthController',
        'supplier' => 'SupplierController',
        '/' => 'DefaultController'
    ]);
});
