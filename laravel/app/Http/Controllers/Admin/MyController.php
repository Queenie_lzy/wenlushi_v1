<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\MyRequest;

class MyController extends Controller
{
    public function getIndex()
    {
        return response()->json(auth()->user());
    }

    public function putIndex(MyRequest $request)
    {
        auth()->user()->update($request->getNonEmptyStringData());

        return response()->json(auth()->user());
    }

    public function getLogout()
    {
        auth()->logout();
        return response()->json();
    }


}
