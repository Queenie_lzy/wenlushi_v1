<?php

namespace App\Http\Controllers\Admin;

use App\Models\StudyAbroadSchool;
use App\Models\SchoolSpecialtyInfo;
use App\Models\StudyAbroadSpecialty;

class DefaultController extends Controller
{
    public function getIndex()
    {
        return view('admin');
    }

    public function getSchoolSpecialty()
    {
        $type = request('type');
        if (request('type') == 'all') $type = '';

        $schools = StudyAbroadSchool::when($type, function($q){
                return $q->where('type', $type);
            })
            ->orderBy('id', 'DESC')
            ->get();
        $specialties = StudyAbroadSpecialty::when($type, function($q){
                return $q->where('type', $type);
            })
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json(compact('schools', 'specialties'));
    }

    public function getSpecialty()
    {
        $specialtyIds = SchoolSpecialtyInfo::where('school_id', request('schoolId'))
            ->pluck('specialty_id')
            ->toArray();
        $specialties = StudyAbroadSpecialty::whereNotIn('id', $specialtyIds)->get();

        return response()->json($specialties);
    }
}
