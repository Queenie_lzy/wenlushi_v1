<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\StudyAbroadSchool;
use App\Models\StudyAbroadSpecialty;
use App\Models\ServiceTag;
use App\Http\Requests\Admin\UserRequest;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')
            ->when(request('deleted_at') == 'true', function ($q) {
                return $q->onlyTrashed();
            })
            ->when(request('keyword'), function($q){
                return $q->orWhere('name', 'like', '%'.request('keyword').'%')
                    ->orWhere('nickname', 'like', '%'.request('keyword').'%');
            })
            ->paginate()
            ->appends(array_filter(request()->all()));

        return response()->json($users);
    }

    public function store(UserRequest $request)
    {
        $data = $request->getNonEmptyStringData();
        if(array_key_exists('service_tags', $data)){
            $service_tags = $data['service_tags'];
            unset($data['service_tags']);
        }
        if(array_key_exists('education', $data)){
            $education = $data['education'];
            unset($data['education']);
        }
        $user = User::create($data);

        if(isset($service_tags)) {
            foreach ($service_tags as $name) {
                $tag = ServiceTag::whereName($name)->first();
                if(!$tag) {
                    $tag = ServiceTag::create(['name' => $name]);
                }
                $user->serviceTags()->attach($tag->id);
            }
        }
        if(isset($education)) {
            $user->educations()->create($education);
        }

        return response()->json($user);
    }

    public function show($id)
    {
        $user = User::with('education')->findOrFail($id);
        $user->service_tags = $user->serviceTags()->pluck('name')->toArray();
        $schools = StudyAbroadSchool::get(['name']);
        $specialties = StudyAbroadSpecialty::get(['name']);

        return response()->json(compact('user','schools','specialties'));
    }

    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        $data = $request->getNonEmptyStringData();
        if(array_key_exists('service_tags', $data)){
            self::updateServiceTags($user, $data['service_tags']);
            unset($data['service_tags']);
        } else {
            $user->serviceTags()->detach();
        }

        if(array_key_exists('education', $data)){
            $education = $data['education'];
            if($education['id']) {
                $user->educations()->find($education['id'])->update($education);
            }
            unset($data['education']);
        }

        $user->update($data);
        return response()->json($user);
    }

    public function destroy($id)
    {
        $user = User::withTrashed()->findOrFail($id);
        switch (request('ac')) {
            case 'restore';
                $user->restore();
                break;
            case 'force';
                $user->forceDelete();
                break;
            default:
                $user->delete();
                break;
        }

        return response()->json();
    }

    private function updateServiceTags($user, $data)
    {
        $oldTags = $user->serviceTags()->pluck('name')->toArray();
        foreach ($data as $name) {
            if(in_array($name, $oldTags)) {
                array_splice($oldTags, array_search($name, $oldTags), 1);
                continue;
            }
            $tag = ServiceTag::whereName($name)->first();
            if(!$tag) {
                $tag = ServiceTag::create(['name' => $name]);
            }
            $user->serviceTags()->attach($tag->id);
        }
        $delTags = array_values($oldTags);
        foreach ($delTags as $key => $value) {
            $tag = ServiceTag::whereName($value)->first();
            $user->serviceTags()->detach($tag->id);
        }
    }
}
