<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
