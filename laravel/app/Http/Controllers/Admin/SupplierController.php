<?php

namespace App\Http\Controllers\Admin;

use App\Services\QiniuService;

/**
 * 第三方服务，供应商
 * Class SupplierController
 * @package App\Http\Controllers\Web
 */
class SupplierController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth', ['only' => [
            'getQiniu',
        ]]);
    }

    /**
     * 返回客户端七牛上传参数
     *
     * @param QiniuService $qiniu_auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQiniu(QiniuService $qiniu_auth)
    {
        return response()->json([
            'download_url' => $qiniu_auth->download_url,
            'upload_url' => $qiniu_auth->upload_url,
            'token' => $qiniu_auth->token(),
        ]);
    }
}
