<?php

namespace App\Http\Controllers\Admin;

use App\Models\StudyAbroadSpecialty;
use App\Http\Requests\Admin\SpecialtyRequest;

class SpecialtiesController extends Controller
{
    public function index()
    {
       $specialties = StudyAbroadSpecialty::orderBy('id', 'DESC')
            ->when(request('keyword'), function($q){
                return $q->where('name', 'like', '%'.request('keyword').'%');
            })
            ->paginate()
            ->appends(array_filter(request()->all()));

        return response()->json($specialties);
    }

    public function store(SpecialtyRequest $request)
    {
        $specialty = StudyAbroadSpecialty::where('name', $request->name)->first();
        if($specialty){
            return response()->json(['status'  => 'error',]);
        }
        StudyAbroadSpecialty::create($request->getNonEmptyStringData());

        return response()->json(['status'=>'success']);
    }

    public function show($id)
    {
        $info = StudyAbroadSpecialty::findOrFail($id);

        return response()->json($info);
    }

    public function update(SpecialtyRequest $request, $id)
    {
        $specialty = StudyAbroadSpecialty::where('name', $request->name)
            ->where('id', '<>', $id)
            ->first();
        if($specialty){
            return response()->json(['status'  => 'error',]);
        }

        StudyAbroadSpecialty::find($id)->update($request->getNonEmptyStringData());

        return response()->json(['status'=>'success']);
    }
}
