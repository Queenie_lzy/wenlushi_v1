<?php

namespace App\Http\Controllers\Admin;

use App\Models\StudyAbroadSchool;
use App\Http\Requests\Admin\SchoolRequest;

class SchoolsController extends Controller
{
    public function index()
    {
        $schools = StudyAbroadSchool::orderBy('id', 'DESC')
            ->when(request('keyword'), function($q){
                return $q->where('name', 'like', '%'.request('keyword').'%');
            })
            ->paginate()
            ->appends(array_filter(request()->all()));

        return response()->json($schools);
    }

    public function store(SchoolRequest $request)
    {
        $school = StudyAbroadSchool::where('name', $request->name)->first();
        if($school){
            return response()->json(['status'  => 'error',]);
        }

        StudyAbroadSchool::create($request->getNonEmptyStringData());

        return response()->json(['status'=>'success']);
    }

    public function show($id)
    {
        $info = StudyAbroadSchool::findOrFail($id);

        return response()->json($info);
    }

    public function update(SchoolRequest $request, $id)
    {
        $school = StudyAbroadSchool::where('name', $request->name)
            ->where('id', '<>', $id)
            ->first();
        if($school){
            return response()->json(['status'  => 'error']);
        }

        StudyAbroadSchool::find($id)->update($request->getNonEmptyStringData());

        return response()->json();
    }
}
