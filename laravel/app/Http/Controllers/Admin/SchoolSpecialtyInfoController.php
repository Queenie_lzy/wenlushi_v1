<?php

namespace App\Http\Controllers\Admin;

use App\Models\SchoolSpecialtyInfo;
use App\Http\Requests\Admin\SchoolSpecialtyInfoRequest;

class SchoolSpecialtyInfoController extends Controller
{
    public function index()
    {
        $keyword = request('keyword');
        $schools = SchoolSpecialtyInfo::with('school', 'specialty')
            ->whereHas('school', function($q) use ($keyword) {
                $q->when($keyword, function($k) use ($keyword) {
                    return $k->where('name', 'like', '%'.$keyword. '%');
                });
            })
            ->orderBy('id', 'DESC')
            ->paginate()
            ->appends(array_filter(request()->all()));

        return response()->json($schools);
    }

    public function store(SchoolSpecialtyInfoRequest $request)
    {
        SchoolSpecialtyInfo::create($request->getNonEmptyStringData());

        return response()->json(['status'=>'success']);
    }

    public function show($id)
    {
        $info = SchoolSpecialtyInfo::findOrFail($id);

        return response()->json(compact('info'));
    }

    public function update(SchoolSpecialtyInfoRequest $request, $id)
    {
        SchoolSpecialtyInfo::find($id)->update($request->getNonEmptyStringData());

        return response()->json();
    }

    public function destroy($id)
    {
        $info = SchoolSpecialtyInfo::findOrFail($id);
        $info->delete();
    }
}
