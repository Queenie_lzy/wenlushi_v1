<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\LoginRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    use ThrottlesLogins;

    public function __construct()
    {
        parent::__construct();
        auth()->setDefaultDriver('admin');
    }

    public function postLogin(LoginRequest $request)
    {
        if (auth()->attempt($request->getNonEmptyStringData(), $request->get('remember'))) {
            return response()->json(auth()->user());
        }
        return $request->response([
            'password' => [
                '密码错误'
            ]
        ]);
    }
}
