<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Http\Requests\Web\AuthLoginRequest;
use App\Http\Requests\Web\AuthRegisterRequest;
use App\Http\Requests\Web\AuthResetPasswordRequest;

class AuthController extends Controller
{
    public function postRegister(AuthRegisterRequest $request)
    {
        $request_data = $request->getData();
        $router_name = $request->router_name;
        $key = str_random(20);
        $email = request('email');
        \Cache::put($email, [
            'key' => $key,
            'user' => $request_data,
            'url' => $router_name
        ], 60 * 24);
        \Event::fire(new \App\Events\EmailEvent('active', $email, compact('key')));

        return response()->json();
    }

    public function postLogin(AuthLoginRequest $request)
    {
        $request_data = $request->getData();
        if (!auth()->attempt($request_data, $request->get('remember'))) {
            return $request->responseError('password', '密码错误');
        }
        $user = auth()->user();
        $user->load('serviceTags');
        return response()->json($user);
    }

    public function putResetPassword(AuthResetPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        $new_password = str_random(16);
        \Event::fire(new \App\Events\EmailEvent('reset-password', $user->email, compact('new_password')));

        $user->password = $new_password;
        $user->save();

        return response()->json();
    }

    public function putReSendRegisterEmail()
    {
        $email = request('email');
        $cache = \Cache::get($email);
        if($cache){
            \Event::fire(new \App\Events\EmailEvent('active', $email, $cache));
            return response()->json();
        }
        return abort(403, '该邮箱已注册成功或者还未注册');
    }

    /**
     * 邮件链接确认激活
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmailActived()
    {
        $email = request('email');
        $key = request('key');
        $cache = \Cache::get($email);
        if ($cache && $cache['key'] == $key) {
            $user = User::create($cache['user']);
            $url = $cache['url'];
            \Cache::forget($email);
            auth()->login($user, true);
            if($url == 'StudyAbroadForm') {
                return redirect('/service/study-abroad-form');
            } else {
                return redirect('/my/resume');
            }
        }
        return redirect('/');
    }
}
