<?php

namespace App\Http\Controllers\Web;

use App\Models\Meeting;
use Illuminate\Http\Request;

class MeetingsController extends Controller
{
    public function index()
    {
        return response()->json();
    }

    public function show($id)
    {
        $meeting = Meeting::findOrFail($id);
        if(request('with')){
            $with = request('with');
            if (is_array($with) && count($with) > 0) {
                $meeting->load($with);
            }
        }

        return response()->json($meeting);
    }
}
