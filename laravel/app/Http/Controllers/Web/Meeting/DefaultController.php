<?php

namespace App\Http\Controllers\Web\Meeting;

use App\Models\Meeting;
use App\Models\User;
use App\Models\UserEducation;
use App\Models\StudyAbroadSchool;

class DefaultController extends Controller
{
    public function getTutors()
    {
        $schools = UserEducation::orderBy('date', 'DESC')
            ->whereHas('user', function($q){
                $q->whereRole('tutor');
            })
            // ->whereIn('id', User::with('education')->where('role','tutor')->get()
            //     ->pluck('education')->pluck('id')->toArray())
            ->selectRaw('count(school) as rows, school')
            ->groupBy('school')
            ->get()
            ->toArray();

        $schoolLists = StudyAbroadSchool::whereNotNull('logo')
            ->lists('logo', 'name');

        $num = User::has('education')->where('role','tutor')->count();

        return response()->json(compact('schoolLists', 'schools', 'num'));
    }

    public function getMeetingCount()
    {
        $user = $this->my;
        $userId = $user->role == 'tutor' ? $user->id : null;
        $count = Meeting::selectRaw('count(status) as rows, status')
            ->when($userId, function($q) use ($userId){
               return $q->where('tutor_id', $userId);
            })
            ->groupBy('status')
            ->get();

        return response()->json($count);
    }
}
