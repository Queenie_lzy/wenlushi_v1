<?php

namespace App\Http\Controllers\Web\Meeting;

use App\Models\Meeting;
use App\Http\Requests\Web\UserMeetingRequest as MeetingRequest;

class TutorController extends Controller
{
    public function index()
    {
        $meetings = Meeting::with('user.education')
            ->when($this->my->role == 'tutor', function($q){
                return $q->where('tutor_id', $this->my->id);
            })
            ->orderBy('id', 'DESC')
            ->when(request('status'), function($q){
                return $q->whereStatus(request('status'));
            })
            ->paginate(8)
            ->appends(request()->all());

        foreach ($meetings as $key => $value) {
            $value->append('time_calculate');
        }

        return response()->json($meetings);
    }

    public function update(MeetingRequest $request, $id)
    {
        $data = $request->getNonEmptyStringData();
        $filter_data = array_only($data, self::getAttributes());

        if(array_key_exists('operation', $data)){
            $filter_data['status'] = $data['operation'];
        }

        if(array_key_exists('calendar_id', $filter_data)){
            if(Meeting::calendarIdExist($filter_data['calendar_id'], $id))
            return response()->json([
                    'status'=>'error','msg'=>'该时间已被占用，请选择其他时间！'
                ]);
        }
        $meeting = Meeting::findOrFail($id)
            ->update($filter_data);

        return response()->json($meeting);
    }

    private function getAttributes()
    {
        return Meeting::getUpdateAttributes();
    }
}
