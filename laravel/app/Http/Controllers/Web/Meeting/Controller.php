<?php

namespace App\Http\Controllers\Web\Meeting;

use App\Http\Controllers\Web\Controller as BaseController;

abstract class Controller extends BaseController
{
    /**
     * @var \App\Models\User
     */
    protected $my;

    public function __construct()
    {
        parent::__construct();

        $this->my = auth()->user();
    }
}
