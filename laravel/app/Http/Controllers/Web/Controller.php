<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->validatorExtend();
    }

    private function validatorExtend()
    {
        \Validator::extend('check_password', function ($attribute, $value, $parameters) {
            return \Hash::check($value, auth()->user()->password);
        }, '旧密码错误');

        \Validator::extend('captcha', function ($attribute, $value, $parameters) {
            if(env('APP_ENV') == 'local'){
                return true;
            }
            $leancloud = new \App\Services\LeanCloudService();
            $mobile_phone = request($parameters[0]);
            $res = $leancloud->verifySmsCode($mobile_phone, $value);
            if (!empty($res->code)) {
                return false;
            }
            return true;
        }, '无效验证码');
    }
}
