<?php

namespace App\Http\Controllers\Web;

use App\Models\CustomerServiceRecord;
use App\Http\Requests\Web\CustomerServiceRecordRequest as RecordRequest;

class CustomerServiceRecordController extends Controller
{
    public function index()
    {
        $records = CustomerServiceRecord::with('serviceTags')
            ->when(request('keyword'), function($q){
                return $q->where('summary', 'like', '%'.request('keyword').'%' );
            })
            ->orderBy('id', 'DESC')
            ->paginate()
            ->appends(request()->all());

        return response()->json($records);
    }

    public function show($id)
    {
        $record = CustomerServiceRecord::find($id);
        $record->service_tags = $record->serviceTags()->pluck('id')->toArray();

        return response()->json($record);
    }

    public function store(RecordRequest $request)
    {
        $data = $request->getNonEmptyStringData();
        if(array_key_exists('service_tags', $data)){
            $service_tags = $data['service_tags'];
            unset($data['service_tags']);
        }
        $record = CustomerServiceRecord::create($data);
        if(isset($service_tags)) {
            $record->serviceTags()->attach(array_values($service_tags));
        }
    }

    public function update(RecordRequest $request, $id)
    {
        $record = CustomerServiceRecord::find($id);
        $data = $request->getNonEmptyStringData();
        if(array_key_exists('service_tags', $data)){
            $service_tags = $record->serviceTags()->pluck('id')->toArray();
            $delIds = array_diff($service_tags,$data['service_tags']);
            if($delIds) $record->serviceTags()->detach($delIds);
            $newIds = array_diff($data['service_tags'], $service_tags);
            if($newIds) $record->serviceTags()->attach($newIds);
            unset($data['service_tags']);
        }
        $record->update($data);
    }

    public function destroy($id)
    {
        CustomerServiceRecord::find($id)->delete();
    }
}
