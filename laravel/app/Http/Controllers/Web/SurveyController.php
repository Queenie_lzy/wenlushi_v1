<?php

namespace App\Http\Controllers\Web;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function index()
    {
        $survey = Survey::selectRaw('
            SUM(wechat) AS wechat,
            SUM(network) AS network,
            SUM(brochure) AS brochure,
            SUM(friend) AS friend,
            SUM(activity) AS activity,
            SUM(other) AS other
            ')
        ->first();
        return response()->json($survey);
    }
}
