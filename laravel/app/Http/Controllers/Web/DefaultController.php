<?php

namespace App\Http\Controllers\Web;

use DB;
use App\Models\{Tag, User, Calendar, ServiceTag, StudyAbroadApplySchool};

class DefaultController extends Controller
{
    public function getIndex()
    {
        return view('web');
    }

    /**
     * 获取多态标签
     * @param type  article/activity/..
     * @return object
     */
    public function getTags()
    {
        switch (request('type')) {
            case 'article':
                $tags = Tag::whereHas('articles', function($q){
                        return $q->when(request('status'), function($k){
                            return $k->where('status',request('status'));
                        });
                    })
                    ->when(request('keyword'), function($q){
                        return $q->where('name', 'LIKE' , '%'.request('keyword').'%');
                    })
                    ->get();
                break;
            case 'activity':
                $tags = Tag::has('activities')
                    ->when(request('keyword'), function($q){
                        return $q->where('name', 'LIKE' , '%'.request('keyword').'%');
                    })
                    ->get(['id', 'name']);
                break;

            default:
                # code...
                break;
        }
        return response()->json($tags);
    }

    /**
     * 留学服务页-导师和成功案例数据
     * @param type  article/activity/..
     * @return object
     */
    public function getStudyAbroadDetail()
    {
        $tutors = User::with('education', 'experience')
            ->when(request('service'), function($q){
                return $q->WhereHas('serviceTags', function($k){
                        $k->where('name', request('service'));
                    });
            })
            ->where('role', 'tutor')->limit(20)->get();
        $cases = StudyAbroadApplySchool::with('school')
                ->whereExists(function ($k) {
                    $k->select(DB::raw(1))
                      ->from('study_abroad_applies')
                      ->whereRaw('study_abroad_applies.id = study_abroad_apply_schools.apply_id')
                      ->where('status', 'complete');
                })
                ->has('onlineApply')
                ->selectRaw('count(school_id) as rows, school_id, grade')
                ->groupBy('school_id')
                ->get();

        return response()->json(compact('tutors', 'cases'));
    }

    /**
     * 用户获取日历数据
     * @param type  article/activity/..
     * @return object
     */
    public function getCalendars()
    {
        $calendars = Calendar::when(request('date'), function($q){
                return $q->where('date', request('date'));
            })
            ->when(request('tutorId'), function($q){
                return $q->where('user_id', request('tutorId'));
            })
            ->where('date', '>=', date('Y-m-d'))
            ->has('meeting', '=', 0)
            ->orderBy('period', 'ASC')
            ->orderBy('hour', 'ASC')
            ->get();

        return response()->json($calendars);
    }

    /**
     * 用户获取服务数据
     * @return object
     */
    public function getServiceTags()
    {
        $serviceTags = ServiceTag::all();
        return response()->json($serviceTags);
    }
}
