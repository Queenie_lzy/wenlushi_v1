<?php

namespace App\Http\Controllers\Web;

use App\Models\Tag;
use App\Models\Article;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::with('user')
            ->orderBy('updated_at', 'DESC')
            ->when(request('tag'), function ($q) {
                return $q->whereHas('tags', function ($k) {
                        return $k->whereIn('tag_id', request('tag'));
                    });
            })
            ->when(request('keyword'), function($q){
                return $q->where('title', 'LIKE', '%' . request('keyword') . '%');
            })
            ->when(request('no_include_id'), function($q){
                return $q->where('id', '<>', request('no_include_id'));
            })
            ->where('status', 'publish')
            ->paginate()
            ->appends(request()->all());

        foreach ($articles as $v){
            $v->append('description');
        }

        return response()->json($articles);
    }

    public function show($id)
    {
        $article = Article::with(
            'user','activity.registers',
            'activity.guest.education',
            'activity.guest.experience'
            )
            ->findOrFail($id);
        if($article->activity) $article->activity->append('date');
        $article->tags = $article->tags;

        return response()->json($article);
    }

    //更新浏览数量
    public function update($id)
    {
        Article::findOrFail($id)
            ->increment('read_count');

        return response()->json();
    }
}
