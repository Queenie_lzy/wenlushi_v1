<?php

namespace App\Http\Controllers\Web\Service;

use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController
{
   /**
     * @var \App\Models\User
     */
    protected $my;

    public function __construct()
    {
        parent::__construct();

        $this->my = auth()->user();
    }
}
