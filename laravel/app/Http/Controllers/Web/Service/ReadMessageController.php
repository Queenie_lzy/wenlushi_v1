<?php

namespace App\Http\Controllers\Web\Service;

use App\Models\StudyAbroadApply;
use App\Http\Requests\Web\ReadMessageRequest;

class ReadMessageController extends Controller
{
    public function index()
    {
        return response()->json();
    }

    public function store(ReadMessageRequest $request)
    {
        $data = $request->getNonEmptyStringData();
        $data['user_id'] = $this->my->id;

        $apply = StudyAbroadApply::findOrFail($data['apply_id']);
        $apply->readMessages()->updateOrCreate([],$data);

       return response()->json();
    }

    public function destroy($id)
    {
        $this->messages->findOrFail($id)->delete();

        return response()->json(['status'=>'success']);
    }
}
