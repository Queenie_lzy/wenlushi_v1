<?php

namespace App\Http\Controllers\Web\Service;

use App\Models\User;
use App\Models\StudyAbroadApply;
use App\Http\Requests\Web\MessageRequest;

class MessagesController extends Controller
{
    /**
     * @var \App\Models\Message
     */
    private $messages;
    private $progress;

    public function callAction($method, $parameters)
    {
        if($parameters['type'] == 'study-abroad'){
            $model = StudyAbroadApply::findOrFail($parameters['id']);
            $this->messages = $model->messages();
        }

        $this->progress = $parameters['progress'];

        unset($parameters['type'], $parameters['id'], $parameters['progress']);
        return parent::callAction($method, $parameters);
    }

    public function index()
    {
        $messages = $this->messages
            ->with('user')
            ->where('period', $this->progress)
            ->orderBy('id', 'desc')
            ->paginate();

        return response()->json($messages);
    }

    public function store(MessageRequest $request)
    {
        $data = $request->getNonEmptyStringData();
        $data['user_id'] = auth()->id();
        $data['period'] = $this->progress;
        $messages = $this->messages
            ->create($data);

        //发送邮件通知
        $emails = $messages->studyAbroadApply->getEmailUsers(auth()->id());
        $user = User::find(auth()->id());
        $path = [
            '留学申请服务',
            config('params.translate')[$this->progress]
        ];
        $view_data = compact('user', 'messages', 'path');
        \Event::fire(new \App\Events\EmailEvent('message', $emails, $view_data));

        return $this->index();
    }

    public function update(MessageRequest $request, $id)
    {
        $data = $request->getNonEmptyStringData();
        $messages = $this->messages->findOrFail($id);
        $messages->update($data);

        //发送邮件通知
        $emails = $messages->studyAbroadApply->getEmailUsers(auth()->id());
        $user = User::find(auth()->id());
        $path = [
            '留学申请服务',
            config('params.translate')[$this->progress]
        ];
        $remark = '修改';
        $view_data = compact('user', 'messages', 'path', 'remark');
        \Event::fire(new \App\Events\EmailEvent('message', $emails, $view_data));

        return response()->json($messages);
    }

    public function destroy($id)
    {
        $this->messages->findOrFail($id)->delete();

        return response()->json(['status'=>'success']);
    }
}
