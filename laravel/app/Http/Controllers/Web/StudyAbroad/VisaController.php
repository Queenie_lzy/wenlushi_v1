<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadVisa;

class VisaController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with('visa', 'schedule')
            ->findOrFail(request('applyId'))
            ->append('process');;

        return response()->json($apply);
    }

    public function update(Request $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);

        if($data = $request->visa){
            if(array_key_exists('progress', $data)) unset($data['progress']);
            $apply->visa()->updateOrCreate([], $data);
        }

        if($apply->visa->progress == 1) $apply->statusTo()->record();
        if($apply->visa->progress != 0) {
            $user = $this->my;
            if($user->role == 'admin'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                if($apply->tutor_id){
                    array_splice($emails, array_search($apply->tutor->email, $emails), 1);
                }
                $path = [
                    '留学申请服务',
                    config('params.translate')['visa']
                ];
                $process['visa'] = $apply->visa->lock;
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('status-report', $emails, $view_data));
            }
        }

        return response()->json(['status'=>'success']);
    }
}
