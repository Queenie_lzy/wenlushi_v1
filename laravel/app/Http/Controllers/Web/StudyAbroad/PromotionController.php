<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadPromotion;
use App\Http\Requests\Web\StudyAbroadPromotionRequest as PromotionRequest;

class PromotionController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with('promotion', 'schedule')
            ->findOrFail(request('applyId'))
            ->append('process');

        return response()->json($apply);
    }

    public function show($id)
    {
        $promotion = StudyAbroadPromotion::findOrFail($id);

        return response()->json(compact('promotion'));
    }

    public function update(Request $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);
        $data = $request->promotion;
        $apply->promotion->update(['content' => $data['content']]);

        if($apply->promotion->progress == 1) $apply->statusTo()->record();
        if($apply->promotion->progress != 0) {
            $user = $this->my;
            if($user->role == 'tutor'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                $path = [
                    '留学申请服务',
                    config('params.translate')['promotion']
                ];
                $process['promotion'] = $apply->promotion->getProgressDetail();
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('message', $emails, $view_data));
            }
        }

        return response()->json(['status'=>'success']);
    }
}
