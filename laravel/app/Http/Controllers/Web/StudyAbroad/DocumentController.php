<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadDocument;
use App\Http\Requests\Web\StudyAbroadDocumentRequest as DocumentRequest;

class DocumentController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with('documents', 'schedule', 'start')
            ->findOrFail(request('applyId'))
            ->append('process');

        $progress = StudyAbroadDocument::getProgress(request('applyId'));

        return response()->json(compact('apply', 'progress'));
    }

    public function store(DocumentRequest $request)
    {
        return response()->json(['status'=>'success']);
    }

    public function update(Request $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);

        if($data = $request->all()){
            $documents = $apply->documents();
            $ids = array_column($data, 'id');
            (clone $documents)->whereNotIn('id', $ids)->delete();
            $_documents = (clone $documents)->whereIn('id', $ids)->get();
            foreach ($data as $v) {
                if(array_key_exists('showList', $v)) unset($v['showList']);
                $v['file'] = array_values(array_filter($v['file']));
                if (!empty($v['id']) && $find_model = $_documents->find($v['id'])) {
                    $find_model->update($v);
                } else {
                    $documents->create($v);
                }
            }
        } else {
            $apply->documents()->delete();
        }

        if(StudyAbroadDocument::getProgress($apply->id) == 1) $apply->statusTo()->record();
        if(StudyAbroadDocument::getProgress($apply->id) != 0) {
            $user = $this->my;
            if($user->role == 'tutor'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                $path = [
                    '留学申请服务',
                    config('params.translate')['document']
                ];
                $process['document'] = StudyAbroadDocument::getProgressDetail($apply->id);
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('status-report', $emails, $view_data));
            }
        }

        return response()->json(['status'=>'success']);
    }
}
