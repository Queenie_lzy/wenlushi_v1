<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use App\Models\Message;
use App\Models\ReadMessage;
use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadSchool;
use App\Models\SchoolSpecialtyInfo;
use App\Models\StudyAbroadDocument;
use App\Models\StudyAbroadSpecialty;
use App\Models\StudyAbroadPromotion;

use App\Repositories\AuthRepository;

class DefaultController extends Controller
{
    public function getReport($applyId)
    {
        $apply = StudyAbroadApply::with(
            'start',
            'strategy',
            'visa',
            'promotion',
            'schedule',
            'onlineApply',
            'feedback',
            'documents'
            )
            ->findOrFail($applyId)
            ->append('process','strategy_end');
        $apply->apply_schools = $apply->applySchools()->with('school', 'specialty')
            ->whereIn('result',['own_expense','fellowship','scholarship'])
            ->get();
        $progresses = array_keys($apply->getProgress());
        $documentProgress =StudyAbroadDocument::getProgress($applyId);

        return response()->json(compact(
                'apply', 'documentProgress', 'progresses'
            ));
    }

    public function getApplyCount()
    {
        $user = $this->my;
        $userId = $user->role == 'tutor' ? $user->id : null;
        $count = StudyAbroadApply::selectRaw('count(status) as rows, status')
            ->when($userId, function($q) use ($userId){
               return $q->where('tutor_id', $userId);
            })
            ->groupBy('status')
            ->get();

        return response()->json($count);
    }

    public function getSpecialties()
    {
        $specialtyIds = SchoolSpecialtyInfo::when(request('schoolId'), function($q){
                return $q->where('school_id', request('schoolId'));
             })
            ->pluck('specialty_id')
            ->toArray();
        $specialties = StudyAbroadSpecialty::whereIn('id', $specialtyIds)->get();

        return response()->json($specialties);
    }

    public function getSpecialtiesByName()
    {
        $specialties = StudyAbroadSpecialty::orderBy('name', 'asc')
            ->when(request('keyword'), function($q){
                return $q->where('name', 'LIKE', '%'.request('keyword').'%');
            })
            ->get();

        return response()->json($specialties);
    }

    /**
     * 根据专业名获取学校
     * type:at_home,abroad
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSchoolsBySpecialty()
    {
        $schools = [];
        $specialty = StudyAbroadSpecialty::when(request('keyword'), function($q){
                return $q->where('name', 'LIKE', '%'.request('keyword').'%');
            })
            ->when(request('type'), function($q){
                return $q->where('type', request('type'));
            })
            ->first();
        if($specialty) {
            $schools = SchoolSpecialtyInfo::with('school')
                ->when(request('type'), function ($q) {
                    return $q->whereHas('school', function ($k){
                        $k->where('type', request('type'));
                    });
                })
                ->where('specialty_id', $specialty['id'])
                ->get();
        }

        return response()->json($schools);
    }

    /**
     * 根据学校名获取学校
     * type:at_home,abroad
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSchoolsByName()
    {
        $schools = StudyAbroadSchool::when(request('keyword'), function($q){
                return $q->where('name', 'LIKE', '%'.request('keyword').'%');
            })
            ->when(request('type'), function($q){
                return $q->where('type', request('type'));
            })
            ->orderBy('type', 'ASC')
            ->get();

        return response()->json($schools);
    }

    //获取数量更新的留言信息数量
    public function getNewMessageCount()
    {
        $readId = 0;
        $user = $this->my;
        $read = ReadMessage::where('user_id', $user->id)
            ->when(request('apply_id'), function($q){
                return $q->where('apply_id', request('apply_id'));
            })
            ->when(request('period'), function($q){
                return $q->where('period', request('period'));
            })
            ->first();

        if($read) $readId = $read->read_id;

        $count = Message::when($readId, function($q) use ($readId){
                return $q->where('id', '>', $readId);
            })
            ->when(request('apply_id'), function($q){
                return $q->where('apply_id', request('apply_id'));
            })
            ->when(request('period'), function($q){
                return $q->where('period', request('period'));
            })
            ->where('user_id', '<>', $user->id)
            ->count();

        return response()->json(compact('count','readId'));
    }
}
