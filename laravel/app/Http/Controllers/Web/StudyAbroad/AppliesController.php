<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;
use App\Models\StudyAbroadApply;

class AppliesController extends Controller
{
    public function index()
    {
        $user = $this->my;
        $userId = $user->role == 'tutor' ? $user->id : null;
        $StudyAbroadApplies = StudyAbroadApply::orderBy('created_at', 'desc')
            ->with(
                'user.education',
                'applySchools.school',
                'applySchools.specialty'
            )
            ->when(request('status'), function($q) {
                return $q->where('status', request('status'));
            })
            ->when($userId, function($q) use ($userId){
               return $q->where('tutor_id', $userId);
            })
            ->when(request('keyword'), function($q){
                $q->whereHas('user', function($q){
                    $q->where('name', 'like', '%'.request('keyword').'%');
                });
                return $q;
            })
            ->paginate(8)
            ->appends(request()->all());

        foreach ($StudyAbroadApplies as $v){
            $v->append('submit_at_friendly', 'progress');
        }

        return response()->json($StudyAbroadApplies);
    }

    public function show($applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);

        $study_abroad_applies = $apply->append('progress')->toArray();
        $user = $apply->user->toArray();
        $user_education = $apply->user->education;
        $user_exams = $apply->user->exams;
        $tutor = $apply->tutor()->with('education', 'experience')->first();
        $meetings = $apply->meetings;

        return response()->json(compact(
            'study_abroad_applies',
            'user',
            'user_education',
            'user_exams',
            'tutor',
            'meetings'
        ));
    }

    public function update(Request $request, $id)
    {
        $studyAbroadApply = StudyAbroadApply::findOrFail($id);
        if( ! $studyAbroadApply) return response()->json(['status'=>'error']);

        switch ($request['status']) {
            case 'start':
                $studyAbroadApply->statusTo()->start();
                break;
            case 'restart':
                $studyAbroadApply->statusTo()->restore();
                break;
            case 'stop':
                $studyAbroadApply->statusTo()->stop($request['remarks']);
                break;
            case 'complete':
                $studyAbroadApply->statusTo()->complete();
                break;
        }

        return response()->json(['status'=>'success']);
    }

    public function destroy($id)
    {
        return response()->json(StudyAbroadApply::destroy($id));
    }
}
