<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadStrategy;
use App\Models\SchoolSpecialtyInfo;
use App\Models\StudyAbroadSpecialty;
use App\Models\StudyAbroadApplySchool;

class StrategyController extends Controller
{
    const VISA_MAP_KEY = 'visa';
    const DOCUMENT_MAP_KEY = 'document';
    const FEEDBACK_MAP_KEY = 'feedback';

    public function index()
    {
        $apply = StudyAbroadApply::with('promotion', 'onlineApply', 'schedule','strategy')
            ->findOrFail(request('applyId'))
            ->append('process','strategy_end');
        $specialties = StudyAbroadSpecialty::all();

        $res = [];
        if(!$apply->onlineApply) {
            $arr1 = SchoolSpecialtyInfo::getGroupSchoolsInfo($apply->major, $apply->school);
            $arr2 = SchoolSpecialtyInfo::getGroupSchoolsInfo($apply->secondary, $apply->secondary_school);
            $res = array_merge($arr1, $arr2);
        } else {
            $applySchools = StudyAbroadApplySchool::with('school')
            ->where('apply_id', request('applyId'))
            ->get();
            foreach($applySchools as $v){
                $res[$v->specialty_id]['specialty'] = $v->specialty->name;
                $res[$v->specialty_id]['school'][] = $v->toArray();
            }
        }
        $apply->apply_schools = $res;

        return response()->json(compact('apply', 'specialties'));
    }

    public function update(Request $request, $apply)
    {
        $apply = StudyAbroadApply::findOrFail($apply);

        //保存申请院校
        if($data = $request->apply_schools){
            self::saveApplySchools($apply, $data);
        } else {
            $apply->applySchools()->delete();
            $apply->onlineApply()->delete();
        }

        //保存提升背景
        if($data = $request->promotion){
            self::savePromotion($apply, $data);
        }

        //保存时间规划
        if($data = $request->schedule){
            self::saveschedule($apply, $data);
        }

        if($data= $request->strategy){
            $data = array_only($data, self::strategyAttributes());
            $apply->strategy()->updateOrCreate([], $data);
        }

        if($apply->strategy->progress == 1) $apply->statusTo()->record();
        //判断登陆者是不是质控,是质控发邮件
        if($apply->strategy->progress != 0) {
            $user = $this->my;
            if($user->role == 'tutor'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                $path = [
                    '留学申请服务',
                    config('params.translate')['strategy']
                ];
                $process['strategy'] = $request->strategy['lock'];
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('status-report', $emails, $view_data));
            }
        }

        return response()->json(['status'=>'success']);
    }

    private function strategyAttributes()
    {
        return StudyAbroadStrategy::getAttributeLabels();
    }

    private function applySchoolAttributes()
    {
        return StudyAbroadApplySchool::getAttributeLabels();
    }

    private function saveApplySchools($apply, $data)
    {
        $onlineApply = $apply->onlineApply();
        $applySchool = $apply->applySchools();
        $oldIds = $applySchool->pluck('id')->toArray();

        foreach ($data as $key => $value) {
            $ids[] = array_column($value['school'], 'id');
            if(count($value['school']) < 1)continue;
            foreach($value['school'] as $v){
                if (!empty($v['id']) && $find_model = $apply->applySchools()->find($v['id'])) {
                    $find_model->update(array_only($v, self::applySchoolAttributes()));
                } else {
                    $schoolInfo = SchoolSpecialtyInfo::getInfo($v['school_id'], $v['specialty_id']);
                    $v = array_merge($v, $schoolInfo);
                    $applySchool->create(array_only($v, self::applySchoolAttributes()));
                }
            }
        }
        $delIds = array_diff($oldIds, array_flatten($ids));
        (clone $applySchool)->whereIn('id', $delIds)->delete();
        if($applySchool->count()){
            $onlineApply->updateOrCreate([], []);
        }
    }

    private function savePromotion($apply, $data)
    {
        $data['content'] = array_values(array_filter($data['content']));
        if(count($data['content']) > 0) {
            if(array_key_exists('progress', $data)) unset($data['progress']);
            $apply->promotion()->updateOrCreate([], $data);
        } else {
            $apply->promotion()->delete();
        }
    }

    private function saveschedule($apply, $data)
    {
        $data['feedback'] = [];
        $processes = $apply->process;
        $prev = $processes[array_search(self::FEEDBACK_MAP_KEY,$processes) - 1];

        if($prev == self::VISA_MAP_KEY && $data['visa'] && $data['visa']['end']){
            $data['feedback']['start'] = $data['visa']['end'];
        }
        if($prev == self::DOCUMENT_MAP_KEY && $data['document'] && $data['document']['end']){
            $data['feedback']['start'] = $data['document']['end'];
        }
        if($data['feedback'] && $data['feedback']['start']){
            $date = $data['feedback']['start'];
            $data['feedback']['end'] = date('Y-m-d',strtotime("$date +7 day"));
        }

        $apply->schedule()->updateOrCreate([], $data);
    }
}
