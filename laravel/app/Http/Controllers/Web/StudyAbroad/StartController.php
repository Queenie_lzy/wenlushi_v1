<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use App\Models\StudyAbroadApply;
use App\Http\Requests\Web\StudyAbroadStartRequest as StartRequest;

class StartController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with('start','tutor.education', 'tutor.experience')
            ->findOrFail(request('applyId'))
            ->append('process');

        return response()->json($apply);
    }

    public function update(StartRequest $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);
        $startService = $apply->start;

        //更新导师
        if ($tutorId = $request->tutor_id) {
            $apply->update(['tutor_id' => $tutorId]);
        }

        $data = $request->getData();

        //只要服务范围改变，制定策略进度重置
        if ($startService) {
            if(array_key_exists('service_scope', $data)){
                self::refreshStrategy($apply, $data['service_scope']);
            }
            $startService->update($data);
        } else {
            $apply->start()->create($data);
        }

        //启动服务进度完成更新服务状态
        self::changeStatus($apply, $data['lock']);

        //判断登陆者是不是质控,是质控发邮件
        if(array_sum($data['lock']) >= 1) {
            $user = $this->my;
            if($user->role == 'admin'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                if($apply->tutor_id){
                    array_splice($emails, array_search($apply->tutor->email, $emails), 1);
                }
                $path = [
                    '留学申请服务',
                    config('params.translate')['start']
                ];
                $process['start'] = $data['lock'];
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('status-report', $emails, $view_data));
            }
        }

        return response()->json(['status' => 'success']);
    }

    private function refreshStrategy($apply, $serviceScope)
    {
        if($serviceScope != $apply->start->service_scope) {
            if($apply->strategy) {
                $apply->strategy()->update(['lock'=>null]);
            }
        }
    }

    private function changeStatus($apply, $locks)
    {
        if (!empty($locks) && count($locks) == array_sum($locks)) {
            $apply->statusTo()->start();
        } else {
            foreach ($locks as $k => $v) {
                if(!$v){
                    $apply->statusTo()->record($k);
                    break;
                }
            }
        }
    }
}
