<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadFeedback;
use App\Http\Requests\Web\StudyAbroadFeedbackRequest as FeedbackRequest;

class FeedbackController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with('feedback','schedule')
            ->findOrFail(request('applyId'))
            ->append('process');

        $apply->progresses = array_keys($apply->getProgress());

        return response()->json($apply);
    }

    public function store(FeedbackRequest $request)
    {
        StudyAbroadApply::findOrFail($request->applyId)
            ->feedback()->updateOrCreate([], $request->getNonEmptyStringData());

        return response()->json(['status'=>'success']);
    }

    public function update(Request $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);

        if($data = $request->all()){
            if(array_key_exists('progress', $data)) unset($data['progress']);
            $apply->feedback()->updateOrCreate([],$data);
        }

        return response()->json(['status'=>'success']);
    }
}
