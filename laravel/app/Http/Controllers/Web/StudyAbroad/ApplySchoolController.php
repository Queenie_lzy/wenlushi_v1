<?php

namespace App\Http\Controllers\Web\StudyAbroad;

use Illuminate\Http\Request;

use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadSchool;
use App\Models\SchoolSpecialtyInfo;
use App\Models\StudyAbroadSpecialty;
use App\Models\StudyAbroadApplySchool;
use App\Models\StudyAbroadOnlineApply;
use App\Http\Requests\Web\StudyAbroadApplySchoolRequest as ApplySchoolRequest;

class ApplySchoolController extends Controller
{
    public function index()
    {
        $apply = StudyAbroadApply::with(
                'applySchools.specialty',
                'applySchools.school',
                'schedule',
                'onlineApply'
            )
            ->findOrFail(request('applyId'))
            ->append('process');

        return response()->json($apply);
    }

    public function update(Request $request, $applyId)
    {
        $apply = StudyAbroadApply::findOrFail($applyId);

        if($data = $request->apply_schools){
            $applySchool = $apply->applySchools();
            $ids = array_column($data, 'id');
            (clone $applySchool)->whereNotIn('id', $ids)->delete();
            $_applySchool = (clone $applySchool)->whereIn('id', $ids)->get();
            foreach ($data as $v) {
                if (!empty($v['id']) && $find_model = $_applySchool->find($v['id'])) {
                    $find_model->update(array_only($v, self::applySchoolAttributes()));
                }
            }
        } else {
            $apply->applySchools()->delete();
        }

        if($data = $request->online_apply){
            $data = array_only($data, self::onlineApplyAttributes());
            $apply->onlineApply()->updateOrCreate([], $data);
        }

        //更新状态
        if($apply->onlineApply->progress == 1) $apply->statusTo()->record();
        if($apply->onlineApply->progress != 0) {
            $user = $this->my;
            if($user->role == 'admin'){
                //发送邮件通知
                $emails = $apply->getEmailUsers($user->id);
                if($apply->tutor_id){
                    array_splice($emails, array_search($apply->tutor->email, $emails), 1);
                }
                $path = [
                    '留学申请服务',
                    config('params.translate')['online_apply']
                ];
                $process['online_apply'] = $apply->onlineApply->getProgress($apply->id);
                $view_data = compact('user', 'path', 'process');
                \Event::fire(new \App\Events\EmailEvent('status-report', $emails, $view_data));
            }
         }

        return response()->json(['status'=>'success']);
    }

    public function destroy($id)
    {
        $onlineApply = StudyAbroadOnlineApply::where('apply_school_id', $id)->first();
        if($onlineApply) $onlineApply->update(['apply_school_id' => null]);
        $applySchool = StudyAbroadApplySchool::findOrFail($id);
        $applySchool->delete();

        return response()->json();
    }

    private function applySchoolAttributes()
    {
        return StudyAbroadApplySchool::getAttributeLabels();
    }

    private function onlineApplyAttributes()
    {
        return StudyAbroadOnlineApply::getAttributeLabels();
    }
}
