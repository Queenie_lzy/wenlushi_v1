<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * role:'student','service','tutor','admin'
     * searchword: 模糊搜索/导师联盟列表页需求
     * service: 问题解答/留学服务
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $models = User::orderBy('id', 'desc')
            ->when(request('keyword'), function ($q) {
                return $q->where('name', 'like', '%' . request('keyword') . '%');
            })
            ->when(request('contact'), function ($q) {
                return $q->where(function($q){
                    $q->where('email', 'like', '%' . request('contact') . '%')
                        ->orWhere('cellphone', 'like', '%' . request('contact') . '%');
                });
            })
            ->when(request('roles'), function ($q) {
                $roles = request('roles');
                if (is_array($roles)) {
                    $q->whereIn('role', $roles);
                } else {
                    $q->where('role', $roles);
                }
                return $q;
            })
            ->when(request('service'), function($q){
                return $q->WhereHas('serviceTags', function($k){
                        $k->where('name', request('service'));
                    });
            })
            ->when(request('searchword')!='all', function($q){
                return $q->where(function($q){
                    $q->orWhere('name', 'like', '%'.request('searchword').'%')
                        ->orWhere('occupation', 'like', '%'.request('searchword').'%')
                        ->orWhere('brief', 'like', '%'.request('searchword').'%')
                        ->orWhereHas('education', function($k){
                            return $k->where(function($k){
                                    $k->where('specialty', 'like', '%'.request('searchword').'%')
                                        ->orWhere('school', 'like', '%'.request('searchword').'%');
                                });
                        })
                        ->orWhereHas('skills', function($k){
                            return $k->where('name', 'like', '%'.request('searchword').'%');
                        })
                        ->orWhereHas('experience', function($k){
                            return $k->where('company', 'like', '%'.request('searchword').'%');
                        })
                        ->orWhereHas('serviceTags', function($k){
                            return $k->where('name', 'like', '%'.request('searchword').'%');
                        })
                        ->orWhereHas('supplements', function($k){
                            return $k->where('content', 'like', '%'.request('searchword').'%');
                        });
                });
            })
            ->when(request('with'), function ($q) {
                $with = request('with');
                if (count($with) > 0) {
                    $q->with($with);
                }
                return $q;
            });
        if(request('all')) {
            $users = $models->get();
        } else {
            $users = $models->paginate()->appends(request()->all());
        }

        if(request('service_tags')){
            foreach ($users as $user) {
                $user->service_tags = $user->serviceTags;
            }
        }

        return response()->json($users);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        if(request('service_tags')) {
            $user->service_tags = $user->serviceTags;
        }
        if(request('with')){
            $with = request('with');
            if (is_array($with) && count($with) > 0) {
                $user->load($with);
            }
        }else{
            $user->load(
                'experiences',
                'educations',
                'exams',
//                'scholarships',
                'supplements'
            );
        }

        return response()->json($user);
    }
}
