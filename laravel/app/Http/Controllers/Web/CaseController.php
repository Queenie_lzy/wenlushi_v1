<?php

namespace App\Http\Controllers\Web;

use DB;
use App\Models\User;
use App\Models\StudyAbroadApply;
use App\Models\StudyAbroadSpecialty;

class CaseController extends Controller
{
    public function index()
    {
        $specialties = StudyAbroadSpecialty::all();
        foreach($specialties as $specialty) {
            $case_groups[$specialty->name] = StudyAbroadApply::with('user.education', 'user.exams')
                ->where('status', 'complete')
                ->when(request('lastYear'), function($q){
                    return $q->where('created_at', '<', date("Y-01-01"));
                })
                ->when(request('applyYear'), function($q){
                    return $q->where('season', 'like', '%'.request('applyYear').'%');
                })
                ->wherehas('applySchools', function($q) use($specialty){
                    $q->has('onlineApply')
                    ->where('specialty_id', $specialty->id);
                })
                ->with(['applySchools' => function($q) use ($specialty){
                    $q->with('school', 'specialty')
                    ->has('onlineApply')
                    ->where('specialty_id', $specialty->id);
                }])
                ->get();
        }

        return response()->json(compact('case_groups'));
    }
}
