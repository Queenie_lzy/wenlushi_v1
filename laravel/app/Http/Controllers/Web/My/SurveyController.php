<?php

namespace App\Http\Controllers\Web\My;

use App\Http\Requests\Web\SurveyRequest;

class SurveyController extends Controller
{
    public function index()
    {
        return response()->json();
    }

    public function store(SurveyRequest $request)
    {
        $my = $this->my;
        $data = $request->getNonEmptyStringData();
        if($data) {
            $survey = $my->survey()->create($data);
        }
    }
}
