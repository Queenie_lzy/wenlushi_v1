<?php

namespace App\Http\Controllers\Web\My;

use App\Models\User;
use App\Http\Requests\Web\MyStudyAbroadRequest;
use App\Repositories\AuthRepository;

class StudyAbroadController extends Controller
{
    public function index()
    {
        $my = $this->my;
        $user = $my->toArray();
        $study_abroad_applies = $my->studyAbroadApply;

        if(request('ac')){
            switch (request('ac')){
                case 'service':
                    if($study_abroad_applies){
                        $study_abroad_applies->load(
                            'tutor.experience',
                            'tutor.education',
                            'applySchools.school',
                            'applySchools.specialty'
                            );
                        $study_abroad_applies->progresses = $study_abroad_applies->getProgress();
                    }
                    $meeting = $my->meetings()->with('tutor.education')
                        ->orderBy('id', 'DESC')->first();
                    return response()->json([
                        'study_abroad_applies' => $study_abroad_applies,
                        'meeting' => $meeting
                    ]);
                    break;
            }
        }

        $user_experiences = $my->experiences;
        $user_educations = $my->educations;
        $user_exams = $my->exams;
//        $user_scholarships = $my->scholarships;
        $user_skills = $my->skills;
        $user_supplements = $my->supplements;
        return response()->json(compact(
            'study_abroad_applies',
            'user',
            'user_experiences',
            'user_educations',
            'user_exams',
            'user_skills',
//            'user_scholarships',
            'user_supplements'
        ));
    }

    //$request->action: apply(留学申请)/user(用户个人资料)
    public function store(MyStudyAbroadRequest $request)
    {
        $user = $this->my;
        $hasApply = $user->studyAbroadApply()->first();

        //留学意愿
        if ($data = $request->study_abroad_applies) {
            $data = array_only($data, [
                'type',
                'country',
                'season',
                'major',
                'secondary',
                'school',
                'secondary_school',
                'other_requirement'
            ]);
            $data['id'] = $user->id;
            $apple = $user->studyAbroadApply()->updateOrCreate([], $data);
            if ($request->action_type == 'submit' && is_null($apple->status)) {
                $apple->statusTo()->submit();
            }
        }

        //个人信息
        if ($data = $request->user) {
            $user->update($data);
        }

        (new AuthRepository())->saveResume($request);

        //发邮件通知质控
        if($request->action == 'apply' && $request->action_type == 'submit'){
            $admins = User::whereRole('admin')->pluck('email')->toArray();
            $isEdit = $hasApply ? true : false;
            $view_data = compact('user', 'isEdit');
            \Event::fire(new \App\Events\EmailEvent('apply_message', $admins, $view_data));
        }

        return $this->index();
    }
}
