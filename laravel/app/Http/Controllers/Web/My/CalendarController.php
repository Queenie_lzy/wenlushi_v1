<?php

namespace App\Http\Controllers\Web\My;

use App\Models\Calendar;
// use App\Http\Requests\Web\CalendarRequest;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {
        $user = $this->my;
        $calendars = $user->calendars()->with('meeting.user')
            ->when(request('date'), function($q){
                return $q->where('date', request('date'));
            })
            ->where('date', '>=', date('Y-m-d'))
            ->get();

        return response()->json($calendars);
    }

    public function show($id)
    {
        return response()->json();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = $this->my;

        foreach ($data as $key => $value) {
            if(array_key_exists('id', $value)){
                if(array_key_exists('meeting', $value)) unset($value['meeting']);
                $user->calendars()->find($value['id'])->update(array_filter($value));
            } else {
                $user->calendars()->create(array_filter($value));
            }
        }

        return response()->json('success');
    }

    public function destroy($id)
    {
        Meeting::find($id)->delete();

        return response()->json();
    }
}
