<?php

namespace App\Http\Controllers\Web\My;

use App\Http\Requests\Web\MyPasswordRequest;

class DefaultController extends Controller
{
    public function getIndex()
    {
        return response()->json($this->my->load('education','serviceTags'));
    }

    public function getLogout()
    {
        auth()->logout();
        return response()->json();
    }

    public function putPassword(MyPasswordRequest $request)
    {
        $this->my->update([
            'password' => $request->password
        ]);
        return response()->json();
    }

    public function putAvatar()
    {
        if(request('avatar')){
            $this->my->update([
                'avatar' => request('avatar')
            ]);
        }
        return response()->json();
    }

    public function getResume()
    {
        $my = $this->my;

        $user = $my->toArray();
        $user_experiences = $my->experiences;
        $user_educations = $my->educations;
        $user_exams = $my->exams;
        $user_skills = $my->skills;
//        $user_scholarships = $my->scholarships;
        $user_supplements = $my->supplements;
        return response()->json(compact(
            'user',
            'user_experiences',
            'user_educations',
            'user_exams',
            'user_skills',
//            'user_scholarships',
            'user_supplements'
        ));
    }

    public function getDraft()
    {
        $user = $this->my;
        $draft = $user->articles()->whereStatus('draft')->first();

        return response()->json($draft);
    }

    //request: anotherUserId
    public function getCheckRelated()
    {
        $user = $this->my;
        $hasApply = $user->studyAbroadApply()
            ->where('tutor_id', request('anotherUserId'))
            ->exists();
        $hasMeetings = $user->meetings()
            ->where('tutor_id', request('anotherUserId'))
            ->exists();
        if($user->role == 'admin' || $hasApply || $hasMeetings){
            return response()->json(true);
        }
        return response()->json(false);
    }
}
