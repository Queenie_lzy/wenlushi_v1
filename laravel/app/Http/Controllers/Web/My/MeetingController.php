<?php

namespace App\Http\Controllers\Web\My;

use App\Models\Meeting;
use App\Http\Requests\Web\UserMeetingRequest as MeetingRequest;

class MeetingController extends Controller
{
    public function index()
    {
        $user = $this->my;
        $meetings = Meeting::with('tutor','calendar')
            ->whereUserId($user->id)
            ->orderBy('status', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(10);

        foreach ($meetings as $key => $value) {
            $value->append('time_calculate');
        }

        return response()->json($meetings);
    }

    public function show($id)
    {
        return response()->json();
    }

    public function store(MeetingRequest $request)
    {
        $data = $request->getNonEmptyStringData();
        $data['session'] = $request->session;
        $user = $this->my;

        if(array_key_exists('id', $data)){
            $user->meetings()->find($data['id'])->update($data);
        } else {
            $data['apply_id'] = array_key_exists('apply_id', $data) ? $user->id : null;
            $user->meetings()->create($data);
        }
        return response()->json('success');
    }

    public function destroy($id)
    {
        Meeting::find($id)->delete();

        return response()->json();
    }
}
