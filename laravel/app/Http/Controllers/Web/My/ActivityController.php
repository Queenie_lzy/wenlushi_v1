<?php

namespace App\Http\Controllers\Web\My;

use App\Models\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function index()
    {
        $my = $this->my;
        if($my->role == 'tutor') {
            $activities = $my->activityGuests()->with('guests.education')->get();
        } else {
            $activities = $my->activityRegisters()->with('guests.education')->get();
        }

        return response()->json($activities);
    }

    //报名参加活动
    public function store(Request $request)
    {
        $activity = Activity::findOrFail($request->activity_id);
        if($activity->registers()->count() == $activity->maximum){
            return response()->json(['status'=>'error','msg'=>'活动报名人数已满']);
        }
        $activity->registers()->attach($this->my->id);

        return response()->json();
    }
}