<?php

namespace App\Http\Controllers\Web\My;

use App\Models\Tag;
use App\Models\User;
use App\Models\Article;
use App\Models\Activity;
use App\Http\Requests\Web\ArticleRequest;

class ArticleController extends Controller
{
    public function index()
    {
        $user = $this->my;
        $userId = $user->role == 'admin' ? request('userId') : $user->id;
        $articles = Article::with('user','activity')
            ->where('status', '<>', 'draft')
            ->orderBy('id', 'DESC')
            ->when(request('keyword'), function($q){
                return $q->where('title', 'LIKE', '%'.request('keyword').'%');
            })
            ->when(request('status'), function($q){
                return $q->whereStatus(request('status'));
            })
            ->when($userId, function($q) use ($userId){
                return $q->whereUserId($userId)
                    ->whereType('article');
            })
            ->paginate()
            ->appends(array_filter(request()->all()));

        return response()->json($articles);
    }

    public function show($id)
    {
        $article = Article::with(
            'activity.guest.education',
            'activity.guest.experience',
            'user.education',
            'user.experience'
        )->findOrFail($id);
        $article->tags = implode(",", $article->tags()->pluck('name')->toArray());

        return response()->json($article);
    }

    public function store(ArticleRequest $request)
    {
        $user = $this->my;
        $data = $request->getNonEmptyStringData();
        $article = $user->articles()->create(array_only($data, self::articleAttributes()));

        if(array_key_exists('tags', $data)) {
            self::createTages($article, $data['tags']);
        }

        if(array_key_exists('type', $data) && $data['type'] == 'activity') {
            $activity = array_only($data['activity'], self::activityAttributes());
            $article->activity()->create($activity);
        }

        return response()->json();
    }

    public function update(ArticleRequest $request, $id)
    {
        $article = Article::findOrFail($id);
        $data = $request->getNonEmptyStringData();

        //处理标签
        if(array_key_exists('tags', $data)) {
            self::updateTags($article, $data['tags']);
        }
        //处理活动
        if(array_key_exists('type', $data) && $data['type'] == 'activity') {
            $activity = array_only($data['activity'], self::activityAttributes());
            $article->activity()->updateOrCreate([],$activity);
        }
        if($data['status'] != 'publish'){
            $data['updated_at'] = date('Y-m-d H:i:s');
        }

        $article->update(array_only($data, self::articleAttributes()));

        return response()->json();
    }

    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->tags()->detach();
        $article->delete();

        return response()->json('success');
    }

    private function articleAttributes()
    {
        return Article::getAttributeLabels();
    }

    private function activityAttributes()
    {
        return Activity::getAttributeLabels();
    }

    private function dataHandle($data)
    {
        $data = str_replace('，', ',', $data);
        $values = explode(",", $data);
        foreach ($values as $key => $value) {
            $values[$key] = trim($value);
        }
        return array_filter(array_unique($values));
    }

    private function createTages($article, $data)
    {
        $tags = self::dataHandle($data);
        foreach ($tags as $key => $value) {
            $tag = Tag::whereName($value)->first();
            if(!$tag){
                $tag = Tag::create(['name'=>$value]);
            }
            $article->tags()->attach($tag->id);
        }
    }

    private function updateTags($article, $data)
    {
        $tags = self::dataHandle($data);
        $oldTags = $article->tags()->pluck('name')->toArray();
        foreach ($tags as $key => $value) {
            if(in_array($value, $oldTags)) {
                array_splice($oldTags, array_search($value, $oldTags), 1);
                continue;
            }
            $tag = Tag::whereName($value)->first();
            if(!$tag){
                $tag = Tag::create(['name'=>$value]);
            }
            $article->tags()->attach($tag->id);
        }
        $delTags = array_values($oldTags);
        foreach ($delTags as $key => $value) {
            $tag = Tag::whereName($value)->first();
            $article->tags()->detach($tag->id);
        }
    }
}
