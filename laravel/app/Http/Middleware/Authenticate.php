<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class Authenticate
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) return $next($request);
        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'message' => 'Unauthorized.',
                'code' => 401
            ], 401);
        }
        switch (Auth::getDefaultDriver()) {
            case 'web':
                return redirect()->action('Web\WechatController@getLogin');
            default:
                return redirect('/');
        }
    }
}
