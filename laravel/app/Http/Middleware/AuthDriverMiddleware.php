<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthDriverMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard) Auth::setDefaultDriver($guard);

        if (env('APP_DEBUG') && is_numeric(request('login_id'))) {
            Auth::loginUsingId(request('login_id'), true);
        }

        $not_json = !$request->wantsJson() && !$request->ajax();
        $not_allow_path = $request->path() != '/' && $request->path() != 'admin';
        $not_web = !$request->get('web');

        if ($not_json && $not_allow_path && $not_web) {
            $request_url = request()->getRequestUri();
            if (array_first(array_filter(explode('/', $request_url))) != 'admin') {
                return redirect('/#' . $request_url);
            } else {
                return redirect('/admin#' . $request_url);
            }
        }
        return $next($request);
    }
}
