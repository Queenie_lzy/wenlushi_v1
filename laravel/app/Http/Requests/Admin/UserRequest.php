<?php

namespace App\Http\Requests\Admin;

class UserRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required',
            'nickname' => '',
            'avatar' => '',
            'email'    => 'required|email|max:100',
            'password' => 'required_with:password_confirmation|min:5|confirmed',
            'password_confirmation' => '',
            'role'     => 'required',
            'duration' => '',
            'service_tags' => 'array',
            'education' => 'array'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }

    protected function getRulesData(){
        $data = parent::getRulesData();
        unset($data['password_confirmation']);
        return $data;
    }
}
