<?php

namespace App\Http\Requests\Admin;

class LoginRequest extends Request
{
    public function rules()
    {
        return [
            'email'    => 'required|email|max:100|exists:administrators',
            'password' => 'required|min:5',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
