<?php

namespace App\Http\Requests\Admin;

class SchoolRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
            'en_name' => '',
            'logo' => 'required'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
