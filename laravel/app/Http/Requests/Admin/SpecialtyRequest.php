<?php

namespace App\Http\Requests\Admin;

class SpecialtyRequest extends Request
{
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
