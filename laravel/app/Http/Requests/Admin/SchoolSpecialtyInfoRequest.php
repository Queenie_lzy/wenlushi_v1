<?php

namespace App\Http\Requests\Admin;

class SchoolSpecialtyInfoRequest extends Request
{
    public function rules()
    {
        return [
            'school_id' => 'required',
            'specialty_id'  => 'required',
            'grade' => 'array',
            'deadline_at' => '',
            'inform_at' => '',
            'department' => '',
            'link' => ''
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
