<?php

namespace App\Http\Requests\Admin;

class MyRequest extends Request
{
    public function rules()
    {
        return [
            'nickname' => 'required',
            'email' => 'required|unique:administrators,email,'.auth()->id(),
            'password' => 'required_with:password_confirmation|confirmed',
            'password_confirmation' => '',
        ];
    }

    public function messages()
    {
        return [
            'password.confirmed' => '密码 与 确认密码 不一致',
            'password.required_with' => '密码 与 确认密码 不一致',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }

    protected function getRulesData()
    {
        $data = parent::getRulesData();
        unset($data['password_confirmation']);

        return $data;
    }
}
