<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function responseError($key, $error_text){
        return $this->response([
            $key => [
                $error_text
            ]
        ]);
    }

    protected function emptyToNull($data)
    {
        foreach ($data as $k => $v) {
            if (is_string($v) && trim($v) == '') {
                $data[$k] = null;
            } elseif (is_array($v)) {
                $data[$k] = $this->emptyToNull($v);
            }
        }

        return $data;
    }

    public function getNonEmptyStringData()
    {
        return $this->executeFiltering(true);
    }

    public function getData()
    {
        return $this->executeFiltering();
    }

    protected function getRulesData()
    {
        if (method_exists($this, 'rules')) {
            return $this->rules();
        }
        return [];
    }

    protected function hasPrefixRule($prefix, $rules)
    {
        foreach ($rules as $key => $value) {
            if(strstr($value, 'array')){
                return true;
            }
            if (starts_with($key, $prefix)) {
                return true;
            }
        }

        return false;
    }

    private function filterParams(&$params, $rules, $prefix, $is_clean_empty_string)
    {
        foreach ($params as $key => &$value) {
            if ($is_clean_empty_string && empty($value) && $value != '0') {
                //移除空字符串
                unset($params[$key]);
            } elseif (is_array($value)) {
                $p = ($prefix ? $prefix.'.' : '').(is_integer($key) ? '*' : $key);
                $this->filterParams($value, $rules, $p, $is_clean_empty_string);
            } elseif (! is_integer($key)) {
                $p = ($prefix ? $prefix.'.' : '').$key;
                if (! $this->hasPrefixRule($p, $rules)) {
                    unset($params[$key]);
                }
            }
        }
    }

    private function executeFiltering($is_clean_empty_string = false)
    {
        $rules = $this->getRulesData();

        $keys = array_unique(array_build(array_keys($rules), function ($k, $v) {
            return [$k, explode('.', $v)[0]];
        }));

        $credentials = $this->only($keys);
        $this->filterParams($credentials, $rules, '', $is_clean_empty_string);

        return $credentials;
    }
}
