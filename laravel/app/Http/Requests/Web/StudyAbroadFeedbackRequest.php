<?php

namespace App\Http\Requests\Web;

class StudyAbroadFeedbackRequest extends Request
{
    public function rules()
    {
        return [
            'whole_evaluation' => 'required|integer',
            'prospect_evaluation' => 'required|integer',
            'strategize' => 'required|integer',
            'promotion' => 'required|integer',
            'document' => 'required|integer',
            'online_apply' => 'required|integer',
            'visa_training' => 'required|integer',
            'service_communication' => 'required|integer',
            'service_price' => 'required|integer',
            'use_service' => 'required',
            'recommend_service' => 'required',
            'service_again' => 'required',
            'advice' => ''
        ];
    }

    public function messages()
    {
        return [
            'whole_evaluation.required' => '整体评价不能为空。',
            'prospect_evaluation.required' => '前景评估不能为空。',
            'strategize.required' => '策略制定不能为空。',
            'promotion.required' => '背景提升不能为空。',
            'document.required' => '文书创作不能为空。',
            'online_apply.required' => '网络申请不能为空。',
            'visa_training.required' => '签证培训不能为空。',
            'service_communication.required' => '服务沟通不能为空。',
            'service_price.required' => '服务价格不能为空。',
            'use_service.required' => '请勾选是否使用过其他机构的类似服务。',
            'recommend_service.required' => '请勾选是否会向朋友推荐问路石教育的留学申请服务。',
            'service_again.required' => '请勾选是否会再次选择问路石教育所提供的服务。',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
