<?php

namespace App\Http\Requests\Web;

class CalendarRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'id' => '',
            // 'time_zone'    => '',
            // 'date'    => 'required',
            // 'period'    => 'required',
            // 'hour'    => 'required',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
