<?php

namespace App\Http\Requests\Web;

class MessageRequest extends Request
{
    public function rules()
    {
        return [
            'content' => 'required'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
