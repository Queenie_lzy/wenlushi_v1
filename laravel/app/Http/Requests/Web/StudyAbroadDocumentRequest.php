<?php

namespace App\Http\Requests\Web;

class StudyAbroadDocumentRequest extends Request
{
    public function rules()
    {
        return [
            'type' => 'required',
            'file' => ''
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
