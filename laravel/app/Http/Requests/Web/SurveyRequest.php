<?php

namespace App\Http\Requests\Web;

class SurveyRequest extends Request
{
    public function rules()
    {
        return [
            'wechat' => '',
            'network' => '',
            'brochure' => '',
            'friend' => '',
            'activity' => '',
            'other' => ''
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
