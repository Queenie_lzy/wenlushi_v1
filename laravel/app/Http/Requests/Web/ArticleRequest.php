<?php

namespace App\Http\Requests\Web;

class ArticleRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => '',
            'title'    => 'required',
            'tags'    => '',
            'status'    => '',
            'cover'    => 'required_unless:status,draft',
            'content'    => 'required_unless:status,draft',
            'type' => '',
            'login_access' => '',
            'author' => '',
            'remark' => '',
            'activity' => 'array',
            'activity.user_id' => 'required_if:type,activity',
            'activity.start' => 'required_if:type,activity',
            'activity.end' => 'required_if:type,activity',
            'activity.deadline_at' => 'required_if:type,activity',
            'activity.maximum' => 'required_if:type,activity',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '标题不能为空。',
            'cover.required_unless' => '封面图不能为空。',
            'content.required_unless' => '正文不能为空。',
            'activity.user_id.required_if' => '嘉宾不能为空。',
            'activity.start.required_if' => '活动开始时间不能为空。',
            'activity.end.required_if' => '活动结束时间不能为空。',
            'activity.deadline_at.required_if' => '截止日期不能为空。',
            'activity.maximum.required_if' => '名额限制不能为空。',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
