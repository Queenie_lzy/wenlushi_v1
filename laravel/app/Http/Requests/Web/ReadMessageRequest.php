<?php

namespace App\Http\Requests\Web;

class ReadMessageRequest extends Request
{
    public function rules()
    {
        return [
            'period' => 'required',
            'apply_id' => 'required',
            'read_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
