<?php

namespace App\Http\Requests\Web;

class StudyAbroadApplySchoolRequest extends Request
{
    public function rules()
    {
        return [
            'school_id' => 'required',
            'specialty_id' => 'required',
            'professor' => ''
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
