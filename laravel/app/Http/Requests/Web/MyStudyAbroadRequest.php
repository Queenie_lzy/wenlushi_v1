<?php

namespace App\Http\Requests\Web;

class MyStudyAbroadRequest extends Request
{
    public function rules()
    {
        return [
            'study_abroad_applies'    => 'required_with:action_type|array',
            'user' => 'required|array',
            'user_experiences' => 'array',
            'user_educations' => 'array',
            'user_exams' => 'array',
            'user_scholarships' => 'array',
            'user_supplements' => 'array',
            'action_type' => 'in:save,submit',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }

    protected function getRulesData()
    {
        $rules = parent::getRulesData();
        unset($rules['action_type']);
        return $rules;
    }
}
