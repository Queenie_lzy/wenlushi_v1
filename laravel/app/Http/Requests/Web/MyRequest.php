<?php

namespace App\Http\Requests\Web;

class MyRequest extends Request
{
    public function rules()
    {
        return [
            'user' => 'required|array',
            'user_experiences' => 'array',
            'user_educations' => 'array',
            'user_exams' => 'array',
            'user_scholarships' => 'array',
            'user_supplements' => 'array',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
