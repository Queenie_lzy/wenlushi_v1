<?php

namespace App\Http\Requests\Web;

class UserMeetingRequest extends Request
{
    public function rules()
    {
        $mode = $this->request->get('mode');
        $operation = $this->request->get('operation');
        $rules = [
            'theme' => 'required',
            'tutor_id' => 'required',
            'mode' => 'required',
            'calendar_id' => '',
            'duration' => '',
            'description' => 'required',
            'apply_id' => '',
            'status' => '',
            'session' => 'required_if:mode,"语音会议"',
            'operation' => '',
            'answer' => 'required_if:status,"service-in"',
            'remark' => '',
        ];
        if($mode == '语音会议') {
            switch ($operation) {
                case 'service-in':
                    $rules['calendar_id'] = 'required';
                    break;
                case 'resolve':
                    $rules['duration'] = 'required';
                    break;
            }
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'theme.required' => '主题不能为空。',
            'description.required' => '描述不能为空。',
            'mode.required' => '应答模式不能为空。',
            'tutor_id.required' => '导师不能为空。',
            'session.required_if' => '预约时间不能为空。',
            'answer.required_if' => '解答不能为空。',
            'calendar_id.required' => '没有会议时间无法受理用户提问。-请选择会议时间',
            'duration.required' => '时长不能为空。',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
