<?php

namespace App\Http\Requests\Web;

class AuthLoginRequest extends Request
{
    public function rules()
    {
        return [
            'email'    => 'required|email|exists:users',
            'password' => 'required|string|between:6,100',
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
