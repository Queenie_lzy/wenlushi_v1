<?php

namespace App\Http\Requests\Web;

class ActivityRequest extends Request
{
    public function rules()
    {
        return [
            'title'    => 'required',
            'tags'    => '',
            'cover'    => '',
            'description'    => '',
            'background' => '',
            'zone_time' => '',
            'start' => 'required',
            'end' => 'required',
            'deadline_at' => 'required',
            'offline_address' => '',
            'online_contact' => '',
            'maximum' => '',
            'guests' => 'array'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '标题不能为空',
            'start.required' => '活动开始时间不能为空',
            'end.required' => '活动结束时间不能为空',
            'deadline_at.required' => '截止时间不能为空'
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
