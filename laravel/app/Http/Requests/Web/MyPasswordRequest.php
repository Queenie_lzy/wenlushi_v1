<?php

namespace App\Http\Requests\Web;

class MyPasswordRequest extends Request
{
    public function rules()
    {
        return [
            'old_password' => 'required|check_password',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'password.confirmed' => '新密码 与 确认密码 不一致',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }

    protected function getRulesData()
    {
        $data = parent::getRulesData();
        unset($data['password_confirmation']);

        return $data;
    }
}
