<?php

namespace App\Http\Requests\Web;

class AuthResetPasswordRequest extends Request
{
    public function rules()
    {
        return [
            'email'       => 'required|email|exists:users,email',
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => '邮箱未注册',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
