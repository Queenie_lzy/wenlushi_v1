<?php

namespace App\Http\Requests\Web;

class StudyAbroadStartRequest extends Request
{
    public function rules()
    {
        return [
            'evaluation' => '',
            'seller_conversation' => '',
            'expert_conversation' => '',
            'service_scope' => '',
            'document_type' => 'array',
            'contract' => 'array',
            'lock' => 'array',
        ];
    }

    public function messages()
    {
        return [
            'evaluation.required' => '背景评价不能为空',
            'seller_conversation.required' => '销售约谈不能为空',
            'expert_conversation.required' => '导师约谈不能为空',
            'service_scope.required' => '服务范围不能为空',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
