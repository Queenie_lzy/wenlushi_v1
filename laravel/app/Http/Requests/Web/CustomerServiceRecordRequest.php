<?php

namespace App\Http\Requests\Web;

class CustomerServiceRecordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name'    => 'required',
            'email'    => 'required',
            'cellphone_code'    => 'required',
            'cellphone'    => 'required',
            'contact_tool'    => 'required',
            'contact_tool_no'    => 'required',
            'summary'    => 'required',
            'service_tags' => 'array'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => '必须选择已注册的的用户。',
            'name.required' => '用户名不能为空。',
            'email.required' => '邮箱不能为空。',
            'cellphone_code.required' => '手机所属地区名不能为空。',
            'cellphone.required' => '手机不能为空。',
            'contact_tool.required' => '通讯工具不能为空。',
            'contact_tool_no.required' => '通讯号码不能为空。',
            'summary.required' => '纪要不能为空。',
        ];
    }

    public function attributes()
    {
        return [
        ];
    }
}
