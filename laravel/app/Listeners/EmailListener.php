<?php

namespace App\Listeners;

use App\Events\EmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailEvent $event
     * @return void
     */
    public function handle(EmailEvent $event)
    {
        $email = $event->email;
        $view_data = $event->view_data;
        switch ($event->type) {
            case 'active':
                $this->send(
                    'mail.active',
                    [
                        'email' => $email,
                        'key' => $view_data['key']
                    ],
                    $email,
                    '问路石 - 账号激活'
                );
                break;
            case 'message':
                $subject = '问路石 - '
                    . implode('-', $view_data['path'])
                    . '-'
                    . config('params.role')[$view_data['user']->role]
                    . '留言'
                    . (isset($view_data['remark']) ? '-'.$view_data['remark'] : null);

                $this->send(
                    'mail.message',
                    $view_data,
                    $email,
                    $subject
                );
                break;
            case 'apply_message':
                $subject = '问路石 - '
                    . '留学申请服务 - 前景评估'
                    . ' - '
                    . ($view_data['isEdit'] ? '修改' : '请求')
                    . ' - '
                    . $view_data['user']->name;

                $this->send(
                    'mail.apply_message',
                    $view_data,
                    $email,
                    $subject
                );
                break;
            case 'status-report':
                $subject = '问路石 - '
                    . implode('-', $view_data['path'])
                    . '-状态更新';

                $this->send(
                    'mail.message',
                    $view_data,
                    $email,
                    $subject
                );
                break;
            case 'reset-password':
                $this->send(
                    'mail.reset_password',
                    $view_data,
                    $email,
                    '问路石 - 重置密码'
                );
                break;
        }
    }

    private function send($view_path, $view_data, $to, $subject)
    {
        \Mail::send($view_path, $view_data, function ($m) use ($to, $subject) {
            /**
             * @var \Illuminate\Mail\Message $m
             */
            $m->from(env('MAIL_USERNAME'), '问路石');
            $m->to($to);
            $m->subject($subject);
        });
    }
}
