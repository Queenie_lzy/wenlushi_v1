<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class QueryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QueryExecuted $event
     * @return void
     */
    public function handle(QueryExecuted $event)
    {
        $this->generateLog($event);
    }

    private function generateLog($event)
    {
        $sql = $event->sql;
        foreach ($event->bindings as $val) {
            $sql = preg_replace('/\?/', "'{$val}'", $sql, 1);
        }
        $sql = $sql . ' (' . $event->time . 'ms)';
        //__METHOD__.'['.__LINE__.']'.
        error_log($sql);
        \Log::info($sql);
    }
}
