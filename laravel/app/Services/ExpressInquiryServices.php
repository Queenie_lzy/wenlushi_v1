<?php

namespace App\Services;

use GuzzleHttp\Client;
use Cache;

/**
 * http://www.kuaidi100.com/openapi/api_post.shtml
 */
class ExpressInquiryServices
{
    const NoResultStatus = 0;
    const SuccessStatus = 1;
    const ErrorStatus = 2;

    const CustomerSigned = 3;

    private static $app_key = null;
    private static $app_customer = null;

    /**
     * @param $com
     * @param $nu
     * @return null|array
     */
    public static function get($com, $nu)
    {
        //获取快递公司代码
        $com_code = array_search($com, config('sites.express_name'));
        if ($com_code) $com = $com_code;

        //利用缓存减少查询次数
        $cache_key = self::class . $nu;
        if (Cache::has($cache_key)) {
            return null;
        }
        Cache::put($cache_key, $com, 120);

        static::$app_key = env('KUAIDI100_KEY');
        static::$app_customer = env('KUAIDI100_CUSTOMER');

        if(static::$app_customer){
            return static::enterprise($com, $nu);
        }
        return static::free($com, $nu);
    }

    /**
     * 免费版
     * @param $com
     * @param $nu
     * @return null|array
     */
    public static function free($com, $nu)
    {
        $app_key = static::$app_key;
        $client = new Client();
        $res = $client
            ->get('http://api.kuaidi100.com/api', [
                'query' => [
                    'id' => $app_key,
                    'com' => $com,
                    'nu' => $nu,
                    'valicode' => '',
                    'show' => 0,
                    'muti' => 1,
                    'order' => 'desc',
                ],
            ]);

        $response_data = json_decode($res->getBody()->getContents());
        \Log::error('快递100:' . $response_data->message, (array)$response_data);
        if ($response_data->status != static::SuccessStatus) {
            return null;
        }
        return $response_data;
    }

    /**
     * 企业版
     * @param $com
     * @param $num
     * @return null|array
     */
    public static function enterprise($com, $num)
    {
        $app_key = static::$app_key;
        $app_customer = static::$app_customer;
        $param = json_encode([
            'com' => $com,
            'num' => $num,
            //'from' => '',
            //'to' => '',
        ]);
        $sign = strtoupper(md5($param . $app_key . $app_customer));

        $client = new Client();
        $res = $client
            ->post('http://poll.kuaidi100.com/poll/query.do', [
                'form_params' => [
                    'customer' => $app_customer,
                    'param' => $param,
                    'sign' => $sign,
                ],
            ]);

        $response_data = json_decode($res->getBody()->getContents());
        \Log::error('快递100:' . $response_data->message, (array)$response_data);
        if (!empty($response_data->returnCode)) {
            return null;
        }
        return $response_data;
    }
}
