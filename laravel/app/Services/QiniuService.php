<?php

namespace App\Services;

use Qiniu\Auth;

class QiniuService
{
    private $auth;
    private $bucket;
    public $download_url;
    public $upload_url;

    public function __construct()
    {
        $accessKey = env('QINIU_ACCESSKEY');
        $secretKey = env('QINIU_SECRETKEY');
        $this->bucket = env('QINIU_BUCKET');
        $this->download_url = 'http://'.env('QINIU_DOWNLOAD_DOMAIN');
        $this->upload_url = 'http://'.env('QINIU_UPLOAD_DOMAIN');
        $this->auth = new Auth($accessKey, $secretKey);
    }

    public function token()
    {
        return $this->auth->uploadToken($this->bucket);
    }

    public function safeToken($callback_url)
    {
        $user_agent = request()->server('HTTP_USER_AGENT', '');
        $body = [
            'uuid'       => '$(uuid)',
            'hash'       => '$(etag)',
            'bucket'     => '$(bucket)',
            'key'        => '$(key)',
            'mime_type'  => '$(mimeType)',
            'fname'      => '$(fname)',
            'fsize'      => '$(fsize)',
            'end_user'   => '$(endUser)',
            'user_agent' => $user_agent,
        ];
        $policy = [
            'endUser'          => str_random(16),
            'callbackUrl'      => $callback_url,
            'callbackBodyType' => 'application/json',
            'callbackBody'     => json_encode($body),
        ];
        return $this->auth->uploadToken($this->bucket, null, 60 * 60 * 24, $policy);
    }

    public static function callback()
    {
        return [
            'hash' => request('hash'),
            'key'  => request('key'),
        ];
    }
}
