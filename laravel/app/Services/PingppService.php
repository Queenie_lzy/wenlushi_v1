<?php

namespace App\Services;

use Pingpp\Pingpp;
use Pingpp\Charge;
use Pingpp\Event;
use Pingpp\Refund;

class PingppService
{
    private $app_id;

    public function __construct()
    {
        $this->app_id = env('PINGPP_APPID');
        $privateKey = env('PINGPP_PRIVATE', resource_path('pingpp/rsa_private_key.pem'));

        Pingpp::setApiKey(env('PINGPP_KEY'));
        if (file_exists($privateKey)) {
            Pingpp::setPrivateKeyPath($privateKey);
        }
    }

    /**
     * @param $order_no [订单号]
     * @param $amount [价格]
     * @param $channel [支付方式:alipay|wx]
     * @param $subject [商品的标题，最长为 32 个]
     * @param $body [商品的描述信息，最长为 100 个]
     * @param array $extra
     * @return Charge
     */
    public function create($order_no, $amount, $channel, $subject = null, $body = null, $extra = [])
    {
        if (!$subject) {
            $subject = $order_no;
        }
        if (!$body) {
            $body = $order_no;
        }

        return Charge::create([
            'order_no' => $order_no,
            'amount' => $amount * 100,
            'app' => ['id' => $this->app_id],
            'channel' => $channel,
            'currency' => 'cny',
            'client_ip' => request()->getClientIp() ?: '127.0.0.1',
            'subject' => $subject,
            'body' => $body,
            'extra' => $extra,
        ]);
    }

    /**
     * 创建退款
     *
     * @param $charge_id
     * @param $amount
     * @param null $description
     * @return Refund
     */
    public function refundsCreate($charge_id, $amount, $description = null)
    {
        $charge = $this->findCharge($charge_id);
        $amount = $amount * 100;
        return $charge->refunds->create(compact('amount', 'description'));
    }

    /**
     * 查找支付请求数据
     *
     * @param $charge_id
     * @return Charge
     */
    public function findCharge($charge_id)
    {
        return Charge::retrieve($charge_id);
    }

    /**
     * 根据事件获取数据
     *
     * @param $event_id
     * @return Charge|Event
     */
    public function findEvent($event_id)
    {
        return Event::retrieve($event_id);
    }

    /**
     * @param $charge_id
     * @param $refunds_id
     * @return Charge
     */
    public function findRefunds($charge_id, $refunds_id)
    {
        $charge = $this->findCharge($charge_id);
        return $charge->refunds->retrieve($refunds_id);
    }
}
