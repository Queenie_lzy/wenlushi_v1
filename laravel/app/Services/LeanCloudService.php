<?php

namespace App\Services;

use GuzzleHttp\Client;

class LeanCloudService
{
    private $client;
    private $app_id;
    private $app_key;
    private $master_key;

    public function __construct()
    {
        $this->app_id = env('LEANCLOUD_APPID');
        $this->app_key = env('LEANCLOUD_APPKEY');
        $this->master_key = env('LEANCLOUD_MASTERKEY');

        $this->client = new Client([
            'base_uri' => 'https://leancloud.cn/1.1/',
            //'debug'    => true,
            'headers' => [
                'Content-Type' => 'application/json',
                'X-LC-Id' => $this->app_id,
                //'X-LC-Key'     => $this->app_key,
                'X-LC-Key' => $this->master_key . ',master',
            ],
            'http_errors' => false,
        ]);
    }

    /**
     * 发送短信
     * @param $phone
     * @return bool
     */
    public function requestSmsCode($phone)
    {
        $res = $this->client
            ->post('requestSmsCode', [
                'json' => [
                    'mobilePhoneNumber' => $phone,
                ],
            ]);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * 验证短信
     * @param $phone
     * @param $code
     * @return mixed
     */
    public function verifySmsCode($phone, $code)
    {
        $res = $this->client
            ->post('verifySmsCode/' . $code, [
                'query' => [
                    'mobilePhoneNumber' => $phone,
                ],
            ]);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * 频道推送
     * @param $channel
     * @param string $alert 内容<128 个字符
     * @param string $title 标题<16 个字符
     * @param array $custom_key
     * @return mixed
     */
    public function channelPush($channel, $alert, $title = null, $custom_key = [])
    {
        $data = array_filter(array_merge(compact('alert', 'title'), $custom_key));
        $res = $this->client
            ->post('push', [
                'json' => [
                    'channels' => $channel,
                    'data' => $data,
                    'expiration_interval' => 60
                ],
            ]);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * 用户推送
     * @param array $users_id
     * @param $alert
     * @param null $title
     * @param int $expiration_interval
     * @param null $push_time
     * @param array $custom_key
     * @return mixed
     */
    public function push($users_id, $alert, $title = null, $expiration_interval = null, $push_time = null, $custom_key = [])
    {
        $data = array_filter(array_merge(compact('alert', 'title'), $custom_key));
        $push_time = $push_time ? gmdate('Y-m-d\TH:i:s.000\Z', strtotime($push_time)) : $push_time;
        if (is_array($users_id)) {
            sort($users_id);
            $users_id = [
                '$in' => $users_id,
            ];
        }else{
            $users_id = (string)$users_id;
        }
        $json = [
            'where' => [
                'user_id' => $users_id,
            ],
            'prod' => 'prod',//dev | prod
            'data' => $data,
            'badge' => 'Increment',
            'expiration_interval' => $expiration_interval,
            'push_time' => $push_time
        ];
        $res = $this->client
            ->post('push', [
                'json' => array_filter($json),
            ]);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * 获取等待推送的定时推送任务
     * @return mixed
     */
    public function getScheduledPushMessages()
    {
        $res = $this->client
            ->get('scheduledPushMessages');
        return json_decode($res->getBody()->getContents())->results;
    }

    /**
     * 取消一个定时推送任务
     * @param $id
     * @return mixed
     */
    public function deleteScheduledPushMessages($id)
    {
        $res = $this->client
            ->delete('scheduledPushMessages/' . $id);
        return json_decode($res->getBody()->getContents());
    }

    /**
     * im 发送消息
     * @param $type
     * @param array $users_id
     * @param $msg
     * @return mixed
     */
    public function rtm_messages($type, array $users_id, $msg)
    {
        sort($users_id);
        $res = $this->client
            ->post('rtm/messages', [
                'json' => [
                    'from_peer' => $type,
                    'message' => json_encode($msg, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
                    'conv_id' => $this->getConversationObjectId($type),
                    'transient' => false,
                    'to_peers' => $users_id,
                ],
            ]);

        return json_decode($res->getBody()->getContents());
    }

    private function getConversationObjectId($type)
    {
        return $this->getOrCreateConversation($type)->objectId;
    }

    private function getOrCreateConversation($type)
    {
        $res = $this->client->get('classes/_Conversation', [
            'query' => [
                'where' => json_encode([
                    'name' => $type,
                    'sys' => true,
                ]),
            ],
        ]);
        $res = json_decode($res->getBody()->getContents());
        if (isset($res->results[0])) {
            return $res->results[0];
        }

        $res = $this->client->post('classes/_Conversation', [
            'json' => [
                'name' => $type,
                'sys' => true,
            ],
        ]);

        return json_decode($res->getBody()->getContents());
    }
}
