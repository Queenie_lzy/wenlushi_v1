<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmailEvent extends Event
{
    use SerializesModels;

    public $type;
    public $email;
    public $view_data;

    public function __construct($type, $email, $view_data = [])
    {
        $this->type = $type;
        $this->email = $email;
        $this->view_data = $view_data;
    }
}
