<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Meeting;
use App\Observers\ArticleObserver;
use App\Observers\MeetingObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Meeting::observe(MeetingObserver::class);
        Article::observe(ArticleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
