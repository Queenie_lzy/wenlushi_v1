<?php

namespace App\Observers;

use App\Models\Article;

class ArticleObserver
{
    public function created(Article $article)
    {
        $now = date('Y-m-d H:i:s');
        $data = [
            'created_at' => $now,
            'updated_at' => $now
        ];
        if($article->status != 'draft'){
            if($article->user->role == 'admin') {
                $data['status'] = 'publish';
            } else {
                $data['status'] = 'check';
            }
        }
        $article->update($data);
    }

    /*
     * 已发布状态修改内容，不更新时间，状态不改变
     */
    public function updated(Article $article)
    {
        // if($article->status != 'publish') {
        //     $article->update(['updated_at' => date('Y-m-d H:i:s')]);
        // }
    }
}