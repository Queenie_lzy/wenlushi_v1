<?php

namespace App\Observers;

use App\Models\Meeting;

class MeetingObserver
{
    /*
     * 第一期：留学服务发起的语音会议时长不扣除时长，普通问答发起的语音会议时长要扣除时长。第一期线下购买时长
     */
    public function updated(Meeting $meeting)
    {
        if(!$meeting->apply_id) {
            if($meeting->mode == '语音会议' && $meeting->status == 'resolve') {
                $res = $meeting->user->duration - $meeting->duration;
                $meeting->user()->update([
                        'duration' => $res > 0 ? $res : 0
                    ]);
            }
        }
    }
}