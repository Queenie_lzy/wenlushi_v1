# 问路石

## 安装

### 假设已经使用 git clone 下来本项目，那么环境需要

- [composer](https://getcomposer.org/doc/)
- [nodejs](https://nodejs.org/en/)

### 代码规范

1. 采用 es6 语法，需遵从[airbnb的javascript语法指南](https://github.com/airbnb/javascript)
2. 使用 jquery 需注意性能以及阅读方面 [编写更好的jQuery代码的建议](http://blog.jobbole.com/52770/)[jQuery性能优化的28个建议](http://www.cnblogs.com/ranzige/p/3625172.html)

### 进入 laravel 目录 运行

1. 安装 PHP 依赖 composer install
2. 安装 npm 依赖 npm i
3. 压缩 静态资源 npm run prod 或 静态资源监控模式 npm run dev

### .env 配置 依赖服务

```

```

### 其他参考文档

- [laravel](https://laravel.com/docs/5.2)
- [laravel elixir](https://laravel.com/docs/5.3/elixir)
- [vuejs](http://vuejs.org/guide/)
